import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';
import eslintPluginPrettierRecommended from 'eslint-plugin-prettier/recommended';
import globals from 'globals';
import tailwind from 'eslint-plugin-tailwindcss';
import reactNativeA11y from 'eslint-plugin-react-native-a11y';
import { fileURLToPath } from 'node:url';
import path from 'node:path';
import { FlatCompat } from '@eslint/eslintrc';
import react from 'eslint-plugin-react';
import reactNative from 'eslint-plugin-react-native';
import reactCompiler from 'eslint-plugin-react-compiler';
import markdown from 'eslint-plugin-markdown';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
  baseDirectory: __dirname,
  recommendedConfig: eslint.configs.recommended,
  allConfig: eslint.configs.all,
});

export default tseslint.config(
  {
    ignores: ['**/dist', '**/node_modules', 'README.md'],
  },
  // todo: should be reenabled
  // { ...eslint.configs.recommended, files: ['**/*.js', '**/*.jsx', '**/*.mjs'] },
  [
    ...tseslint.configs.recommendedTypeChecked,
    ...tseslint.configs.strict,
    ...tseslint.configs.stylisticTypeChecked,
  ].map((config) => ({
    ...config,
    files: ['**/*.ts', '**/*.tsx', '**/*.mts'],
    rules: {
      '@typescript-eslint/array-type': 'off',
      '@typescript-eslint/consistent-type-definitions': 'off',
      '@typescript-eslint/consistent-type-imports': [
        'warn',
        {
          prefer: 'type-imports',
          fixStyle: 'inline-type-imports',
        },
      ],
      '@typescript-eslint/no-unused-vars': [
        'warn',
        {
          argsIgnorePattern: '^_',
        },
      ],
      '@typescript-eslint/require-await': 'off',
      '@typescript-eslint/no-misused-promises': [
        'error',
        {
          checksVoidReturn: {
            attributes: false,
          },
        },
      ],
      '@typescript-eslint/no-floating-promises': 'error',
      '@typescript-eslint/unbound-method': 'off',
      '@typescript-eslint/no-unbound-vars': 'off',
    },
  })),
  eslintPluginPrettierRecommended,
  {
    languageOptions: {
      parserOptions: {
        projectService: true,
        tsconfigRootDir: import.meta.dirname,
      },
    },
  },
  tailwind.configs['flat/recommended'].map((config) => ({
    ...config,
    files: ['**/*.ts', '**/*.tsx', '**/*.mts'],
    settings: {
      tailwindcss: {
        callees: ['cn', 'clsx', 'twMerge'],
        config: 'tailwind.config.cjs',
        validateTemplateStrings: true,
      },
    },
    rules: {
      'tailwindcss/no-custom-classname': 'error',
    },
  })),

  ...compat
    .extends('plugin:react-native-a11y/all', 'plugin:react/recommended')
    .map((config) => ({
      ...config,
      files: ['**/*.tsx'],
      ignores: ['**/__tests__/**', '**/examples/**'],
      rules: {
        // to get rid of the error:
        // Error while loading rule 'react-native-a11y/has-valid-accessibility-ignores-invert-colors': Cannot read properties of undefined (reading 'sourceCode')
        'react-native-a11y/has-valid-accessibility-ignores-invert-colors':
          'off',
      },
    })),

  {
    files: ['**/*.tsx'],
    plugins: {
      react,
      'react-native': reactNative,
      'react-compiler': reactCompiler,
    },
    languageOptions: {
      globals: {
        ...reactNative.environments['react-native']['react-native'],
      },
      parserOptions: {
        ecmaFeatures: {
          jsx: true,
        },
      },
    },
    settings: {
      react: {
        version: 'detect',
      },
    },
    rules: {
      'react-native/no-raw-text': [
        'error',
        {
          skip: ['Link', 'Strong'],
        },
      ],
      'react-compiler/react-compiler': 'error',
      'react/prop-types': 'off',
    },
  },
  ...markdown.configs.recommended
);
