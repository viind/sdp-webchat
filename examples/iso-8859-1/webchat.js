!(function () {
  const e = document.createElement('script'),
    t = document.head || document.getElementsByTagName('head')[0];
  e.src = '/viind-sdp-webchat.min.js';
  e.async = true;
  e.charset = 'UTF-8';
  window.exports = window.exports ?? {};
  e.type = 'module';
  e.onload = () => {
    window.setupViindWebChat({
      socketUrl: '$$SOCKET_URL$$',
      socketPath: '/socket.io.multitenant/',
      initPayload: 'Hallo',
      openButton: {
        title: 'Hallo!',
        description: 'Ich bin Max der Chatbot.\nProbieren Sie mich gerne aus.',
      },
      header: {
        title: 'Max',
        description: 'KI-Chatbot der Stadt Musterstadt',
      },
      inputTextFieldHint: 'Nachricht eintippen...',
      embedded: false,
      showFullScreenButton: true,
      infos: [
        {
          key: 'phone',
          value: '01234 123456',
        },
        {
          key: 'email',
          value: 'info@example.de',
        },
        {
          key: 'address',
          value: 'Stadtverwaltung Musterstadt\nHauptstraße 1\n12345 Buxdehude',
        },
      ],
      languages: [
        'Deutsch',
        'English',
        'Italiano',
        'Français',
        'Ελληνική',
        'Português',
        'Românesc',
        'Русский',
        'Српски',
        'Shqiptare',
        'Türkçe',
      ],
      navigationLinks: [
        {
          link: 'https://www.viind.com/datenschutz-sdp',
          title: 'Datenschutz',
        },
      ],
      params: {
        storage: 'session',
      },
      tooltipHeader: '',
      tooltipText: '... ich beantworte Ihre Fragen!',
      tooltipSuggestions: [],
      chatIndicator: true,
      customerId: '$$CUSTOMER_ID$$',
    });
  };
  t.insertBefore(e, t.firstChild);
})();
