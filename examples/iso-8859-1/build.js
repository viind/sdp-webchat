import { join } from 'node:path';
import dotenv from 'dotenv';
import { cp, readFile, writeFile, mkdir } from 'node:fs/promises';
import { parse } from 'node-html-parser';
import assert from 'node:assert';

const root = join(__dirname, '..', '..');

const paths = {
  env: join(root, '.env.dev'),
  buildOutput: join(root, 'dist', 'web'),
  servedOutput: join(root, 'examples', 'iso-8859-1', 'dist'),
  webchatJs: join(root, 'examples', 'iso-8859-1', 'webchat.js'),
};

async function main() {
  const env = await loadEnvConfig();
  await copyDistOutput();
  await copyWebchatJs();
  await applyEnvToWebchatJs(env);
  await editIndexHtml();
}

async function loadEnvConfig() {
  const envFile = await readFile(paths.env, 'utf-8');
  const config = dotenv.parse(envFile);

  assert(config.VITE_CUSTOMER_ID, 'VITE_CUSTOMER_ID is not set');
  assert(config.VITE_SOCKET_URL, 'VITE_SOCKET_URL is not set');

  return {
    customerId: config.VITE_CUSTOMER_ID,
    socketUrl: config.VITE_SOCKET_URL,
  };
}

async function copyDistOutput() {
  const src = paths.buildOutput;
  const dest = paths.servedOutput;
  await mkdir(dest, { recursive: true });
  await cp(src, dest, { recursive: true });
}

async function copyWebchatJs() {
  const src = paths.webchatJs;
  const dest = join(paths.servedOutput, 'webchat.js');
  await cp(src, dest);
}

async function applyEnvToWebchatJs(env) {
  const filePath = join(paths.servedOutput, 'webchat.js');
  const fileContent = await readFile(filePath, 'utf-8');
  const newFileContent = fileContent
    .replace('$$SOCKET_URL$$', env.socketUrl)
    .replace('$$CUSTOMER_ID$$', env.customerId);
  await writeFile(filePath, newFileContent);
}

async function editIndexHtml() {
  const filePath = join(paths.servedOutput, 'index.html');
  const fileContent = await readFile(filePath, 'utf-8');

  const root = parse(fileContent);

  // set the correct charset in the meta tag
  root.querySelector('meta[charset]').remove();
  root
    .querySelector('head')
    .appendChild(
      parse(
        '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">'
      )
    );

  // remove all script tags
  const scriptTags = root.querySelectorAll('script');
  scriptTags.forEach((script) => script.remove());

  // add a script tag to load the webchat.js file
  const newScript = parse('<script src="./webchat.js"></script>');
  root.querySelector('body').appendChild(newScript);

  // `latin1` is the correct charset for iso-8859-1
  await writeFile(filePath, root.toString(), 'latin1');
}

main();
