import * as React from 'react';

import { StyleSheet, View } from 'react-native';
import { ViindSdpWebChat } from '../../../projects/lib/src';

export default function App() {
  const customerId = process.env.EXPO_PUBLIC_CUSTOMER_ID;
  if (customerId === undefined) {
    throw new Error('Missing customer id');
  }
  return (
    <View style={styles.container}>
      <ViindSdpWebChat
        socketUrl="https://sdp-chatbot.cluster02.viind.io"
        socketPath="/socket.io.multitenant/"
        customerId={customerId}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
});
