const { hairlineWidth } = require('nativewind/theme');

/** @type {import('tailwindcss').Config} */
module.exports = {
  prefix: 'vi-',
  darkMode: 'class',
  content: ['projects/lib/**/*.{js,jsx,ts,tsx}'],
  presets: [require('nativewind/preset')],
  theme: {
    fontFamily: {
      roboto: ['Roboto', 'sans'],
    },
    colors: {
      'background': 'rgb(var(--viind-background) / <alpha-value>)',
      'background-focus': 'rgb(var(--viind-background-focus) / <alpha-value>)',
      'foreground': 'rgb(var(--viind-foreground) / <alpha-value>)',
      'foreground-primary':
        'rgb(var(--viind-foreground-primary) / <alpha-value>)',
      'primary': {
        DEFAULT: 'rgb(var(--viind-primary) / <alpha-value>)',
        foreground: 'rgb(var(--viind-primary-foreground) / <alpha-value>)',
      },
      'primary-focus': 'rgb(var(--viind-primary-focus) / <alpha-value>)',
      'user-message': {
        DEFAULT: 'rgb(var(--viind-user-message) / <alpha-value>)',
        foreground: 'rgb(var(--viind-user-message-foreground) / <alpha-value>)',
      },
      'bot-message': {
        DEFAULT: 'rgb(var(--viind-bot-message) / <alpha-value>)',
        foreground: 'rgb(var(--viind-bot-message-foreground) / <alpha-value>)',
      },
      'bot-message-focus':
        'rgb(var(--viind-bot-message-focus) / <alpha-value>)',
      'border': {
        DEFAULT: 'rgb(var(--viind-border) / <alpha-value>)',
        message: 'rgb(var(--viind-border-message) / <alpha-value>)',
      },
      'success': 'rgb(var(--viind-success) / <alpha-value>)',
      'success-foreground':
        'rgb(var(--viind-success-foreground) / <alpha-value>)',
      'error': 'rgb(var(--viind-error) / <alpha-value>)',
      'error-foreground': 'rgb(var(--viind-error-foreground) / <alpha-value>)',
    },
    ringColor: {
      'background-focus': 'rgb(var(--viind-background-focus) / <alpha-value>)',
    },
    keyframes: {
      'accordion-down': {
        from: { height: '0' },
        to: { height: 'var(--radix-accordion-content-height)' },
      },
      'accordion-up': {
        from: { height: 'var(--radix-accordion-content-height)' },
        to: { height: '0' },
      },
      'spin': {
        from: {
          transform: 'rotate(0deg)',
        },
        to: {
          transform: 'rotate(360deg)',
        },
      },
    },
    animation: {
      'accordion-down': 'accordion-down 0.2s ease-out',
      'accordion-up': 'accordion-up 0.2s ease-out',
      'spin': 'spin 2s linear infinite',
    },
    extend: {
      borderWidth: {
        hairline: hairlineWidth(),
      },
    },
  },
  plugins: [require('tailwindcss-animate')],
};
