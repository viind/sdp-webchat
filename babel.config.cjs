const ReactCompilerConfig = {
  target: '18',
};

module.exports = {
  presets: ['nativewind/babel'],
  plugins: [
    ['babel-plugin-react-compiler', ReactCompilerConfig], // must run first!

    '@babel/plugin-proposal-export-namespace-from',
    'react-native-reanimated/plugin',
  ],
};
