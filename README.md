<h1 align="center">viind SDP Webchat 💬</h1>
<h5 align="center">
 A chat widget to deploy virtual assistants made with <a href="https://github.com/rasaHQ/rasa">Rasa</a> or <a href="https://www.viind.com">viind's Smart Dialogue Platform</a> on any website.
</h5>
<br />

## Usage

### Web

To use the web chat, you need to include the script in your HTML file and initialize the web chat with the `setupViindWebChat` function.

**Script tag**

The script is hosted on the following url: `https://assets.viind.com/webchat/VERSION/viind-sdp-webchat.min.js` You can choose a specific version by replacing `VERSION` with the desired version (see available tags) or use the latest major version by using for example `v3`.

```html
<script
  type="module"
  crossorigin
  id="sdp-webchat-script"
  src="https://assets.viind.com/webchat/v3/viind-sdp-webchat.min.js"
></script>
```

**Initialization**

After the script has been loaded, you can initialize the web chat by calling the `setupViindWebChat` function.

```html
<script>
  var exports = {}; // This is required only if you did not define it somewhere else already.

  const script = getScript();
  script.addEventListener('load', () => {
    globalThis
      .setupViindWebChat({
        customerId: '<your-customer-id>',
        socketUrl: '<your cluster url>',
      })
      .then(({ sendMessage }) => {
        // `sendMessage` is a function that can be used to send a message as if the user typed it.

        // Example:
        sendMessage('Hello World!');
      });
  });

  function getScript() {
    const byId = document.getElementById('sdp-webchat-script');
    if (byId) {
      return byId;
    }
    const bySrc = [...document.getElementsByTagName('script')].find((script) =>
      script.src.includes('viind-sdp-webchat')
    );
    return bySrc;
  }
</script>
```

> ![NOTE]
> You can use an ID to identify the script element or you simply rely on the `src` attribute containing the string `viind-sdp-webchat`.

> ![IMPORTANT]
> You must provide a `customerId` and a `socketUrl` to the `setupViindWebChat` function.

The web chat can be configured by passing a config object to the `setupViindWebChat` function.

```ts
/**
 * Create the Viind WebChat as an overlay on top of the current page.
 */
setupViindWebChat(config: ViindWebChatConfig): Promise<{ sendMessage: SendMessage }>;

/**
 * Create the Viind WebChat as an embedded chat inside the provided parent element.
 */
setupViindWebChat(
  config: ViindWebChatConfig,
  parentElement: HTMLElement
): Promise<{ sendMessage: SendMessage }>;

/**
 * Send a message as if the user typed it.
 *
 * @param message Only string is allowed. If the message is empty, nothing happens
 *
 * @throws {Error} If the message is not of type string.
 */
type SendMessage = (message: string) => void;
```

To **embed** the web chat inside a parent element, you need to pass the parent element as the second argument to the `setupViindWebChat` function.
This will place the webchat in the provided parent element.

> ![IMPORTANT]
> The web chat will use the available space, so make sure that you assign a `height` and `width` to the parent element.
> Otherwise it will only smallest size possible, which results in the chat not being readable.

#### Required configuration

> **\*** This option is required

| Option             | Type     | Default    | Description                              |
| ------------------ | -------- | ---------- | ---------------------------------------- |
| `customerId`**\*** | `string` | (required) | The customer id of your instance.        |
| `socketUrl`**\***  | `string` | (required) | The socket url of your assigned cluster. |

#### Integration configurations

| Option                       | Type                                                                      | Default                   | Description                                                                                                  |
| ---------------------------- | ------------------------------------------------------------------------- | ------------------------- | ------------------------------------------------------------------------------------------------------------ |
| `socketPath`                 | `string`                                                                  | `/socket.io.multitenant/` | Even though the WS path is configurable, you should not need to change it.                                   |
| `logLevel`                   | `trace`, `debug`, `info`, `warn`, `error`                                 | `warn`                    | The log level to use for the web chat. Helpful in case of integration issues                                 |
| `storageStrategy`            | `localStorage` or `sessionStorage`                                        | `sessionStorage`          | The storage strategy to use for storing configuration and other state.                                       |
| `forceView`                  | `mobile`, `desktop-windowed`, `desktop-fullscreen`,`desktop`, `undefined` | `undefined`               | Lock the web chat into a specific view instead of it automatically detecting the view which should be used.  |
| `initPayload`                | `string`                                                                  | ''                        | Upon opening the web chat, this message will be send to the server as if the user had entered it.            |
| `sidePanelClosedOnFirstOpen` | `boolean`, `undefined`                                                    | `undefined`               | If set to `true`, the side panel will be closed on first open. This only affects the `desktop-windowed` view |

#### Language configuration

Currently supported languages:

```js
const languages = [
  'Deutsch',
  'English',
  'Українською',
  'Français',
  'Česky',
  'Български',
  'Dansk',
  'Ελληνική',
  'Español',
  'Eesti',
  'Suomalainen',
  'Magyar',
  'Bahasa Indonesia',
  'Italiano',
  '日本語',
  '한국어',
  'Lietuvių kalba',
  'Latviešu',
  'Norsk',
  'Nederlands',
  'Polski',
  'Português',
  'Românesc',
  'Русский',
  'Slovenská',
  'Slovenski',
  'Svenska',
  'Türkçe',
  '中文',
  'Hrvatski',
  'Српски',
  'Tiếng Việt',
];
```

| Option                         | Type                   | Default         | Description                                                                        |
| ------------------------------ | ---------------------- | --------------- | ---------------------------------------------------------------------------------- |
| `applySystemLanguageOnStartup` | `boolean`, `undefined` | `undefined`     | If set to `true`, the web chat will try to apply the system language on startup.   |
| `languages`                    | `string[]`             | (all languages) | To only allow a subset of languages, you need to specify which languages to allow. |

#### Your own information configurations

```ts
type InfoOptions =
  | (
      | {
          value: string;
          key: 'phone' | 'email' | 'address';
        }
      | {
          title: string;
          key: 'link';
          url: string;
        }
    )[]
  | undefined;
type ChannelOptions =
  | {
      title: string;
      // if not present, it is `url`
      type?: 'address' | 'url' | 'phone' | 'email';
      value: string;
      icon?: string | undefined;
    }[]
  | undefined;
type HeaderOptions =
  | {
      title?: string | undefined;
      description?: string | undefined;
    }
  | undefined;
type OpenButton =
  | {
      forceSize?: 'big' | 'small' | undefined;
      forceOrientation?: 'horizontal' | 'vertical' | undefined;
      title?: string | undefined;
      description?: string | undefined;
    }
  | undefined;
type PositioningOptions =
  | {
      zIndex?: number | undefined;
      windowPaddingBottom?: number | undefined;
      windowPaddingRight?: number | undefined;
      openButtonPaddingBottom?: number | undefined;
      openButtonPaddingRight?: number | undefined;
      windowCloseButtonPaddingBottom?: number | undefined;
    }
  | undefined;
type AvatarOptions =
  | {
      headerUrl?: string | undefined;
      headerRoundLevel?: number | undefined;
      bigOpenButtonUrl?: string | undefined;
      bigOpenButtonRoundLevel?: number | undefined;
      smallOpenButtonUrl?: string | undefined;
    }
  | undefined;
type TtsSttOptions =
  | {
      disableTextToSpeech: boolean;
      disableSpeechToText: boolean;
    }
  | undefined;
```

> ![NOTE]
> Custom icons for channels are available for `whatsapp`, `facebook messenger` and `phone` (for the `channel` value).
> For all other `channel` values it will use the same icon as the channel page

> ![NOTE]
> Non of the texts that can be configured here allow any styling or custom HTML.

| Option        | Type                         | Default     | Description                                                                                                                                                              |
| ------------- | ---------------------------- | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `infos`       | `InfoOptions`                | `undefined` | Controls what is visible in the info view. In case of no configuration, then a default text will be shown instead on the page                                            |
| `channels`    | `ChannelOptions`             | `undefined` | Controls what is visible in the channel view. In case of no configuration, then channel view will not be shown                                                           |
| `header`      | `HeaderOptions`              | `undefined` | An object to configure the header of the web chat. The header is only visible upon opening the web chat                                                                  |
| `openButton`  | `OpenButton`                 | `undefined` | An object to configure the open button of the web chat. The `title` and `description` are only visible in the big button mode                                            |
| `positioning` | `PositioningOptions`         | `undefined` | An object to configure the positioning of the web chat. The `zIndex` is only applied if the webchat is not in embedded mode. All units except for `zIndex` are in `rem`. |
| `avatar`      | `AvatarOptions`              | `undefined` | An object to configure the avatar of the web chat. The `Level` options are in `%`.                                                                                       |
| `ttsStt`      | `TtsSttOptions` \| `boolean` | `false`     | A boolean to enable or disable the feature or an object to have the feature enabled, but to disable what you don't want.                                                 |

Initially the button will be big and upon opening the web chat once, then button will stay small indefinitely.

#### Styling configurations

```ts
type ThemeOptions = {
  background: string | undefined;
  foreground: string | undefined;
  foregroundPrimary: string | undefined;
  primary: string | undefined;
  primaryForeground: string | undefined;
  userMessage: string | undefined;
  userMessageForeground: string | undefined;
  botMessage: string | undefined;
  botMessageForeground: string | undefined;
  border: string | undefined;
  borderMessage: string | undefined;
  success: string | undefined;
  successForeground: string | undefined;
  error: string | undefined;
  errorForeground: string | undefined;
  scrollbarThumb: string | undefined;
};
```

> ![NOTE]
> Possible values to override the theme are `hex` (e.g. `#ffffff`, has to be the long form) or `rgb` as numbers (e.g. `255 255 255`)

| Option             | Type     | Default | Description                                       |
| ------------------ | -------- | ------- | ------------------------------------------------- |
| `scrollbarStyling` | `string` | `viind` | defines which style is applied for the scrollbar. |
| `theme`            | `        |

The global style shows how to define a new scrollbar style [index.css](projects/lib/index.css).
The important part is to use the following selector `[data-scrollbar-styling='<your-style-name>']`.

> ![NOTE]
> There is no need to use the `theme` option as you can also simply set the CSS variables directly.
> The default values will be added to the `html` element.
> So, as long as you define them on a lower level, they will be applied.
> This also allows you to provide multiple themes for the web chat and they could be conditionally applied.
> Multiple themes is not possible with the `theme` option.

### Library

## License

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

```
http://www.apache.org/licenses/LICENSE-2.0
```

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.(C) 2021 Dialogue Technologies Inc. All rights reserved.
