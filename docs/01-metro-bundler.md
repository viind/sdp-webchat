# Use the Metro bundler

The Metro bundler is the current default bundler used by Expo.
It will replace WebPack in the long run.

Tried to use metro bundler, but it was not possible right now to configure it the web output in a way to we need.

## Reasons why it did not work

- Hardly any documentation
  - The most extensive documentation for [metro](https://https://docs.expo.dev/versions/latest/config/metro/)
  - Metros own documentation is limited
- Our use case is rather special
  - We want to output everything as a single JS file and nothing should be in the index.html
  - Metro placed styles in the index.html and created a single JS file
