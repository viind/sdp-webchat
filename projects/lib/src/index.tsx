import React from 'react';

import { z } from 'zod';
import '../lang/i18n';
import { applyZIndexToParent } from './hooks/apply-z-index-to-parent';
import { App } from './layout/app';
import {
  ChatOpenProvider,
  webChatDialogButtonSchema,
} from './provider/chat-open-provider';
import {
  DialogProvider,
  dialogProviderSchema,
} from './provider/dialog-provider';
import {
  allowedViewsSchema,
  FeatureProvider,
} from './provider/feature-provider';
import { FullscreenProvider } from './provider/fullscreen-provider';
import { PanelProvider } from './provider/panel-provider';
import {
  sidePanelConfigSchema,
  SidePanelProvider,
} from './provider/side-panel-provider';
import {
  StorageProvider,
  storageStrategySchema,
} from './provider/storage-provider';
import { StoreProvider, useStore } from './provider/store-provider';
import { ThemeProvider } from './provider/theme-provider';
import { storeSchema } from './store';
export const viindSdpWebChatSchema = webChatDialogButtonSchema
  .merge(
    z.object({
      storageStrategy: storageStrategySchema,
      forcedView: allowedViewsSchema.optional(),
      embedded: z.boolean().optional(),
    })
  )
  .merge(dialogProviderSchema)
  .merge(sidePanelConfigSchema)
  .merge(storeSchema);

export type ViindProps = z.infer<typeof viindSdpWebChatSchema> & {
  rootElement?: HTMLElement;
  rootTag?: HTMLElement;
};

export function ViindSdpWebChat(viindProps: ViindProps) {
  const { updatedConfig, changedConfigFields } = streamlineConfig(viindProps);

  return (
    <Providers viindProps={updatedConfig}>
      <Helper
        rootTag={updatedConfig.rootTag}
        viindProps={updatedConfig}
        changedConfigFields={changedConfigFields}
      >
        <App />
      </Helper>
    </Providers>
  );
}

function Providers({
  children,
  viindProps,
}: {
  children: React.ReactNode;
  viindProps: ViindProps;
}) {
  return (
    <StoreProvider config={viindProps}>
      <StorageProvider strategy={viindProps.storageStrategy}>
        <ThemeProvider>
          <FullscreenProvider>
            <SidePanelProvider {...viindProps}>
              <FeatureProvider
                forcedView={viindProps.forcedView}
                embedded={viindProps.embedded}
              >
                <ChatOpenProvider {...viindProps.openButton}>
                  <PanelProvider>
                    <DialogProvider {...viindProps}>{children}</DialogProvider>
                  </PanelProvider>
                </ChatOpenProvider>
              </FeatureProvider>
            </SidePanelProvider>
          </FullscreenProvider>
        </ThemeProvider>
      </StorageProvider>
    </StoreProvider>
  );
}

function Helper({
  children,
  rootTag,
  viindProps,
  changedConfigFields,
}: {
  children: React.ReactNode;
  rootTag?: HTMLElement;
  viindProps: ViindProps;
  changedConfigFields: ReturnType<
    typeof streamlineConfig
  >['changedConfigFields'];
}) {
  // needed, so that the Providers are mounted
  logMountedState(viindProps, changedConfigFields);
  applyZIndexToParent(rootTag);
  const setRootElement = useStore((state) => state.positioning.setRootElement);
  const hydrated = useStore((state) => state.isHydrated);
  const setScrollbarStyling = useStore(
    (state) => state.theme.setScrollbarStyling
  );

  React.useEffect(() => {
    setRootElement(viindProps.rootElement);
  }, [viindProps.rootElement]);

  React.useEffect(() => {
    setScrollbarStyling(viindProps.scrollbarStyling);
  }, [viindProps.scrollbarStyling]);

  if (!hydrated) {
    return null;
  }

  return <>{children}</>;
}

function logMountedState(
  viindProps: ViindProps,
  configFields: Array<{ key: string; change: string }>
) {
  const { info, warn } = useStore((state) => state.logger);

  React.useEffect(() => {
    info('ViindSdpWebChat: mounted with config', viindProps);

    if (configFields.length > 0) {
      warn('ViindSdpWebChat: config fields that were overridden', configFields);
    }
    return () => {
      info('ViindSdpWebChat: unmounted');
    };
  }, []);
}

type MarkSpecificAsRequired<T, Keys extends keyof T> = Omit<T, Keys> & {
  [K in Keys]-?: Required<Pick<T, K>>[K];
};

function streamlineConfig(viindProps: ViindProps) {
  const updatedConfig = { ...viindProps };
  const changedConfigFields: Array<{ key: string; change: string }> = [];

  if (updatedConfig.embedded === true) {
    if (updatedConfig.forcedView !== undefined) {
      changedConfigFields.push({
        key: 'forcedView',
        change: `When embedded mode is used, only the mobile view can be used.
Therefore this option is ignored.
To remove this warning, remove the configuration option of 'forcedView'.`,
      });
    }

    if (updatedConfig.rootElement === undefined) {
      throw new Error(
        `When embedded mode is used, a root element must be provided.`
      );
    }
  }

  if (updatedConfig.scrollbarStyling === undefined) {
    updatedConfig.scrollbarStyling = 'viind';
  }

  return {
    updatedConfig: updatedConfig as MarkSpecificAsRequired<
      ViindProps,
      'scrollbarStyling'
    >,
    changedConfigFields,
  };
}
