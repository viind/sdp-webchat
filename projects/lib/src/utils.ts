import { Platform } from 'react-native';

export function focusComponent(component?: { focus: () => void } | null) {
  if (!component) {
    return;
  }

  component.focus();
  if (component instanceof HTMLElement) {
    /**
     * The browser behaves differently when the user uses the "tab" key to focus on an element.
     * This event is automatically triggered in this case.
     * So, we need to trigger it manually when we programmatically focus on an element.
     */
    component.dispatchEvent(new Event('focusin'));
  }
}

export function phoneToUrl(phoneNumber: string) {
  return `tel:${phoneNumber}`;
}

export function emailToUrl(email: string) {
  return `mailto:${email}`;
}

export function addressToUrl(address: string) {
  const encodedAddress = address.replaceAll('\n', '+').replaceAll(' ', '+');
  const url = Platform.select({
    android: `geo:0,0?q=${encodedAddress}`,
    ios: `maps:0,0?q=${encodedAddress}`,
    web: `https://www.google.com/maps/search/?api=1&query=${encodedAddress}`,
  });

  if (!url) {
    throw new Error('Unsupported platform');
  }
  return url;
}

export function isRealChrome() {
  let result = false;

  // If userAgentData is available, use that first:
  // @ts-ignore
  if (navigator.userAgentData && navigator.userAgentData.brands) {
    // navigator.userAgentData.brands is an array of objects like:
    // [ {brand: "Chromium", version: "109"}, {brand: "Not:A-Brand", version: "99"}, ... ]
    // @ts-ignore
    const brands = navigator.userAgentData.brands;
    result = brands.some((b: any) => b.brand === 'Google Chrome');
  } else {
    // Fallback to older UA / vendor checks:
    const vendor = navigator.vendor || '';
    const ua = navigator.userAgent || '';
    result =
      vendor === 'Google Inc.' &&
      ua.includes('Chrome') &&
      !ua.includes('Chromium') &&
      !ua.includes('Edg') &&
      !ua.includes('OPR') &&
      !ua.includes('Brave');
  }
  return result;
}

export function isChromium() {
  let result = false;

  // Fallback to older UA / vendor checks:
  const vendor = navigator.vendor || '';
  const ua = navigator.userAgent || '';
  result = vendor === 'Google Inc.' && ua.includes('Chrome');
  return result;
}
