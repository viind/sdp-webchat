import { Platform, View } from 'react-native';

const Section: React.FC<{
  children: React.ReactNode;
  className?: string;
  /** the title is only needed if the section has no header */
}> = ({ children, className }) => {
  if (Platform.OS === 'web') {
    return <section className={className}>{children}</section>;
  }
  return <View className={className}>{children}</View>;
};

export default Section;
