import * as React from 'react';
import { Button } from '~/components/ui';
import { ExpandWindow } from '~/lib/icons/ExpandWindow';
import { MinimizeWindow } from '~/lib/icons/MinimizeWindow';
import { useFullscreen } from '../provider/fullscreen-provider';
import { useStore } from '../provider/store-provider';

export function DesktopFullscreenToggle() {
  return <FullscreenToggle size={30} />;
}

export function WindowedFullscreenToggle() {
  return <FullscreenToggle size={20} className="vi-rounded-tr-2xl" />;
}

export function MobileFullscreenToggle() {
  return <FullscreenToggle size={20} className="vi-rounded-tr-2xl" />;
}

const FullscreenToggle: React.FC<{ size: number; className?: string }> = ({
  className,
  size,
}) => {
  const t = useStore((state) => state.language.t);
  const { isFullscreen, toggleFullScreen } = useFullscreen();

  return (
    <Button
      variant="quiet-on-primary"
      size="icon"
      aria-label={
        isFullscreen
          ? t('fullscreen-toggle.make-windowed.aria-label')
          : t('fullscreen-toggle.make-fullscreen.aria-label')
      }
      language="ui"
      focus="onPrimary"
      accessibilityHint={
        isFullscreen
          ? t('fullscreen-toggle.make-windowed.aria-hint')
          : t('fullscreen-toggle.make-fullscreen.aria-hint')
      }
      onPress={() => toggleFullScreen()}
      className={className}
    >
      {isFullscreen ? (
        <MinimizeWindow size={size} />
      ) : (
        <ExpandWindow size={size} />
      )}
    </Button>
  );
};
FullscreenToggle.displayName = 'FullscreenToggle';
