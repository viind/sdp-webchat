import type React from 'react';
import { Button } from '~/components/ui';
import { CircleX } from '~/lib/icons/CircleX';
import { X } from '~/lib/icons/X';
import { cn } from '~/lib/utils';
import { useChatOpen } from '../provider/chat-open-provider';
import { useStore } from '../provider/store-provider';
import { useFeature } from '../provider/feature-provider';

export function DesktopCloseButton() {
  return (
    <CloseButton>
      <CircleX size={35} />
    </CloseButton>
  );
}

export function WindowedCloseButton({ className }: { className?: string }) {
  return (
    <CloseButton
      className={cn(
        'vi-pointer-events-auto vi-size-16 vi-rounded-full vi-bg-primary',
        className
      )}
    >
      <X size={25} />
    </CloseButton>
  );
}

export function MobileCloseButton() {
  const { showSmallHeaderClose } = useFeature();
  return (
    <CloseButton className={showSmallHeaderClose ? 'vi-size-10' : undefined}>
      <X size={20} className="vi-text-background" />
    </CloseButton>
  );
}

function CloseButton({
  children,
  className,
}: {
  children: React.ReactNode;
  className?: string;
}) {
  const t = useStore((state) => state.language.t);
  const { toggleOpen } = useChatOpen();
  return (
    <Button
      aria-label={t('close-button.aria-label')}
      language="ui"
      variant="quiet-on-primary"
      size="icon"
      focus="primaryWithRing"
      onPress={() => toggleOpen()}
      className={className}
    >
      {children}
    </Button>
  );
}
