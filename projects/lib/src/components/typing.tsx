import * as React from 'react';
import { View } from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withDelay,
  withRepeat,
  withSequence,
  withTiming,
} from 'react-native-reanimated';

const DotsAnimation = () => {
  const dot1 = useSharedValue(0);
  const dot2 = useSharedValue(0);
  const dot3 = useSharedValue(0);

  const topY = React.useMemo(() => -5, []);
  const bottomY = React.useMemo(() => 5, []);
  const duration = React.useMemo(() => 500, []);

  const dot1Style = useAnimatedStyle(
    () => ({
      transform: [
        {
          translateY: dot1.value,
        },
      ],
    }),
    [dot1]
  );

  const dot2Style = useAnimatedStyle(
    () => ({
      transform: [
        {
          translateY: dot2.value,
        },
      ],
    }),
    [dot2]
  );

  const dot3Style = useAnimatedStyle(
    () => ({
      transform: [
        {
          translateY: dot3.value,
        },
      ],
    }),
    [dot3]
  );

  React.useEffect(() => {
    dot1.value = withRepeat(
      withSequence(
        withTiming(topY, { duration }),
        withTiming(bottomY, { duration })
      ),
      0,
      true
    );
  }, [dot1, topY, bottomY, duration]);

  React.useEffect(() => {
    dot2.value = withDelay(
      100,
      withRepeat(
        withSequence(
          withTiming(topY, { duration }),
          withTiming(bottomY, { duration })
        ),
        0,
        true
      )
    );
  }, [dot2, topY, bottomY, duration]);

  React.useEffect(() => {
    dot3.value = withDelay(
      200,
      withRepeat(
        withSequence(
          withTiming(topY, { duration }),
          withTiming(bottomY, { duration })
        ),
        0,
        true
      )
    );
  }, [dot3, topY, bottomY, duration]);

  return (
    <View className="vi-flex-row vi-gap-1 vi-pt-4">
      <Animated.View style={[dot1Style]}>
        <View className="vi-size-2 vi-rounded-full vi-bg-bot-message-foreground" />
      </Animated.View>
      <Animated.View style={[dot2Style]}>
        <View className="vi-size-2 vi-rounded-full vi-bg-bot-message-foreground" />
      </Animated.View>
      <Animated.View style={[dot3Style]}>
        <View className="vi-size-2 vi-rounded-full vi-bg-bot-message-foreground" />
      </Animated.View>
    </View>
  );
};

interface Props {
  isTyping?: boolean;
}

export function TypingIndicator({ isTyping }: Props) {
  if (!isTyping) return null;

  return (
    <View className="vi-self-start vi-pr-16">
      <View className="vi-flex vi-h-14 vi-items-end vi-justify-center vi-rounded-2xl vi-bg-bot-message vi-px-6 vi-py-4">
        <DotsAnimation />
      </View>
    </View>
  );
}
