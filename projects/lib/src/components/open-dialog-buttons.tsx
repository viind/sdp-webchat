import * as React from 'react';
import { Trans } from 'react-i18next';
import { Image, View } from 'react-native';
import HelloViindy from '~/assets/images/hello_viindy.png';
import MiniViindy from '~/assets/images/min_viindy.png';
import { Button, Text, TextClassContext } from '~/components/ui';
import { CircleX } from '~/lib/icons/CircleX';
import { cn } from '~/lib/utils';
import { useChatOpen } from '../provider/chat-open-provider';
import { useStore } from '../provider/store-provider';
import { Pressable } from '~/components/ui/pressable';
import { Strong } from '~/components/ui/strong';

const OpenDialogButton: React.FC<{ className?: string }> = ({ className }) => {
  const { buttonOrientation, buttonSize, toggleOpen } = useChatOpen();

  const BigButton =
    buttonOrientation === 'horizontal'
      ? BigHorizontalButton
      : BigVerticalButton;
  const Button = buttonSize === 'big' ? BigButton : SmallButton;

  return <Button onPress={() => toggleOpen()} className={className} />;
};
OpenDialogButton.displayName = 'OpenDialogButton';
export { OpenDialogButton };

const SmallButton: React.FC<{
  onPress: () => void;
  className?: string;
}> = ({ onPress, className }) => {
  const t = useStore((state) => state.language.t);
  const smallOpenButtonUrl = useStore(
    (state) => state.avatar.smallOpenButtonUrl
  );
  return (
    <Pressable
      aria-label={t('open-button.aria-label')}
      language="ui"
      role="button"
      onPress={onPress}
      className={cn(
        className,
        'vi-h-16 vi-w-16 vi-rounded-full vi-border-2 vi-border-primary vi-bg-primary focus-within:vi-outline focus-within:vi-outline-2',
        'vi-outline-background-focus'
      )}
    >
      <Image
        accessibilityIgnoresInvertColors
        className="vi-size-full vi-rounded-full"
        source={smallOpenButtonUrl ?? MiniViindy}
        resizeMode="contain"
      />
    </Pressable>
  );
};
SmallButton.displayName = 'SmallButton';

const BigVerticalButton: React.FC<{
  onPress: () => void;
  className?: string;
}> = ({ onPress, className }) => {
  return (
    <FocusableShell
      className={cn(
        'vi-flex vi-h-[21rem] vi-w-60 vi-flex-col-reverse vi-rounded-t-2xl vi-rounded-bl-[5.25rem] vi-rounded-br-2xl vi-bg-primary vi-text-primary-foreground',
        className
      )}
    >
      <TriggerFocusButton
        onPress={onPress}
        className={'vi-grow vi-rounded-bl-[5.25rem] vi-pl-8'}
      >
        <AvatarImage />
        <View className="vi-flex vi-flex-col vi-gap-2">
          <TextBlock />
        </View>
      </TriggerFocusButton>
      <View className="vi-flex vi-flex-row">
        <FillerButton onPress={onPress} />
        <HideBigOpenButton />
      </View>
    </FocusableShell>
  );
};
BigVerticalButton.displayName = 'BigVerticalButton';

const BigHorizontalButton: React.FC<{
  onPress: () => void;
  className?: string;
}> = ({ onPress, className }) => {
  return (
    <FocusableShell
      className={cn(
        'vi-flex vi-h-52 vi-w-[28rem] vi-flex-row vi-items-center vi-justify-between vi-rounded-t-2xl vi-rounded-bl-[5.25rem] vi-rounded-br-2xl vi-bg-primary vi-text-primary-foreground',
        className
      )}
    >
      <TriggerFocusButton
        onPress={onPress}
        className="vi-h-full vi-grow vi-flex-row vi-p-3 vi-pl-8"
      >
        <AvatarImage />
        <View className="vi-flex vi-grow vi-flex-col vi-justify-center vi-gap-2 vi-pl-5">
          <TextBlock />
        </View>
      </TriggerFocusButton>
      <View className="vi-flex vi-h-full vi-flex-col">
        <HideBigOpenButton />
        <FillerButton onPress={onPress} />
      </View>
    </FocusableShell>
  );
};
BigHorizontalButton.displayName = 'BigHorizontalButton';

function TextBlock() {
  const { title, description, descriptionKey } = useOpenButtonTexts();

  return (
    <TextClassContext.Provider value={cn('vi-text-primary-foreground')}>
      <Text role="heading" className="vi-text-4xl" language="ui">
        {title}
      </Text>
      <Text className="vi-text-sm" language="ui">
        {description ?? (
          <Trans i18nKey={descriptionKey}>
            first
            <Strong language="ui">second</Strong>, third
          </Trans>
        )}
      </Text>
    </TextClassContext.Provider>
  );
}

function AvatarImage() {
  const bigOpenButtonUrl = useStore((state) => state.avatar.bigOpenButtonUrl);
  const bigOpenButtonRoundLevel = useStore(
    (state) => state.avatar.bigOpenButtonRoundLevel
  );
  return (
    <View className="vi-flex vi-items-center vi-justify-center">
      <Image
        accessibilityIgnoresInvertColors
        className="vi-size-40"
        style={{
          borderRadius: bigOpenButtonRoundLevel,
        }}
        source={bigOpenButtonUrl ?? HelloViindy}
        resizeMode="contain"
      />
    </View>
  );
}

function FocusableShell({
  children,
  className,
}: {
  children: React.ReactNode;
  className: string;
}) {
  return (
    <View
      className={cn(
        'vi-outline-primary-focus has-[.open-webchat:focus]:vi-outline has-[.open-webchat:focus]:vi-outline-2',
        className
      )}
    >
      {children}
    </View>
  );
}

function TriggerFocusButton({
  children,
  className,
  onPress,
}: {
  children: React.ReactNode;
  className: string;
  onPress: () => void;
}) {
  const { buttonAriaLabel } = useOpenButtonTexts();
  return (
    <Pressable
      aria-label={buttonAriaLabel}
      language="ui"
      role="button"
      onPress={onPress}
      className={cn(
        'vi-outline-none ' +
          // this class only exists to use it in the parent for focusing
          'open-webchat',
        className
      )}
    >
      {children}
    </Pressable>
  );
}

function FillerButton({ onPress }: { onPress: () => void }) {
  return (
    <Pressable
      aria-label={undefined}
      language={undefined}
      aria-hidden
      role="button"
      onPress={onPress}
      className="vi-grow"
      tabIndex={-1}
    ></Pressable>
  );
}

function HideBigOpenButton() {
  const t = useStore((state) => state.language.t);
  const { dismissBigButton } = useChatOpen();
  return (
    <Button
      aria-label={t('open-button.big.show-small.aria-label')}
      language="ui"
      variant="quiet-on-primary"
      size="icon"
      focus="onPrimary"
      onPress={() => dismissBigButton()}
      className="vi-rounded-tr-2xl vi-pr-3 vi-pt-3"
    >
      <CircleX size={35} />
    </Button>
  );
}

function useOpenButtonTexts() {
  const t = useStore((state) => state.language.t);
  const defaultLanguage = useStore((state) => state.language.defaultLanguage);
  const { title, description } = useChatOpen();

  const actualTitle = title ?? t('open-button.big.hello');
  const actualDescriptionKey = 'open-button.big.message';
  // Parse the translation text to handle HTML tags properly
  const descriptionText =
    description ??
    t(actualDescriptionKey)
      .replace(/<\/?[0-9]+>/g, '') // Remove HTML tags like <1> and </1>
      .replace(/\n/g, ' '); // Replace newlines with spaces

  const buttonAriaLabel = t('open-button.aria-label', {
    introduction: `${actualTitle} ${descriptionText}`,
  });

  return {
    title: actualTitle,
    /*
      Hint: there is no option to translate the title.
      Assume the provided language is the same as the default language.
      Customers will be able to define what the default language will be in the future.
      */
    titleLanguage: title ? defaultLanguage : 'ui',
    description,
    descriptionLanguage: description ? defaultLanguage : 'ui',
    descriptionKey: actualDescriptionKey,
    buttonAriaLabel,
  };
}
