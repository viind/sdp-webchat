import * as React from 'react';
import { Button } from '~/components/ui';
import { AngleUp } from '~/lib/icons/AngleUp';
import { cn } from '~/lib/utils';
import { useStore } from '../provider/store-provider';

const ResizeButton: React.FC<{
  className?: string;
  variant: 'quiet' | 'quiet-on-primary';
}> = ({ className, variant }) => {
  const t = useStore((state) => state.language.t);

  return (
    <>
      <Button
        size="icon"
        variant={variant}
        aria-label={t('panel.resize.aria-label')}
        language="ui"
        onPress={() => console.log('resize')}
        className={cn(
          'vi-absolute vi-left-0 vi-top-0 vi-z-10 vi-size-fit vi-rounded-full vi-pl-1 vi-pt-1',
          className
        )}
      >
        <AngleUp size={20} />
      </Button>
    </>
  );
};
ResizeButton.displayName = 'ResizeButton';
export { ResizeButton };
