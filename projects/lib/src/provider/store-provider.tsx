import * as React from 'react';
import { createStore } from '../store';
import { useStore as useZustandStore } from 'zustand';

type StoreConfig = Parameters<typeof createStore>[0];

type StoreContextData = {
  store: WebChatStoreInstance;
};

const StoreContext = React.createContext<StoreContextData | undefined>(
  undefined
);

// used this blog as reference: https://tkdodo.eu/blog/zustand-and-react-context
export function StoreProvider({
  children,
  config,
}: {
  children: React.ReactNode;
  config: StoreConfig;
}) {
  const [store] = React.useState(() => createStore(config));

  return (
    <StoreContext.Provider value={{ store }}>{children}</StoreContext.Provider>
  );
}

export function useStore<T>(selector: (state: WebChatStore) => T): T {
  const context = React.useContext(StoreContext);
  if (!context) {
    throw new Error('useStore must be used within a StoreProvider');
  }
  return useZustandStore(context.store, selector);
}

export function useRawStore() {
  const context = React.useContext(StoreContext);
  if (!context) {
    throw new Error('useStore must be used within a StoreProvider');
  }
  return context.store;
}

export function useSubscribe<T>(
  selector: (state: WebChatStore) => T,
  listener: (selectedState: T, previousSelectedState: T) => void,
  options?:
    | {
        equalityFn?: ((a: T, b: T) => boolean) | undefined;
        fireImmediately?: boolean;
      }
    | undefined
): () => void {
  const context = React.useContext(StoreContext);
  if (!context) {
    throw new Error('useStore must be used within a StoreProvider');
  }
  return context.store.subscribe(selector, listener, options);
}
