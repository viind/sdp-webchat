import React from 'react';
import { Platform } from 'react-native';
import { useColorScheme } from '~/lib/useColorScheme';
import { cn } from '~/lib/utils';
import { useStore } from '~/src/provider/store-provider';

export function ThemeProvider({ children }: { children: React.ReactNode }) {
  const { isDarkColorScheme } = useColorScheme();
  const error = useStore((state) => state.logger.error);
  const cssVariables = useStore((state) => state.theme.variables);
  if (Platform.OS !== 'web') {
    error('WebCssVariableSetter: only supported on web');
    return <>{children}</>;
  }

  return (
    <div
      style={cssVariables}
      className={cn('vi-font-[Roboto]', isDarkColorScheme && 'dark')}
    >
      {children}
    </div>
  );
}
