import * as React from 'react';
import { z } from 'zod';
import { useManagedStorage } from '~/src/provider/storage-provider';
import { useStore } from '~/src/provider/store-provider';

declare global {
  interface KnownStorage {
    sidePanel: SidePanel;
  }
}

const sidePanelSchema = z.object({
  showCloseIcon: z.boolean(),
});

export const sidePanelConfigSchema = z.object({
  sidePanelClosedOnFirstOpen: z.boolean().optional(),
});

type SidePanel = z.infer<typeof sidePanelSchema>;

type SidePanelContextData = {
  toggleOpenState: () => void;
  openSidePanel: () => void;
  isSidePanelOpen: boolean;
};

const SidePanelContext = React.createContext<SidePanelContextData | undefined>(
  undefined
);

export function SidePanelProvider({
  children,
  sidePanelClosedOnFirstOpen,
}: {
  children: React.ReactNode;
} & z.infer<typeof sidePanelConfigSchema>) {
  const [sidePanel, setSidePanel] = useManagedStorage({
    key: 'sidePanel',
    initialValue: {
      showCloseIcon: !sidePanelClosedOnFirstOpen,
    },
    schema: sidePanelSchema,
  });
  const { debug } = useStore((state) => state.logger);

  return (
    <SidePanelContext.Provider
      value={{
        toggleOpenState: () =>
          setSidePanel((prev) => {
            debug(
              `SidePanelProvider: ${prev.showCloseIcon ? 'open' : 'close'} side bar`
            );
            return { ...prev, showCloseIcon: !prev.showCloseIcon };
          }),
        openSidePanel: () =>
          setSidePanel((prev) => {
            if (prev.showCloseIcon) return prev;

            debug(`SidePanelProvider: open side bar`);
            return { ...prev, showCloseIcon: true };
          }),
        isSidePanelOpen: sidePanel.showCloseIcon,
      }}
    >
      {children}
    </SidePanelContext.Provider>
  );
}

export function useSidePanel() {
  const context = React.useContext(SidePanelContext);
  if (!context) {
    throw new Error("Can't use useSidePanel outside of SidePanelProvider");
  }
  return context;
}
