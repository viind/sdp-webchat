import * as React from 'react';
import { z } from 'zod';
import { useManagedStorage } from '~/src/provider/storage-provider';

declare global {
  interface KnownStorage {
    desktopFullScreen: z.infer<typeof fullScreenSchema>;
  }
}

const fullScreenSchema = z.boolean();

type FullscreenContextData = {
  toggleFullScreen: () => void;
  isFullscreen: boolean;
};

const FullscreenContext = React.createContext<
  FullscreenContextData | undefined
>(undefined);

export function FullscreenProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [isFullscreen, setIsFullscreen] = useManagedStorage({
    key: 'desktopFullScreen',
    initialValue: false,
    schema: fullScreenSchema,
  });

  return (
    <FullscreenContext.Provider
      value={{
        toggleFullScreen: () => {
          setIsFullscreen((current) => !current);
        },
        isFullscreen,
      }}
    >
      {children}
    </FullscreenContext.Provider>
  );
}

export function useFullscreen() {
  const context = React.useContext(FullscreenContext);
  if (!context) {
    throw new Error(
      "Can't use useFullscreen outside of FullscreenProvider. Make sure you're using it in a FullscreenProvider."
    );
  }
  return context;
}
