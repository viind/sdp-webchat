import * as React from 'react';
import { z } from 'zod';
import { useFeature, useWindowSize } from './feature-provider';
import { useManagedStorage } from './storage-provider';

declare global {
  interface KnownStorage {
    chatOpen: z.infer<typeof chatOpenSchema>;
  }
}

const chatOpenSchema = z.object({
  isOpen: z.boolean(),
  bigButtonDismissed: z.boolean(),
});

const configSchema = z.object({
  forceSize: z.enum(['big', 'small']).optional(),
  forceOrientation: z.enum(['horizontal', 'vertical']).optional(),
  noBigButtonOnMobile: z.boolean().optional(),
  title: z.string().optional(),
  description: z.string().optional(),
});

export const webChatDialogButtonSchema = z.object({
  openButton: configSchema.optional(),
});

type ChatOpenContextData = {
  isOpen: boolean;
  buttonSize: z.infer<typeof configSchema>['forceSize'];
  buttonOrientation: z.infer<typeof configSchema>['forceOrientation'];
  title: z.infer<typeof configSchema>['title'];
  description: z.infer<typeof configSchema>['description'];
  toggleOpen: () => void;
  dismissBigButton: () => void;
};

const ChatOpenContext = React.createContext<ChatOpenContextData | undefined>(
  undefined
);

export function ChatOpenProvider({
  forceSize,
  forceOrientation,
  noBigButtonOnMobile,
  description,
  title,
  children,
}: z.infer<typeof configSchema> & { children: React.ReactNode }) {
  const [openState, setOpenState] = useManagedStorage({
    key: 'chatOpen',
    initialValue: {
      isOpen: false,
      bigButtonDismissed: false,
    },
    schema: chatOpenSchema,
  });
  const windowSize = useWindowSize();
  const { view } = useFeature();

  const recommendedSize =
    forceSize ?? (noBigButtonOnMobile && view === 'mobile' ? 'small' : 'big');

  const buttonSize = openState.bigButtonDismissed ? 'small' : recommendedSize;
  const buttonOrientation =
    forceOrientation ??
    (windowSize.width <= windowSize.height ? 'vertical' : 'horizontal');

  return (
    <ChatOpenContext.Provider
      value={{
        isOpen: openState.isOpen,
        buttonOrientation,
        buttonSize,
        title: title,
        description: description,
        toggleOpen: () => {
          setOpenState((current) => ({
            ...current,
            isOpen: !current.isOpen,
            bigButtonDismissed: true,
          }));
        },
        dismissBigButton: () => {
          setOpenState((current) => ({
            ...current,
            bigButtonDismissed: true,
          }));
        },
      }}
    >
      {children}
    </ChatOpenContext.Provider>
  );
}

export function useChatOpen() {
  const context = React.useContext(ChatOpenContext);
  if (!context) {
    throw new Error(
      "Can't use useChatOpen outside of ChatOpenProvider. Make sure you're using it in a ChatOpenProvider."
    );
  }
  return context;
}
