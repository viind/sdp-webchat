import * as React from 'react';
import { Dimensions, type ScaledSize } from 'react-native';
import { z } from 'zod';
import { useFullscreen } from '~/src/provider/fullscreen-provider';
import { useSidePanel } from '~/src/provider/side-panel-provider';

const breakpoints = {
  width: {
    'mobile': 800,
    'desktop-fullscreen': 960,
  },
  height: {
    'desktop-fullscreen': 770,
    'reduced-mobile': 400,
  },
} as const;

type View = 'mobile' | 'desktop-windowed' | 'desktop-fullscreen' | 'embedded';

export const allowedViewsSchema = z.enum([
  'mobile',
  'desktop-windowed',
  'desktop-fullscreen',
  'desktop',
]);

type FeatureContextData = {
  showHeaderFullScreen: boolean;
  showHeaderResizeWindow: boolean;
  showHeaderShowViindy: boolean;
  showHeaderClose: boolean;
  showSmallHeaderClose: boolean;
  showHeaderDescription: boolean;
  chatFooterInScrollArea: boolean;
  dontShowIframe: boolean;
  isEmbedded: boolean;
  view: View;
};
type AllFeatures = Omit<FeatureContextData, 'view'>;

const FeatureContext = React.createContext<FeatureContextData | undefined>(
  undefined
);

type PossibleDependencies =
  | 'isSidePanelOpen'
  | 'allowFullscreenSwitch'
  | 'embedded'
  | 'reducedMobileView';
type Inputs = {
  isSidePanelOpen: boolean;
  allowFullscreenSwitch: boolean;
  isFullscreen: boolean;
  view: View;
  possibleOtherViews: View[];
  embedded: boolean;
};
type FeatureConfig = Record<
  keyof AllFeatures,
  | boolean
  | PossibleDependencies
  | `not-${PossibleDependencies}`
  | ((inputs: Inputs) => boolean)
>;

const viewFeatureToggle = {
  'mobile': {
    showHeaderFullScreen: false,
    showHeaderResizeWindow: false,
    showHeaderShowViindy: 'not-reducedMobileView',
    showHeaderDescription: 'not-reducedMobileView',
    showSmallHeaderClose: 'reducedMobileView',
    chatFooterInScrollArea: 'reducedMobileView',
    dontShowIframe: 'reducedMobileView',
    showHeaderClose: true,
    isEmbedded: 'embedded',
  },
  'desktop-windowed': {
    /**
     * The following features are not yet implemented.
     * But this should be the config
     */
    // showHeaderResizeWindow: 'not-isSidePanelOpen',
    showHeaderResizeWindow: false,
    showHeaderFullScreen: ({
      allowFullscreenSwitch,
      possibleOtherViews,
      view,
    }) =>
      possibleOtherViews.includes('desktop-windowed') ||
      view === 'desktop-windowed'
        ? allowFullscreenSwitch
        : false,
    showHeaderShowViindy: 'not-isSidePanelOpen',
    showHeaderDescription: true,
    showSmallHeaderClose: false,
    showHeaderClose: false,
    chatFooterInScrollArea: false,
    dontShowIframe: false,
    isEmbedded: 'embedded',
  },
  'desktop-fullscreen': {
    /**
     * The following features are not yet implemented.
     * But this should be the config
     */
    // showHeaderResizeWindow: 'not-isSidePanelOpen',
    showHeaderResizeWindow: false,
    showHeaderFullScreen: ({
      allowFullscreenSwitch,
      possibleOtherViews,
      view,
    }) =>
      possibleOtherViews.includes('desktop-windowed') ||
      possibleOtherViews.includes('embedded') ||
      view === 'desktop-windowed'
        ? allowFullscreenSwitch
        : false,
    showHeaderShowViindy: false,
    showHeaderDescription: true,
    showSmallHeaderClose: false,
    showHeaderClose: true,
    chatFooterInScrollArea: false,
    dontShowIframe: false,
    isEmbedded: 'embedded',
  },
  'embedded': {
    showHeaderShowViindy: true,
    showHeaderResizeWindow: false,
    showHeaderFullScreen: true,
    showHeaderDescription: true,
    showSmallHeaderClose: false,
    showHeaderClose: false,
    chatFooterInScrollArea: false,
    dontShowIframe: false,
    isEmbedded: 'embedded',
  },
} satisfies Record<FeatureContextData['view'], FeatureConfig>;

export function FeatureProvider({
  children,
  forcedView,
  embedded,
}: {
  children: React.ReactNode;
  forcedView?: z.infer<typeof allowedViewsSchema> | undefined;
  embedded?: boolean;
}) {
  const { isFullscreen } = useFullscreen();
  const { width: windowWidth, height: windowHeight } = useWindowSize();
  const [allowFullscreenSwitch] = React.useState(
    forcedView === undefined || forcedView === 'desktop'
  );
  const { isSidePanelOpen } = useSidePanel();

  const { usedView, possibleOtherViews } = React.useMemo(
    () =>
      defineView({
        embedded: embedded ?? false,
        isFullscreen,
        windowWidth,
        windowHeight,
        forcedView,
      }),
    [isFullscreen, windowWidth, windowHeight, forcedView]
  );

  const features = React.useMemo(
    () =>
      convertToFeatureDictionary(
        viewFeatureToggle[usedView],
        {
          isSidePanelOpen,
          allowFullscreenSwitch,
          embedded: embedded ?? false,
          reducedMobileView:
            windowHeight < breakpoints.height['reduced-mobile'],
        },
        {
          isSidePanelOpen,
          allowFullscreenSwitch,
          isFullscreen,
          view: usedView,
          possibleOtherViews: possibleOtherViews,
          embedded: embedded ?? false,
        }
      ),
    [
      usedView,
      possibleOtherViews,
      isSidePanelOpen,
      allowFullscreenSwitch,
      isFullscreen,
    ]
  );

  return (
    <FeatureContext.Provider
      value={{
        ...features,
        view: usedView,
      }}
    >
      {children}
    </FeatureContext.Provider>
  );
}

/**
 * decides which view should be shown.
 */
function defineView({
  embedded,
  isFullscreen,
  windowWidth,
  windowHeight,
  forcedView,
}: {
  embedded: boolean;
  isFullscreen: boolean;
  windowWidth: number;
  windowHeight: number;
  forcedView?: z.infer<typeof allowedViewsSchema>;
}): {
  usedView: FeatureContextData['view'];
  possibleOtherViews: FeatureContextData['view'][];
} {
  if (embedded) {
    if (isFullscreen) {
      return {
        usedView: 'desktop-fullscreen',
        possibleOtherViews: ['embedded'],
      };
    }
    return { usedView: 'embedded', possibleOtherViews: ['desktop-fullscreen'] };
  }

  if (forcedView === 'mobile') {
    return { usedView: 'mobile', possibleOtherViews: [] };
  }
  if (forcedView === 'desktop-fullscreen') {
    return { usedView: 'desktop-fullscreen', possibleOtherViews: [] };
  }
  if (forcedView === 'desktop-windowed') {
    return { usedView: 'desktop-windowed', possibleOtherViews: [] };
  }
  if (forcedView === 'desktop') {
    // allow switching between fullscreen and windowed

    if (!fitsDesktopWindowedView(windowWidth, windowHeight)) {
      return { usedView: 'desktop-fullscreen', possibleOtherViews: [] };
    }

    if (isFullscreen) {
      return {
        usedView: 'desktop-fullscreen',
        possibleOtherViews: ['desktop-windowed'],
      };
    }
    return {
      usedView: 'desktop-windowed',
      possibleOtherViews: ['desktop-fullscreen'],
    };
  }

  if (windowWidth < breakpoints.width.mobile) {
    return { usedView: 'mobile', possibleOtherViews: [] };
  }

  if (!fitsDesktopWindowedView(windowWidth, windowHeight)) {
    return { usedView: 'desktop-fullscreen', possibleOtherViews: ['mobile'] };
  }

  if (isFullscreen) {
    return {
      usedView: 'desktop-fullscreen',
      possibleOtherViews: ['desktop-windowed', 'mobile'],
    };
  }
  return {
    usedView: 'desktop-windowed',
    possibleOtherViews: ['desktop-fullscreen', 'mobile'],
  };
}

function fitsDesktopWindowedView(windowWidth: number, windowHeight: number) {
  return (
    windowWidth >= breakpoints.width['desktop-fullscreen'] &&
    windowHeight >= breakpoints.height['desktop-fullscreen']
  );
}

function convertToFeatureDictionary(
  featureConfig: FeatureConfig,
  PossibleDependencies: Record<PossibleDependencies, boolean>,
  inputs: Inputs
) {
  const result = {} as Record<keyof AllFeatures, boolean>;

  for (const [key, value] of Object.entries(featureConfig) as [
    keyof AllFeatures,
    FeatureConfig[keyof AllFeatures],
  ][]) {
    if (typeof value === 'boolean') {
      result[key] = value;
      continue;
    }

    if (typeof value === 'function') {
      result[key] = value(inputs);
      continue;
    }

    const invert = value.startsWith('not-');
    if (invert) {
      const actualValue = value.replace('not-', '') as PossibleDependencies;
      result[key] = !PossibleDependencies[actualValue];
      continue;
    }

    result[key] = PossibleDependencies[value as PossibleDependencies];
  }

  return result;
}

export function useFeature() {
  const context = React.useContext(FeatureContext);
  if (!context) {
    throw new Error(
      "Can't use feature hook outside of FeatureProvider. Make sure you're using it in a FeatureProvider."
    );
  }
  return context;
}

export function useWindowSize() {
  const [windowSize, setWindowSize] = React.useState<ScaledSize>(
    Dimensions.get('window')
  );

  React.useEffect(() => {
    const sub = Dimensions.addEventListener('change', ({ window }) =>
      setWindowSize(window)
    );

    return () => {
      sub.remove();
    };
  }, []);

  return windowSize;
}
