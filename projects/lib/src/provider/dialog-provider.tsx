import React from 'react';
import { AccessibilityInfo, type TextInput } from 'react-native';
import { z } from 'zod';
import { useRawStore, useStore } from '~/src/provider/store-provider';
import { updateGlobalUserMessage } from '../hooks/update-global-user-message';
import { Socket, type BotSocketMessage } from '../service/socket';
import { useStorage } from './storage-provider';
import { focusComponent } from '../utils';

declare global {
  interface KnownStorage {
    dialog: Dialog;
    sessionId: z.infer<typeof sessionIdSchema>;
  }
}

export type Dialog = z.infer<typeof dialogSchema>;
export type Message = z.infer<typeof messageSchema>;
export type BotButtons = Extract<Message, { type: 'bot-buttons' }>;
export type BotMessage = Extract<Message, { type: 'bot-message' }>;
export type SmallCornerVariant = z.infer<typeof smallCornerVariantSchema>;

const sessionIdSchema = z.string();

const smallCornerVariantSchema = z.union([
  z.literal('none'),
  z.literal('top'),
  z.literal('bottom'),
  z.literal('both'),
]);

const likeSchema = z.union([z.literal('like'), z.literal('dislike')]);

const messageSchema = z.union([
  z.object({
    type: z.literal('bot-message'),
    message: z.string(),
    id: z.string(),
    smallCorner: smallCornerVariantSchema.optional(),
    like: likeSchema.optional(),
    feedbackProvided: z.boolean().optional(),
    language: z.string(),
  }),
  z.object({
    type: z.literal('bot-buttons'),
    buttons: z.array(
      z.object({
        type: z.literal('bot-button'),
        message: z.string(),
        id: z.string(),
        enabled: z.boolean(),
        payload: z.string(),
        language: z.string(),
      })
    ),
  }),
  z.object({
    type: z.literal('user-message'),
    message: z.string(),
    id: z.string(),
    smallCorner: smallCornerVariantSchema.optional(),
    language: z.string(),
  }),
]);

const dialogSchema = z.array(
  z.union([
    z.object({
      type: z.literal('date'),
      date: z.string(),
    }),
    messageSchema,
    z.object({
      type: z.literal('group'),
      messages: z.array(messageSchema),
    }),
  ])
);

type DialogContextData = {
  entries: Dialog;
  numberOfMessages: number;
  size: 'small' | 'big';
  showTyping: boolean;
  chatReference: React.RefObject<TextInput>;
  addUserMessage: (message: string) => void;
  onBotButtonPress: (message: string, payload: string) => void;
  onLikePress: (
    messageId: string,
    pressedOn: z.infer<typeof likeSchema>
  ) => void;
  startChat(): Promise<void>;
  stopChat(): void;
  provideFeedback: (messageId: string, text: string) => void;
  changeSize: (size: 'small' | 'big') => void;
};

const DialogContext = React.createContext<DialogContextData | undefined>(
  undefined
);

export const dialogProviderSchema = z.object({
  socketUrl: z.string(),
  socketPath: z.string(),
  customerId: z.string(),
  initPayload: z.string().default('Hallo'),
});

export function DialogProvider({
  children,
  socketPath,
  socketUrl,
  customerId,
  initPayload,
}: {
  children: React.ReactNode;
} & z.infer<typeof dialogProviderSchema>) {
  const [entries, setEntries] = React.useState<Dialog>([]);
  const [readStorageComplete, setReadStorageComplete] = React.useState(false);
  const [{ readPromise, resolveReadPromise }] = React.useState<{
    readPromise: Promise<void>;
    resolveReadPromise: () => void;
  }>(() => {
    let resolve: (() => void) | undefined = undefined;
    const promise = new Promise<void>((res) => {
      resolve = res;
    });

    if (resolve === undefined) {
      throw new Error('resolve is undefined');
    }
    return { readPromise: promise, resolveReadPromise: resolve };
  });
  // `idCounter` are used for unique key properties to render as a list
  const [idCounter, setIdCounter] = React.useState(1);
  const [numberOfMessages, setNumberOfMessages] = React.useState(0);
  const [sessionId, setSessionId] = React.useState<string | undefined>(
    undefined
  );
  const { read, write } = useStorage();
  const store = useRawStore();
  const logger = useStore((state) => state.logger);
  const { error, debug, trace } = logger;
  const socketRef = React.useRef(new Socket(socketUrl, socketPath, logger));
  const [size, setSize] = React.useState<'small' | 'big'>('small');
  const openIframe = useStore((state) => state.iframe.openIframe);
  const iframeConfig = useStore((state) => state.iframe.iframeConfig);
  const [ready, setReady] = React.useState(false);
  const [showTyping, enableTyping, disableTyping] = intervalToggle(20);
  const [chatLanguage, setChatLanguage] = React.useState<string>(
    store.getState().language.chatLanguage
  );
  const chatReference = React.useRef<TextInput>(null);

  React.useEffect(() => {
    // we can't wait withing a `useEffect` call so we are not waiting for the promise to resolve
    void Promise.resolve()
      .then(async () => {
        const storageData = await read('dialog', dialogSchema);
        if (storageData === undefined) {
          return;
        }

        setEntries(addDateIfNeeded(storageData));
        debug('DialogProvider: Found dialog data in storage.');
        trace('DialogProvider: Storage data:', storageData);

        const biggestId = findBiggestId(storageData);
        setIdCounter(biggestId + 1);
        debug(`DialogProvider: Biggest id found in storage: ${biggestId}`);
      })
      .then(async () => {
        const storageData = await read('sessionId', sessionIdSchema);
        setSessionId(storageData);
      })
      .then(() => {
        trace('DialogProvider: finished initialization');
        setReadStorageComplete(true);
      })
      .catch((e: unknown) => error('DialogProvider: initialization failed', e))
      .then(() => resolveReadPromise());

    socketRef.current.on('sessionConfirmed', (id) => setSessionId(id));
    socketRef.current.on('readyChanged', (ready) => setReady(ready));

    // temporary solution, until the dialog provider is converted to a zustand store
    store.subscribe(
      (state) => ({
        name: state.language.chatLanguageName,
        key: state.language.chatLanguage,
      }),
      (newChatLanguage, previous) => {
        if (
          newChatLanguage.name === previous.name &&
          newChatLanguage.key === previous.key
        ) {
          return;
        }
        addUserMessage(newChatLanguage.name, newChatLanguage.key);
      }
    );
    store.subscribe(
      (state) => state.language.chatLanguage,
      (newChatLanguage) => {
        setChatLanguage(newChatLanguage);
      }
    );
  }, []);

  React.useEffect(() => {
    const onBotMessage = (message: BotSocketMessage) =>
      setEntries(updateDialogWithNewBotMessage(message, chatLanguage, error));
    socketRef.current.on('botMessage', onBotMessage);
    return () => socketRef.current.off('botMessage', onBotMessage);
  }, [chatLanguage]);

  React.useEffect(() => {
    const onBotMessage = () => disableTyping();
    socketRef.current.on('botMessage', onBotMessage);
    return () => socketRef.current.off('botMessage', onBotMessage);
  }, [disableTyping]);

  React.useEffect(() => {
    if (ready === false) {
      return;
    }

    const {
      chatLanguageName,
      hasDefaultLanguage,
      enabledLanguages,
      userChangedLanguage,
    } = store.getState().language;

    // send the name of the language instead of the initial payload if its not the default language
    const actualMessage = hasDefaultLanguage ? initPayload : chatLanguageName;
    const userMessages = entries.filter((x) => x.type === 'user-message');

    if (userMessages.length === 0) {
      // if there is nothing, then send the initial message
      return addUserMessage(actualMessage);
    }

    if (userChangedLanguage) {
      // if the user actively has changed the language of the chat, then we won't initialize a new conversation
      // in case that the homepage changes the defaultLanguage
      return;
    }

    const lastUserMessage = userMessages.at(-1);
    if (lastUserMessage?.message === actualMessage) {
      // the current default language was already applied, so no need to reapply it
      return;
    }

    const languageNames = Object.values(enabledLanguages).filter(
      (x) => Array.isArray(x) === false
    );
    const messagesThatAreNotLanguageOrInitPayload = userMessages
      .map((x) => x.message)
      .filter((x) => languageNames.includes(x) === false)
      .filter((x) => x !== initPayload);

    if (messagesThatAreNotLanguageOrInitPayload.length > 0) {
      // As there was at least one active message from the user
      // we should not initialize a new conversation
      return;
    }

    addUserMessage(actualMessage);
  }, [ready, entries]);

  React.useEffect(() => {
    setNumberOfMessages(countMessages(entries));
    if (readStorageComplete === false) {
      return;
    }

    void write('dialog', entries);
  }, [entries]);

  React.useEffect(() => {
    if (sessionId === undefined || readStorageComplete === false) {
      return;
    }

    void write('sessionId', sessionId);
  }, [sessionId]);

  const addUserMessage = React.useCallback(
    (message: string, language?: string) => {
      const actualMessage = message.trim();
      if (actualMessage.length === 0) {
        trace(`DialogProvider: Skipping empty user message`);
        return;
      }
      enableTyping();
      const newMessage: Message = {
        type: 'user-message',
        id: `id${idCounter}`,
        message: actualMessage,
        smallCorner: 'bottom',
        language: language ?? chatLanguage,
      };
      socketRef.current.sendUserMessage(actualMessage, {
        iframeUrl: iframeConfig?.url,
      });
      setIdCounter((idCounter) => idCounter + 1);

      setEntries((currentEntries) =>
        addMessageToDialog(addDateIfNeeded(currentEntries), newMessage, error)
      );
    },
    [error, trace, enableTyping, idCounter, socketRef.current, iframeConfig]
  );

  // false positive
  // eslint-disable-next-line react-compiler/react-compiler
  updateGlobalUserMessage(addUserMessage);

  const onBotButtonPress = (message: string, payload: string) => {
    focusComponent(chatReference.current);
    const newMessage: Message = {
      type: 'user-message',
      id: `id${idCounter}`,
      message,
      smallCorner: 'bottom',
      language: chatLanguage,
    };
    setIdCounter((idCounter) => idCounter + 1);

    const newIframeConfig = parsePayloadAsIframeConfig(payload, error);
    if (newIframeConfig !== undefined) {
      // for now, no message is send to the backend upon opening an iframe
      openIframe(newIframeConfig);
    } else {
      // it is intentional that the message is shown to the user and the payload is actually send to the server
      socketRef.current.sendUserMessage(payload, {
        iframeUrl: iframeConfig?.url,
      });
      enableTyping();
    }

    setEntries((currentEntries) =>
      addMessageToDialog(currentEntries, newMessage, error)
    );
  };

  const onLikePress = (
    messageId: string,
    pressedOn: z.infer<typeof likeSchema>
  ) => {
    const entry = findById(entries, messageId);
    if (entry === undefined) {
      error(
        'DialogProvider: Failed to like a bot message, because could not find message with id',
        messageId
      );
      return;
    }
    if (entry.type !== 'bot-message') {
      error(
        `DialogProvider: Failed to like a bot message, because the provided message id ${messageId} is not a bot message`
      );
      return;
    }

    if (entry.like === pressedOn) {
      debug(`DialogProvider: Resetting like state for ${messageId}`);
      entry.like = undefined;
      entry.feedbackProvided = false;
    } else {
      debug(`DialogProvider: Like pressed for ${messageId}`);
      entry.like = pressedOn;
    }

    socketRef.current.sendFeedback({
      reaction: entry.like,
      messageId: entry.id,
    });
    setEntries((entries) => [...entries]);
  };

  const provideFeedback = (messageId: string, text: string) => {
    if (text.length === 0) {
      trace(`DialogProvider: Skipping empty feedback text`);
      return;
    }

    const entry = findById(entries, messageId);
    if (entry === undefined) {
      error(
        'DialogProvider: Failed to like a bot message, because could not find message with id',
        messageId
      );
      return;
    }
    if (entry.type !== 'bot-message') {
      error(
        `DialogProvider: Failed to like a bot message, because the provided message id ${messageId} is not a bot message`
      );
      return;
    }
    if (entry.like === undefined) {
      error(
        `DialogProvider: Failed to provide feedback text to a bot message without like state, because the provided message id ${messageId} does not have a like state`
      );
      return;
    }

    entry.feedbackProvided = true;
    socketRef.current.sendFeedback({
      reason: text,
      reaction: entry.like,
      messageId,
    });
    setEntries((entries) => [...entries]);
  };

  const addDateIfNeeded = (entries: Dialog): Dialog => {
    // todo: ensure that it is correctly localized
    const todaysDate = new Date().toLocaleDateString('de-DE');
    const allDates = entries.filter((entry) => entry.type === 'date');
    if (allDates.length === 0) {
      return [...entries, { type: 'date', date: todaysDate }];
    }

    const hasTodaysDate = allDates.some((entry) => entry.date === todaysDate);
    if (!hasTodaysDate) {
      return [...entries, { type: 'date', date: todaysDate }];
    }
    return entries;
  };

  return (
    <DialogContext.Provider
      value={{
        size,
        entries,
        numberOfMessages,
        showTyping,
        chatReference,
        addUserMessage,
        onBotButtonPress,
        onLikePress,
        provideFeedback,
        startChat: async () => {
          return readPromise.then(() => {
            if (socketRef.current.connected) return;

            socketRef.current.connect({
              sessionId,
              customerId,
            });
          });
        },
        stopChat: () => {
          socketRef.current.disconnect();
        },
        changeSize: setSize,
      }}
    >
      {children}
    </DialogContext.Provider>
  );
}

export function useDialog() {
  const context = React.useContext(DialogContext);
  if (!context) {
    throw new Error(
      "Can't use Dialog hook outside of DialogProvider. Make sure you're using it in a DialogProvider."
    );
  }

  return context;
}

function findById(entries: Dialog, id: string) {
  for (const entry of entries) {
    if (entry.type === 'date') {
      continue;
    }
    if (entry.type === 'bot-buttons') {
      for (const button of entry.buttons) {
        if (button.id === id) {
          return button;
        }
      }
      continue;
    }
    if (entry.type === 'group') {
      for (const message of entry.messages) {
        if (message.type === 'bot-buttons') {
          for (const button of message.buttons) {
            if (button.id === id) {
              return button;
            }
          }
          continue;
        }
        if (message.id === id) {
          return message;
        }
      }
      continue;
    }
    if (entry.id === id) {
      return entry;
    }
  }
  return undefined;
}

function countMessages(entries: Dialog) {
  let count = 0;
  for (const entry of entries) {
    if (entry.type === 'date') {
      continue;
    }
    if (entry.type === 'bot-buttons') {
      count += entry.buttons.length;
      continue;
    }
    if (entry.type === 'group') {
      count += entry.messages.length;
      continue;
    }
    count++;
  }
  return count;
}

function findBiggestId(entries: Dialog) {
  return Math.max(
    /**
     * In case of no entries, the biggest id is 0.
     * Without this, the function would return Infinity.
     */
    0,
    ...entries
      .flatMap((entry) => {
        if (entry.type === 'date' || entry.type === 'bot-message') {
          // bot messages have uuid's as an ID and therefore are not considered
          return [];
        }

        if (entry.type === 'bot-buttons') {
          return entry.buttons.map((x) => x.id);
        }

        if (entry.type === 'group') {
          return entry.messages.flatMap((x) => {
            if (x.type === 'bot-message') {
              // bot messages have uuid's as an ID and therefore are not considered
              return [];
            }
            if (x.type === 'bot-buttons') {
              return x.buttons.map((y) => y.id);
            }
            return [x.id];
          });
        }
        return [entry.id];
      })
      .map((id) => Number(id.replace('id', '')))
  );
}

function updateDialogWithNewBotMessage(
  message: BotSocketMessage,
  chatLanguage: string,
  error: WebChatStore['logger']['error']
) {
  return (currentEntries: Dialog) => {
    let updatedEntries = addMessageToDialog(
      currentEntries,
      {
        type: 'bot-message',
        id: message.id,
        message: message.text,
        smallCorner: 'bottom',
        language: chatLanguage,
      },
      error
    );

    // there can be quick replies with empty title or payload
    // we don't want to show those
    message.quick_replies = message.quick_replies.filter(
      (x) => x.title !== '' && x.payload !== ''
    );

    // todo: the text should be cleaned of any markdown syntax
    // maybe there is an alternative way to solve this here (or it is not needed)
    AccessibilityInfo.announceForAccessibility(`Bot: ${message.text}`);
    if (message.quick_replies.length > 0) {
      updatedEntries = addMessageToDialog(
        updatedEntries,
        {
          type: 'bot-buttons',
          buttons: message.quick_replies.map((x) => ({
            type: 'bot-button',
            message: x.title,
            // as bot buttons are very temporary, we can just use the payload as the id
            id: x.payload,
            enabled: true,
            payload: x.payload,
            language: chatLanguage,
          })),
        },
        error
      );
      AccessibilityInfo.announceForAccessibility(
        `Bot stellt Vorschläge bereit: ${message.quick_replies.map((x) => x.payload).join(', ')}`
      );
    }
    return updatedEntries;
  };
}

function addMessageToDialog(
  entries: Dialog,
  newMessage: Message,
  error: WebChatStore['logger']['error']
): Dialog {
  // remove all bot buttons upon user message
  const consideredEntries =
    newMessage.type === 'user-message'
      ? entries.filter((x) => x.type !== 'bot-buttons')
      : entries;

  if (
    newMessage.type === 'user-message' &&
    consideredEntries.length !== entries.length
  ) {
    // ensure the user is up to date about the buttons being removed
    AccessibilityInfo.announceForAccessibility(
      `Vorschlag '${newMessage.message}' gesendet. Andere Vorschläge wurden entfernt.`
    );
  }

  const lastEntry = consideredEntries.pop();
  if (lastEntry === undefined) {
    return [newMessage];
  }

  if (newMessage.type === 'bot-buttons') {
    return [...consideredEntries, lastEntry, newMessage];
  }

  const oppositeType =
    newMessage.type === 'bot-message' ? 'user-message' : 'bot-message';
  if (
    lastEntry.type === 'date' ||
    lastEntry.type === 'bot-buttons' ||
    lastEntry.type === oppositeType
  ) {
    return [...consideredEntries, lastEntry, newMessage];
  }

  if (lastEntry.type === 'group') {
    const lastMessage = lastEntry.messages[lastEntry.messages.length - 1];
    if (lastMessage === undefined) {
      const message =
        'DialogProvider: The first message of a group should never be undefined';
      error(message, lastEntry);
      throw new Error(message);
    }

    if (lastMessage.type === newMessage.type) {
      lastEntry.messages.push(newMessage);
      lastMessage.smallCorner = 'both';
      newMessage.smallCorner = 'top';
      return [...consideredEntries, lastEntry];
    } else {
      return [...consideredEntries, lastEntry, newMessage];
    }
  }

  if (lastEntry.type === newMessage.type) {
    lastEntry.smallCorner = 'bottom';
    newMessage.smallCorner = 'top';

    return [
      ...consideredEntries,
      {
        type: 'group',
        messages: [{ ...lastEntry }, newMessage],
      },
    ];
  }

  const message = `DialogProvider: message should have been added by this point. Message is dropped now`;
  error(message, newMessage);
  throw new Error(message);
}

function parsePayloadAsIframeConfig(
  payload: string,
  error: WebChatStore['logger']['error']
): { pageTitle: string; url: string } | undefined {
  // short circuit if the payload does not start with the expected prefix
  if (payload.startsWith('webchat_hook[') === false) return undefined;

  const matches = [...payload.matchAll(/"(.*?)"/g)];
  const [titleMatch, urlMatch] = matches;
  if (
    matches.length !== 2 ||
    titleMatch === undefined ||
    urlMatch === undefined
  ) {
    error(
      `DialogProvider: Failed to parse payload as iframe config. Expected 2 values, but got ${matches.length}`,
      payload,
      matches
    );
    return undefined;
  }

  const title = titleMatch[1];
  const url = urlMatch[1];

  if (title === undefined || url === undefined) {
    error(
      `DialogProvider: Failed ot parse payload as iframe config. But were unable to match a title or url without quotes`,
      payload,
      matches,
      { title, url }
    );
    return undefined;
  }

  return { pageTitle: title, url: url };
}

function intervalToggle(intervalInSeconds: number) {
  const [value, setValue] = React.useState(false);
  const [intervalId, setIntervalId] = React.useState<NodeJS.Timer | null>(null);

  const disable = React.useCallback(() => {
    setValue(false);
    if (intervalId !== null) {
      clearInterval(intervalId);
      setIntervalId(null);
    }
  }, [intervalId]);

  const enable = React.useCallback(() => {
    setValue(true);
    if (intervalId !== null) {
      clearInterval(intervalId);
    }
    setIntervalId(setInterval(() => disable(), intervalInSeconds * 1000));
  }, [intervalId]);

  return [value, enable, disable] as const;
}
