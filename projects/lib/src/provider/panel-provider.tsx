import * as React from 'react';
import { z } from 'zod';
import { useManagedStorage } from '~/src/provider/storage-provider';
import { useFeature } from './feature-provider';
import { useStore } from '~/src/provider/store-provider';
import { useSidePanel } from './side-panel-provider';

declare global {
  interface KnownStorage {
    panelSelection: z.infer<typeof panelSelectionSchema>;
  }
}
const panelSelectionSchema = z.union([
  z.literal('chat'),
  z.literal('viindy'),
  z.literal('language'),
  z.literal('channel'),
  z.literal('info'),
  z.literal('iframe'),
]);

type PanelConfig = {
  showToggleButton: boolean;
  orientation: 'left' | 'bottom';
  size: 'normal' | 'big';
  options: z.infer<typeof panelSelectionSchema>[];
  view: 'mobile' | 'desktop-windowed' | 'desktop-fullscreen';
  defaultView: z.infer<typeof panelSelectionSchema>;
};

type Panel = {
  selection: z.infer<typeof panelSelectionSchema>;
  config: PanelConfig;
};
type PanelItem = NonNullable<Panel['selection']>;

type PanelContextData = {
  clickOnItem: (item: PanelItem) => void;
  panel: Panel;
};

const PanelContext = React.createContext<PanelContextData | undefined>(
  undefined
);

const panelConfig = {
  'mobile': {
    orientation: 'bottom',
    size: 'normal',
    showToggleButton: false,
    options: ['chat', 'viindy', 'iframe', 'language', 'channel', 'info'],
    view: 'mobile',
    defaultView: 'chat',
  },
  'embedded': {
    orientation: 'bottom',
    size: 'normal',
    showToggleButton: false,
    options: ['chat', 'viindy', 'iframe', 'language', 'channel', 'info'],
    view: 'mobile',
    defaultView: 'chat',
  },
  'desktop-windowed': {
    orientation: 'left',
    size: 'normal',
    showToggleButton: true,
    options: ['viindy', 'iframe', 'language', 'channel', 'info'],
    view: 'desktop-windowed',
    defaultView: 'viindy',
  },
  'desktop-fullscreen': {
    orientation: 'left',
    size: 'big',
    showToggleButton: false,
    options: ['viindy', 'iframe', 'language', 'channel', 'info'],
    view: 'desktop-fullscreen',
    defaultView: 'viindy',
  },
} satisfies Record<ReturnType<typeof useFeature>['view'], PanelConfig>;

export function PanelProvider({ children }: { children: React.ReactNode }) {
  const { view } = useFeature();
  const { openSidePanel } = useSidePanel();
  const showChannelIcon = useStore((state) => state.channel.showChannelIcon);
  const showLanguageMenu = useStore((state) => state.language.showLanguageMenu);
  const debug = useStore((state) => state.logger.debug);
  const isIframeOpen = useStore((state) => state.iframe.isIframeOpen);
  const currentPanelConfig = { ...panelConfig[view] };

  const [selection, setSelection] = useManagedStorage({
    key: 'panelSelection',
    initialValue: currentPanelConfig.defaultView,
    schema: panelSelectionSchema,
  });

  currentPanelConfig.options = currentPanelConfig.options.filter((x) => {
    if (showLanguageMenu === false && x === 'language') {
      return false;
    }
    if (showChannelIcon === false && x === 'channel') {
      return false;
    }
    if (isIframeOpen && x === 'viindy') {
      return false;
    }
    if (isIframeOpen === false && x === 'iframe') {
      return false;
    }
    return true;
  });

  React.useEffect(() => {
    const validView = currentPanelConfig.options[0]!;

    if (currentPanelConfig.view !== 'mobile' && selection === 'chat') {
      // chat only exists in mobile, so change it to a valid one
      setSelection(validView);
    }

    if (
      (currentPanelConfig.options as string[]).includes(selection) === false
    ) {
      // should only happen if the user opens the webchat again after the configuration was changed
      setSelection(validView);
    }
  }, [view, selection]);

  React.useEffect(() => {
    if (
      selection === 'iframe' &&
      currentPanelConfig.options.includes('viindy')
    ) {
      setSelection('viindy');
    }
    if (
      selection === 'viindy' &&
      currentPanelConfig.options.includes('iframe')
    ) {
      setSelection('iframe');
    }
  }, [currentPanelConfig.options, selection]);

  return (
    <PanelContext.Provider
      value={{
        clickOnItem: (item) => {
          debug(`PanelProvider: select panel item: ${item}`);
          setSelection(item);
          openSidePanel();
        },
        panel: {
          config: currentPanelConfig,
          selection,
        },
      }}
    >
      {children}
    </PanelContext.Provider>
  );
}

export function usePanel() {
  const context = React.useContext(PanelContext);
  if (!context) {
    throw new Error("Can't use usePanel outside of PanelProvider");
  }
  return context;
}
