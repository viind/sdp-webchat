import React from 'react';
import { z } from 'zod';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Platform } from 'react-native';
import { useStore } from '~/src/provider/store-provider';

declare global {
  /**
   * Used to define the known storage keys and its types
   */
  interface KnownStorage {}
}

type StorageContextData = {
  read<T extends keyof KnownStorage>(
    key: T,
    schema: z.ZodType<KnownStorage[T]>
  ): Promise<KnownStorage[T] | undefined>;
  write<T extends keyof KnownStorage>(
    key: T,
    value: KnownStorage[T] | undefined
  ): Promise<void>;
};

const StorageContext = React.createContext<StorageContextData | undefined>(
  undefined
);

export const storageStrategySchema = z
  .union([z.literal('localStorage'), z.literal('sessionStorage')])
  .optional();

/**
 * The strategy is only used on the web and the default is `sessionStorage`.
 */
export function StorageProvider({
  children,
  strategy,
}: {
  children: React.ReactNode;
  strategy: z.infer<typeof storageStrategySchema>;
}) {
  const { error, debug } = useStore((state) => state.logger);

  const readValue =
    Platform.OS !== 'web'
      ? (key: string) => AsyncStorage.getItem(appendPrefix(key))
      : strategy === 'localStorage'
        ? async (key: string) => localStorage.getItem(appendPrefix(key))
        : async (key: string) => sessionStorage.getItem(appendPrefix(key));
  const writeValue =
    Platform.OS !== 'web'
      ? (key: string, value: string) =>
          AsyncStorage.setItem(appendPrefix(key), value)
      : strategy === 'localStorage'
        ? async (key: string, value: string) =>
            localStorage.setItem(appendPrefix(key), value)
        : async (key: string, value: string) =>
            sessionStorage.setItem(appendPrefix(key), value);
  const deleteValue =
    Platform.OS !== 'web'
      ? (key: string) => AsyncStorage.removeItem(appendPrefix(key))
      : strategy === 'localStorage'
        ? async (key: string) => localStorage.removeItem(appendPrefix(key))
        : async (key: string) => sessionStorage.removeItem(appendPrefix(key));

  return (
    <StorageContext.Provider
      value={{
        read: async (key, schema) => {
          let currentValue: string | null;
          try {
            currentValue = await readValue(key);
            if (!currentValue) {
              debug(`StorageProvider: No value found for key ${key}`);
              return undefined;
            }
          } catch (e) {
            error(
              `StorageProvider: Failed to read storage value for key ${key}`,
              e
            );
            return undefined;
          }

          try {
            const data = JSON.parse(currentValue) as unknown;
            return schema.parse(data);
          } catch (e) {
            error(
              `StorageProvider: Failed to parse storage value for key ${key}`,
              e
            );
            return undefined;
          }
        },
        write: async (key, value) => {
          if (value === undefined) {
            return deleteValue(key).catch((e) =>
              error(
                `StorageProvider: Failed to remove storage value for key ${key}`,
                e
              )
            );
          }
          return writeValue(key, JSON.stringify(value)).catch((e) =>
            error(
              `StorageProvider: Failed to write storage value for key ${key}`,
              e
            )
          );
        },
      }}
    >
      {children}
    </StorageContext.Provider>
  );
}

export function useStorage() {
  const context = React.useContext(StorageContext);
  if (!context) {
    throw new Error(
      "Can't use Storage hook outside of StorageProvider. Make sure you're using it in a StorageProvider."
    );
  }

  return context;
}

type InitParams<Key extends keyof KnownStorage> = {
  key: Key;
  initialValue: KnownStorage[Key];
  schema: z.ZodType<KnownStorage[Key]>;
};

export function useManagedStorage<Key extends keyof KnownStorage>({
  key,
  initialValue,
  schema,
}: InitParams<Key>) {
  const { read, write } = useStorage();
  const [value, setValue] = React.useState(initialValue);
  const [readComplete, setReadComplete] = React.useState(false);
  const { error } = useStore((state) => state.logger);

  React.useEffect(() => {
    const readInitialValueFromStorage = async () => {
      try {
        const storedValue = await read(key, schema);
        if (storedValue) {
          setValue(storedValue);
        }
      } catch (err) {
        error(`Failed to initialize storage for key ${key}`, err);
        setValue(initialValue);
      } finally {
        setReadComplete(true);
      }
    };

    void readInitialValueFromStorage();
  }, []);

  React.useEffect(() => {
    if (readComplete) {
      write(key, value).catch((err) => {
        error(`Failed to write value for key ${key}`, err);
      });
    }
  }, [value, write]);

  return [value, setValue, readComplete] as const;
}
function appendPrefix(key: string) {
  return `viind-${key}`;
}
