import * as React from 'react';
import { Image, View, type ImageSourcePropType } from 'react-native';
import type { LanguageOption } from '~/components/primitives/hooks/useLanguage';
import { Text } from '~/components/ui';
import { Strong } from '~/components/ui/strong';
import { cn } from '~/lib/utils';
import { useFeature } from '~/src/provider/feature-provider';
import { useStore } from '~/src/provider/store-provider';

export function Panel({
  children,
  className,
}: {
  children: React.ReactNode;
  className?: string;
}) {
  return (
    <View
      className={cn(
        'vi-size-full vi-w-full vi-flex-row vi-justify-center vi-p-4',
        className
      )}
    >
      <View className="vi-w-full vi-max-w-xl vi-flex-col vi-gap-5">
        {children}
      </View>
    </View>
  );
}

export function PanelHeader({
  children,
  className,
}: {
  children: React.ReactNode;
  className?: string;
}) {
  return (
    <View className={cn('vi-flex vi-flex-col vi-items-center', className)}>
      {children}
    </View>
  );
}

export function PanelTitle({
  title,
  language,
}: {
  title: string;
  language?: LanguageOption;
}) {
  const t = useStore((state) => state.language.t);
  return (
    <Text role="heading" className="vi-text-2xl" language="ui">
      <Strong language={language ?? 'ui'}>{t(title)}</Strong>
    </Text>
  );
}

export function PanelImage({ src }: { src: ImageSourcePropType }) {
  const { view } = useFeature();
  const sidePanelUrl = useStore((state) => state.avatar.sidePanelUrl);

  return (
    <>
      {view !== 'mobile' && view !== 'embedded' && (
        <View className="vi-flex vi-flex-col vi-items-center">
          <Image
            accessibilityIgnoresInvertColors
            className="vi-h-40 vi-w-full"
            resizeMode="contain"
            source={(sidePanelUrl as ImageSourcePropType | undefined) ?? src}
          />
        </View>
      )}
    </>
  );
}
