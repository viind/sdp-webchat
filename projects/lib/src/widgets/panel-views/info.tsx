import * as React from 'react';
import { Linking, ScrollView, View } from 'react-native';
import ViindyStanding from '~/assets/images/viindy_standing.png';
import { Button, Text } from '~/components/ui';
import { Link } from '~/components/ui/link';
import { Location } from '~/lib/icons/Location';
import { Mail } from '~/lib/icons/Mail';
import { Smartphone } from '~/lib/icons/Smartphone';
import { ViindLogo } from '~/lib/icons/ViindLogo';
import { cn } from '~/lib/utils';
import { useStore } from '~/src/provider/store-provider';
import { addressToUrl, emailToUrl, phoneToUrl } from '~/src/utils';
import { Panel, PanelHeader, PanelImage, PanelTitle } from './components/panel';
import { Strong } from '~/components/ui/strong';

type KnownInfoKeys = WebChatStore['info']['infos'][number]['key'];
type NonTextInfoKeys = Exclude<KnownInfoKeys, 'text'>;

const keyToComponentMap = {
  address: AddressInfo,
  email: EmailInfo,
  phone: PhoneInfo,
} satisfies Record<NonTextInfoKeys, React.FC<{ value: string }>>;

const keyToButtonIconMap = {
  phone: Smartphone,
  email: Mail,
  address: Location,
} satisfies Record<NonTextInfoKeys, React.FC<{ size: number }>>;

const keyToUrlMap = {
  phone: phoneToUrl,
  email: emailToUrl,
  address: addressToUrl,
} satisfies Record<NonTextInfoKeys, (value: string) => string>;

const Info: React.FC = () => {
  return (
    <Panel>
      <PanelImage src={ViindyStanding} />
      <PanelHeader>
        <PanelTitle title="info.title" />
      </PanelHeader>
      <DynamicInfos />
      <ViindInfo />
    </Panel>
  );
};
Info.displayName = 'Info';
export { Info };

function DynamicInfos() {
  const infos = useStore((state) => state.info.infos);
  const links = useStore((state) => state.info.links);
  const t = useStore((state) => state.language.t);
  const defaultLanguage = useStore((state) => state.language.defaultLanguage);

  const showBorder = infos.length > 0 || links.length > 0;
  const texts = infos.filter(({ key }) => key === 'text');
  const knownInfos = infos.filter(({ key }) => key !== 'text');

  return (
    <ScrollView
      className="vi-grow"
      contentContainerClassName="vi-w-full vi-flex-col vi-gap-5"
    >
      {showBorder === false && (
        <Text language="ui">{t('info.no-info-available')}</Text>
      )}
      {texts.map(({ value }) => (
        <Text key={value} language={{ custom: defaultLanguage }}>
          {value}
        </Text>
      ))}
      <View className="vi-grid vi-grid-cols-[auto,1fr] vi-gap-x-2 vi-gap-y-5">
        {knownInfos.map(({ key, value }) => {
          if (key === 'text') {
            return <></>;
          }
          const Component = keyToComponentMap[key];
          return <Component key={key} value={value} />;
        })}
      </View>
      <View
        className={cn(
          'vi-flex vi-flex-col vi-gap-1 vi-py-4',
          showBorder && 'vi-border-t-hairline vi-border-border-message'
        )}
      >
        {links.map(({ title, url }) => (
          <View key={url} className="vi-flex vi-flex-row">
            <Link
              href={url}
              /*
              Hint: there is no option to translate the info options.
              Assume the provided language is the same as the default language.
              Customers will be able to define what the default language will be in the future.
            */
              language={{ custom: defaultLanguage }}
            >
              <Strong language={{ custom: defaultLanguage }}>{title}</Strong>
            </Link>
          </View>
        ))}
      </View>
    </ScrollView>
  );
}

function PhoneInfo({ value: phoneNumber }: { value: string }) {
  const t = useStore((state) => state.language.t);

  return (
    <>
      <InfoTitle title={t('info.phone.title')} />
      <InfoValue
        value={phoneNumber}
        buttonText={t('info.phone.button')}
        buttonIcon="phone"
      />
    </>
  );
}

function EmailInfo({ value: email }: { value: string }) {
  const t = useStore((state) => state.language.t);

  return (
    <>
      <InfoTitle title={t('info.email.title')} />
      <InfoValue
        value={email}
        buttonText={t('info.email.button')}
        buttonIcon="email"
      />
    </>
  );
}

function AddressInfo({ value: address }: { value: string }) {
  const t = useStore((state) => state.language.t);

  return (
    <>
      <InfoTitle title={t('info.address.title')} />
      <InfoValue
        value={address}
        buttonText={t('info.address.button')}
        buttonIcon="address"
      />
    </>
  );
}

function InfoTitle({ title }: { title: string }) {
  const defaultLanguage = useStore((state) => state.language.defaultLanguage);
  /*
    Hint: there is no option to translate the info options.
    Assume the provided language is the same as the default language.
    Customers will be able to define what the default language will be in the future.
  */
  return (
    <Strong className="vi-pr-4" language={{ custom: defaultLanguage }}>
      {title}:
    </Strong>
  );
}

function InfoValue({
  value,
  buttonIcon,
  buttonText,
}: {
  value: string;
  buttonIcon: NonTextInfoKeys;
  buttonText: string;
}) {
  const Icon = keyToButtonIconMap[buttonIcon];
  const defaultLanguage = useStore((state) => state.language.defaultLanguage);
  /*
    Hint: there is no option to translate the info options.
    Assume the provided language is the same as the default language.
    Customers will be able to define what the default language will be in the future.
  */
  return (
    <View className="vi-flex vi-flex-col vi-gap-2">
      <Text language={{ custom: defaultLanguage }} className="vi-leading-tight">
        {value}
      </Text>
      <View className="vi-flex vi-flex-row">
        <Button
          variant="outline"
          className="vi-flex vi-flex-row vi-justify-start vi-gap-2 vi-rounded-2xl vi-px-6"
          onPress={() => Linking.openURL(keyToUrlMap[buttonIcon](value))}
          aria-label={undefined}
          language={undefined}
        >
          <Icon size={20} />
          <Text language={{ custom: defaultLanguage }}>{buttonText}</Text>
        </Button>
      </View>
    </View>
  );
}

function ViindInfo() {
  const t = useStore((state) => state.language.t);

  return (
    <View className="vi-flex vi-w-full vi-flex-row vi-items-center vi-gap-2">
      <Link
        href="https://viind.com"
        className="vi-text-sm vi-opacity-85"
        language="ui"
      >
        {t('info.powered-by')}
      </Link>
      <ViindLogo scale={0.85} />
    </View>
  );
}
