import * as React from 'react';
import { Linking, Platform, ScrollView, View } from 'react-native';
import { Button } from '~/components/ui';
import { Text } from '~/components/ui/text';
import { X } from '~/lib/icons/X';
import { useStore } from '../../provider/store-provider';
import { Panel, PanelHeader, PanelTitle } from './components/panel';
import { focusComponent } from '~/src/utils';
import { useFeature } from '~/src/provider/feature-provider';
import { NewTab } from '~/lib/icons/NewTab';

const IframeView: React.FC = () => {
  const isIframeOpen = useStore((state) => state.iframe.isIframeOpen);
  const iframeConfig = useStore((state) => state.iframe.iframeConfig);
  const closeIframe = useStore((state) => state.iframe.closeIframe);
  const t = useStore((state) => state.language.t);
  const newTabRef = React.useRef<View>(null);
  const { dontShowIframe } = useFeature();

  React.useEffect(() => {
    focusComponent(newTabRef.current);
  }, []);

  if (Platform.OS !== 'web') {
    return <Text language={{ custom: 'en' }}>Not supported platform</Text>;
  }
  if (!isIframeOpen || iframeConfig === undefined) return <></>;

  return (
    <Panel className="vi-pt-0">
      <ScrollView contentContainerClassName="vi-h-full">
        <PanelHeader className="vi-flex-row">
          <PanelTitle language="chat" title={iframeConfig.pageTitle} />
          <View className="vi-flex-col vi-justify-end">
            <Button
              aria-label={t('iframe.close.aria-label', {
                title: iframeConfig.pageTitle,
              })}
              language="ui"
              className="vi-rounded-full"
              onPress={() => {
                closeIframe();
              }}
              size="icon"
              variant="quiet"
            >
              <X size={16} />
            </Button>
            <Button
              aria-label={t('iframe.new-tab.aria-label', {
                title: iframeConfig.pageTitle,
              })}
              language="ui"
              className="vi-rounded-full"
              variant="quiet"
              size="icon"
              ref={newTabRef}
              onPress={() => Linking.openURL(iframeConfig.url)}
            >
              <NewTab size={16} />
            </Button>
          </View>
        </PanelHeader>
        {dontShowIframe ? (
          <Text language="ui">{t('iframe.to-little-space')}</Text>
        ) : (
          <iframe
            src={iframeConfig.url}
            className="vi-size-full"
            tabIndex={0}
          />
        )}
      </ScrollView>
    </Panel>
  );
};
IframeView.displayName = 'IframeView';
export { IframeView };
