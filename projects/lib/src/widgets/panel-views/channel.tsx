import { AccordionContent } from '@radix-ui/react-accordion';
import * as React from 'react';
import { ScrollView, View } from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import ViindyStanding from '~/assets/images/viindy_standing.png';
import { Text } from '~/components/ui';
import {
  Accordion,
  AccordionItem,
  AccordionTrigger,
} from '~/components/ui/accordion';
import { Link } from '~/components/ui/link';
import { Channel } from '~/lib/icons/Channel';
import { FacebookMessenger } from '~/lib/icons/FacebookMessenger';
import { Smartphone } from '~/lib/icons/Smartphone';
import type { SvgProps } from '~/lib/icons/tailwindSvg';
import { WhatsApp } from '~/lib/icons/WhatsApp';
import { useStore } from '~/src/provider/store-provider';
import { addressToUrl, emailToUrl, phoneToUrl } from '~/src/utils';
import { Panel, PanelHeader, PanelImage, PanelTitle } from './components/panel';

type ChannelTypes = WebChatStore['channel']['channels'][number]['type'];

const ChannelView: React.FC = () => {
  return (
    <Panel>
      <PanelImage src={ViindyStanding} />
      <PanelHeader>
        <PanelTitle title="channel.title" />
      </PanelHeader>
      <DynamicChannels />
    </Panel>
  );
};
ChannelView.displayName = 'ChannelView';
export { ChannelView };

const knownIcons: Record<string, React.FC<SvgProps> | undefined> = {
  'whatsapp': WhatsApp,
  'facebook-messenger': FacebookMessenger,
  'phone': Smartphone,
};

function DynamicChannels() {
  const channels = useStore((state) => state.channel.channels);
  const t = useStore((state) => state.language.t);

  const defaultLanguage = useStore((state) => state.language.defaultLanguage);

  return (
    <ScrollView>
      <Accordion type="single" collapsible className="vi-pr-2">
        {channels.map(({ title, value, type, icon }) => (
          <AccordionItem
            key={title}
            value={value}
            className="vi-mb-4 vi-rounded-2xl vi-border vi-px-4"
          >
            <AccordionTrigger>
              <View className="vi-flex vi-flex-row vi-items-center vi-gap-4">
                <Icon icon={icon ?? ''} />
                {/*
                Hint: there is no option to translate the channel options.
                Assume the provided language is the same as the default language.
                Customers will be able to define what the default language will be in the future.
                */}
                <Text language={{ custom: defaultLanguage }}>{title}</Text>
              </View>
            </AccordionTrigger>
            <AccordionContent>
              <View className="vi-flex vi-flex-col vi-items-center vi-gap-4 vi-pb-5">
                <QRCode
                  value={value}
                  logoSVG={knownIcons[icon?.toLowerCase() ?? '']}
                />
                {/*
                Hint: there is no option to translate the channel options.
                Assume the provided language is the same as the default language.
                Customers will be able to define what the default language will be in the future.
                */}
                <Text
                  className="vi-text-center"
                  language={{ custom: defaultLanguage }}
                >
                  {t('channel.qr-code-hint')}:
                </Text>
                {/*
                Hint: there is no option to translate the channel options.
                Assume the provided language is the same as the default language.
                Customers will be able to define what the default language will be in the future.
                */}
                <Link
                  className="vi-text-foreground-primary"
                  href={typeToUrl[type](value)}
                  language={{ custom: defaultLanguage }}
                >
                  {value}
                </Link>
              </View>
            </AccordionContent>
          </AccordionItem>
        ))}
      </Accordion>
    </ScrollView>
  );
}

function Icon({ icon }: { icon: string }) {
  const UsedIcon = knownIcons[icon.toLowerCase()];
  if (UsedIcon === undefined) {
    return <DefaultIcon />;
  }
  return <UsedIcon size={32} />;
}

function DefaultIcon() {
  return (
    <View className="vi-flex vi-size-8 vi-items-center vi-justify-center vi-rounded-full vi-bg-primary">
      <Channel size={18} className="vi-pb-[2px] vi-text-primary-foreground" />
    </View>
  );
}

const typeToUrl = {
  url: (value) => value,
  phone: phoneToUrl,
  email: emailToUrl,
  address: addressToUrl,
} satisfies Record<ChannelTypes, (value: string) => string>;
