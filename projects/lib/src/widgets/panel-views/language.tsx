import * as Flags from 'country-flag-icons/react/3x2';
import * as React from 'react';
import { View } from 'react-native';
import LanguageViindy from '~/assets/images/language_viindy_placeholder.png';
import { Button, Input, Text } from '~/components/ui';
import { Check } from '~/lib/icons/Check';
import { EarthEurope } from '~/lib/icons/EarthEurope';
import { Search } from '~/lib/icons/Search';
import { cn } from '~/lib/utils';
import { languageToFlagCodeMap } from '~/src/generated/language-to-flag-code';
import { useStore } from '~/src/provider/store-provider';
import { Panel, PanelHeader, PanelImage, PanelTitle } from './components/panel';
import { Label } from '~/components/ui/label';

type LanguageSubset = WebChatStore['language']['enabledLanguages'];
type KnownLanguages = keyof LanguageSubset;

export function Language() {
  const chatLanguageName = useStore((state) => state.language.chatLanguageName);
  return (
    <Panel>
      <PanelImage src={LanguageViindy} />
      <PanelHeader>
        <PanelTitle title="language.title" />
        <View className="vi-flex vi-flex-row vi-items-center vi-gap-2">
          <EarthEurope className="vi-text-foreground" size={18} />
          <Text className="vi-text-foreground" language="chat">
            {chatLanguageName}
          </Text>
        </View>
      </PanelHeader>
      <LanguageSelection />
    </Panel>
  );
}

function LanguageSelection() {
  const enabledLanguages = useStore((state) => state.language.enabledLanguages);
  const chatLanguage = useStore((state) => state.language.chatLanguage);
  const changeLanguage = useStore((state) => state.language.changeLanguage);
  const [search, setSearch] = React.useState('');
  const t = useStore((state) => state.language.t);
  const showLanguageSearch = useStore(
    (state) => state.language.showLanguageSearch
  );

  const filteredLanguages = React.useMemo(() => {
    const languageNames = Object.values(enabledLanguages).flat().sort();
    return (
      search === ''
        ? languageNames
        : languageNames.filter((name) =>
            name.toLowerCase().includes(search.toLowerCase())
          )
    )
      .map((lang) => ({
        key: getLanguageCodeFromName(enabledLanguages, lang),
        name: lang,
      }))
      .filter(({ name }) => name !== undefined) as {
      key: KnownLanguages;
      name: string;
    }[];
  }, [search, enabledLanguages]);

  return (
    <>
      <View
        className={cn(
          'vi-flex vi-w-full vi-flex-row vi-items-center vi-py-3',
          !showLanguageSearch && 'vi-hidden'
        )}
      >
        <Input
          className="vi-rounded-r-none vi-border-r-0"
          placeholder={t('language.search-input.placeholder')}
          value={search}
          onChangeText={setSearch}
          id="viind-web-chat-language-search"
        />
        <Button
          variant="quiet"
          size="icon"
          realDisabled
          className="vi-group vi-rounded-l-none vi-rounded-r-[1.75rem] vi-border vi-border-l-0 vi-border-border-message vi-bg-background vi-pr-3 vi-opacity-75 hover:vi-opacity-75"
          aria-hidden
          aria-label={undefined}
          language={undefined}
        >
          <Search size={25} />
        </Button>
      </View>
      <View className="vi-w-full vi-border-b-hairline vi-border-border-message">
        {filteredLanguages.map(({ key, name }) => (
          <View className="vi-p-0.5 vi-pr-2" key={key}>
            <Button
              id={`viind-web-chat-language-${key}`}
              variant="quiet"
              className={cn(
                'vi-flex vi-flex-row vi-justify-between vi-gap-2 hover:vi-bg-bot-message',
                chatLanguage === key && 'vi-bg-bot-message'
              )}
              aria-selected={chatLanguage === key}
              aria-label={undefined}
              language={undefined}
              onPress={() => changeLanguage(name)}
            >
              <View className="vi-flex vi-flex-row vi-items-center vi-gap-2">
                <FlagIcon code={key} className="vi-size-6" />
                <Label
                  htmlFor={`viind-web-chat-language-${key}`}
                  className="vi-hidden"
                >
                  {/* to allow for multiple languages for the button explanation, use this text that is only visible to screen readers */}
                  <Text language="ui">
                    {t('language.change-language-button.aria-label', {
                      language: name,
                    })}
                  </Text>
                </Label>
                <Text
                  language={{ custom: key }}
                  className={cn(
                    chatLanguage === key && 'vi-text-bot-message-foreground'
                  )}
                >
                  {name}
                </Text>
              </View>
              <View>{chatLanguage === key && <Check size={20} />}</View>
            </Button>
          </View>
        ))}
      </View>
    </>
  );
}

function FlagIcon({
  code,
  className,
}: {
  code: KnownLanguages;
  className?: string;
}) {
  if (code === 'unknown') return <></>;

  const uppercaseCountryCode = (languageToFlagCodeMap[code]?.toUpperCase() ??
    '') as keyof typeof Flags;
  const Flag = Flags[uppercaseCountryCode];
  if (Flag === undefined) {
    return <></>;
  }
  return <Flag className={className} />;
}

function getLanguageCodeFromName(
  enabledLanguages: LanguageSubset,
  name: string
) {
  return (
    Object.entries(enabledLanguages).find(([, value]) => value === name)?.[0] ??
    'unknown'
  );
}
