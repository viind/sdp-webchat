import * as React from 'react';
import { Platform, ScrollView, View, type TextInput } from 'react-native';
import Markdown, {
  renderRules,
  type RenderRules,
} from 'react-native-markdown-display';
import { TextLanguageContext } from '~/components/primitives/hooks/useLanguage';
import { Button, Input, Text, TextClassContext } from '~/components/ui';
import { Label } from '~/components/ui/label';
import { Link } from '~/components/ui/link';
import { Strong } from '~/components/ui/strong';
import { CircleStop } from '~/lib/icons/CircleStop';
import { Microphone } from '~/lib/icons/Microphone';
import { Send } from '~/lib/icons/Send';
import { Spinner } from '~/lib/icons/Spinner';
import { ThumbDown } from '~/lib/icons/ThumbDown';
import { ThumbUp } from '~/lib/icons/ThumbUp';
import { VolumeHigh } from '~/lib/icons/VolumeHigh';
import { cn } from '~/lib/utils';
import Section from '~/src/components/section';
import { useFeature } from '~/src/provider/feature-provider';
import { useStore } from '~/src/provider/store-provider';
import { focusComponent } from '~/src/utils';
import { TypingIndicator } from '../../components/typing';
import {
  useDialog,
  type BotButtons,
  type BotMessage,
  type Message,
  type SmallCornerVariant,
} from '../../provider/dialog-provider';

const rules = {
  link: (node, children, _, styles) => {
    // this is needed to add the TouchableOpacity to the link, to make it selectable
    return (
      <Link
        key={node.key}
        href={node.attributes.href}
        focus="onBotMessage"
        // The library does not provide typings for this
        style={styles.link}
        language="context"
      >
        {children}
      </Link>
    );
  },
  text: (node) => {
    return (
      <Text language="context" key={node.key}>
        {node.content}
      </Text>
    );
  },
  strong: (node, children, _, styles) => {
    return (
      <Strong key={node.key} language="context" style={styles.bold}>
        {children}
      </Strong>
    );
  },
  ordered_list:
    Platform.OS !== 'web'
      ? renderRules.ordered_list
      : (node, children) => {
          return (
            <ol
              key={node.key}
              style={{ listStylePosition: 'inside', listStyleType: 'decimal' }}
              className="vi-my-[10px]"
            >
              {children}
            </ol>
          );
        },
  bullet_list:
    Platform.OS !== 'web'
      ? renderRules.bullet_list
      : (node, children) => {
          return (
            <ul
              key={node.key}
              style={{ listStyle: 'inside' }}
              className="vi-my-[10px]"
            >
              {children}
            </ul>
          );
        },
  list_item:
    Platform.OS !== 'web'
      ? renderRules.list_item
      : (node, children) => {
          return <li key={node.key}>{children}</li>;
        },
} satisfies RenderRules;

export function Chat({
  size,
  className,
}: {
  size?: 'small' | 'big';
  className?: string;
}) {
  const t = useStore((state) => state.language.t);
  const { entries, addUserMessage, changeSize, showTyping } = useDialog();
  const { chatFooterInScrollArea } = useFeature();

  const scrollViewRef = React.useRef<ScrollView>(null);

  React.useEffect(() => {
    changeSize(size ?? 'small');
  }, [size]);

  return (
    <Section className={cn(className, 'vi-flex vi-flex-col')}>
      {/* this is only for screen readers so that the section has a title */}
      <Text role="heading" language="ui" className="vi-hidden">
        {t('chat.aria-label')}
      </Text>
      <ScrollView
        stickyHeaderIndices={[0]}
        ref={scrollViewRef}
        aria-live="polite"
        onContentSizeChange={() => {
          scrollViewRef.current?.scrollToEnd({ animated: true });
        }}
        contentContainerClassName={cn('vi-w-full vi-max-w-3xl')}
        className="vi-items-center"
      >
        <View className="vi-h-2 vi-w-full vi-bg-background"></View>
        <View className="vi-flex vi-grow vi-flex-col vi-gap-4 vi-px-4 vi-py-2">
          {entries.map((entry, index) => {
            if (entry.type === 'date') {
              return <ChatDate key={index} date={entry.date} />;
            }
            if (entry.type === 'group') {
              return (
                <MessageGroup key={index}>
                  {entry.messages.map((messageData) => message(messageData))}
                </MessageGroup>
              );
            }
            return message(entry);
          })}
          <TypingIndicator isTyping={showTyping} />
        </View>
        {chatFooterInScrollArea && (
          <ChatFooter onSendMessage={addUserMessage} />
        )}
      </ScrollView>
      {chatFooterInScrollArea === false && (
        <ChatFooter onSendMessage={addUserMessage} />
      )}
    </Section>
  );
}

function message(message: Message) {
  if (message.type === 'bot-buttons') {
    return (
      <BotButtons
        key={message.buttons.map((x) => x.id).join(',')}
        buttons={message.buttons}
      />
    );
  }
  if (message.type === 'bot-message') {
    return <BotMessage key={message.id} {...message} />;
  }
  return (
    <UserMessage
      key={message.id}
      message={message.message}
      smallCorner={message.smallCorner}
      language={message.language}
    />
  );
}

function MessageGroup({ children }: { children: React.ReactNode }) {
  return <View className="vi-flex vi-flex-col vi-gap-1">{children}</View>;
}

function cornerVariantToTailwind(
  config: SmallCornerVariant,
  bottom: string,
  top: string
) {
  if (config === 'none') return '';
  if (config === 'bottom') return bottom;
  if (config === 'top') return top;
  return `${bottom} ${top}`;
}

function BotMessage({
  id,
  message,
  smallCorner = 'none',
  like,
  feedbackProvided,
  language,
}: Pick<
  BotMessage,
  'id' | 'message' | 'smallCorner' | 'like' | 'feedbackProvided' | 'language'
>) {
  const { onLikePress, provideFeedback, size } = useDialog();
  const [feedbackText, setFeedbackText] = React.useState('');
  const t = useStore((state) => state.language.t);
  const showTextToSpeech = useStore((state) => state.ttsStt.showTextToSpeech);
  const readText = useStore((state) => state.ttsStt.readText);
  const stopReading = useStore((state) => state.ttsStt.stopReading);
  const [isReading, setIsReading] = React.useState(false);
  const feedbackRef = React.useRef<TextInput>(null);

  const toggleReadMessage = () => {
    if (isReading) {
      stopReading();
    } else {
      readText(message, () => setIsReading(false));
      setIsReading(true);
    }
  };

  const roundedConfig = {
    message: {
      false: cn(
        'vi-rounded-2xl',
        cornerVariantToTailwind(
          smallCorner,
          cn('vi-rounded-bl-sm'),
          cn('vi-rounded-tl-sm')
        )
      ),
      true: cn(
        'vi-rounded-t-2xl',
        cornerVariantToTailwind(smallCorner, cn(''), cn('vi-rounded-tl-sm'))
      ),
    },
    feedback: {
      true: cn(
        'vi-rounded-b-2xl',
        cornerVariantToTailwind(smallCorner, cn('vi-rounded-bl-sm'), cn(''))
      ),
      false: '',
    },
  } as const;
  const configSelector = `${like !== undefined}` as const;
  const FeedbackButtonConfig = {
    small: {
      buttonSize: cn('vi-size-6'),
      iconSize: 16,
      positioning: cn('-vi-mr-9'),
      gap: cn('vi-gap-1'),
    },
    big: {
      buttonSize: cn('vi-size-8'),
      iconSize: 20,
      positioning: cn('-vi-mr-10'),
      gap: cn('vi-gap-2'),
    },
  } satisfies Record<
    typeof size,
    {
      buttonSize: string;
      iconSize: number;
      positioning: string;
      gap: string;
    }
  >;
  const SpeechSynthesisButtonConfig = {
    small: {
      buttonSize: cn('vi-size-6'),
      iconSize: 16,
      positioning: cn('-vi-bottom-8'),
      gap: cn('vi-gap-1'),
    },
    big: {
      buttonSize: cn('vi-size-8'),
      iconSize: 20,
      positioning: cn('-vi-bottom-9'),
      gap: cn('vi-gap-2'),
    },
  } satisfies Record<
    typeof size,
    {
      buttonSize: string;
      iconSize: number;
      positioning: string;
      gap: string;
    }
  >;

  return (
    <View className="vi-self-start vi-pr-16">
      <View
        className={cn(
          'vi-group vi-relative vi-flex vi-items-end vi-justify-center vi-bg-bot-message vi-px-6 vi-py-4',
          roundedConfig.message[configSelector]
        )}
      >
        <TextClassContext.Provider
          value={cn('vi-break-words vi-text-bot-message-foreground')}
        >
          <Text language={{ custom: language }}>
            <TextLanguageContext.Provider value={language}>
              <Markdown rules={rules}>{message}</Markdown>
            </TextLanguageContext.Provider>
          </Text>
        </TextClassContext.Provider>
        <View
          className={cn('vi-absolute', FeedbackButtonConfig[size].positioning)}
        >
          <View
            className={cn(
              'vi-flex vi-flex-col',
              FeedbackButtonConfig[size].gap
            )}
          >
            <Button
              id={`viind-web-chat-like-${id}`}
              aria-label={undefined}
              language={undefined}
              focus="onBackground"
              variant="quiet"
              size="icon"
              className={cn(
                'vi-rounded-full vi-bg-user-message vi-p-1',
                like === 'like' && 'vi-bg-success',
                FeedbackButtonConfig[size].buttonSize
              )}
              aria-selected={like === 'like'}
              onPress={() => {
                focusComponent(feedbackRef.current);
                onLikePress(id, 'like');
              }}
            >
              <Label
                htmlFor={`viind-web-chat-like-${id}`}
                className="vi-hidden"
              >
                {/* to allow for multiple languages for the screen readers */}
                <Text language="ui">
                  {t(
                    like === 'like'
                      ? 'chat.bot-message.remove-like.aria-label'
                      : 'chat.bot-message.like.aria-label'
                  )}
                </Text>
                <Text language={{ custom: language }}>{message}</Text>
              </Label>
              <ThumbUp
                size={FeedbackButtonConfig[size].iconSize}
                className={cn(
                  '-vi-scale-x-100 vi-text-user-message-foreground',
                  like === 'like' && 'vi-text-success-foreground'
                )}
              />
            </Button>
            <Button
              id={`viind-web-chat-dislike-${id}`}
              aria-label={undefined}
              language={undefined}
              focus="onBackground"
              variant="quiet"
              size="icon"
              className={cn(
                'vi-size-6 vi-rounded-full vi-bg-user-message vi-p-1',
                like === 'dislike' && 'vi-bg-error',
                FeedbackButtonConfig[size].buttonSize
              )}
              aria-selected={like === 'dislike'}
              onPress={() => {
                focusComponent(feedbackRef.current);
                onLikePress(id, 'dislike');
              }}
            >
              <Label
                htmlFor={`viind-web-chat-dislike-${id}`}
                className="vi-hidden"
              >
                {/* to allow for multiple languages for the screen readers */}
                <Text language="ui">
                  {t(
                    like === 'dislike'
                      ? 'chat.bot-message.remove-dislike.aria-label'
                      : 'chat.bot-message.dislike.aria-label'
                  )}
                </Text>
                <Text language={{ custom: language }}>{message}</Text>
              </Label>
              <ThumbDown
                size={FeedbackButtonConfig[size].iconSize}
                className={cn(
                  '-vi-scale-x-100 vi-text-user-message-foreground',
                  like === 'dislike' && 'vi-text-error-foreground'
                )}
              />
            </Button>
          </View>
        </View>
      </View>
      <View
        className={cn(
          'vi-border-t vi-border-border-message vi-bg-bot-message vi-px-4 vi-py-3',
          like === undefined && 'vi-hidden',
          roundedConfig.feedback[configSelector]
        )}
        aria-hidden={like === undefined}
      >
        <View
          className={cn(
            'vi-flex vi-flex-row vi-items-center',
            Boolean(feedbackProvided) && 'vi-hidden'
          )}
          aria-hidden={Boolean(feedbackProvided)}
        >
          <Input
            id="viind-web-chat-send-feedback"
            ref={feedbackRef}
            className="vi-h-12 vi-w-full vi-grow vi-rounded-l-[1.75rem] vi-rounded-r-none vi-border vi-border-r-0 vi-border-border-message vi-bg-background vi-pl-4 vi-text-foreground vi-opacity-75"
            placeholder={t('chat.bot-message.send-feedback.placeholder')}
            value={feedbackText}
            onChangeText={setFeedbackText}
            onKeyPress={(ev) => {
              if (ev.nativeEvent.key === 'Enter') {
                provideFeedback(id, feedbackText);
              }
            }}
          />
          <Button
            language="ui"
            aria-label={t('chat.bot-message.send-feedback.aria-label')}
            variant="quiet"
            size="icon"
            disabled={feedbackText === ''}
            onPress={() => {
              if (feedbackText === '') return;
              provideFeedback(id, feedbackText);
            }}
            className={cn(
              'vi-group vi-rounded-l-none vi-rounded-r-[1.75rem] vi-border vi-border-l-0 vi-border-border-message vi-bg-background vi-opacity-75 hover:vi-opacity-75'
            )}
          >
            <Send
              size={25}
              className={cn(feedbackText === '' && 'vi-text-opacity-15')}
            />
          </Button>
        </View>
        <View
          className={cn(
            'vi-flex vi-flex-row vi-items-center',
            Boolean(feedbackProvided) === false && 'vi-hidden'
          )}
          aria-hidden={Boolean(feedbackProvided) === false}
        >
          <View className="vi-flex vi-h-12 vi-w-full vi-items-center vi-justify-center vi-rounded-[1.75rem] vi-border vi-border-border-message vi-bg-background vi-px-2 vi-opacity-50">
            <Text language="ui">
              {t('chat.bot-message.send-feedback.thank-you')}
            </Text>
          </View>
        </View>
      </View>
      {showTextToSpeech && (
        <View
          className={cn(
            'vi-absolute',
            SpeechSynthesisButtonConfig[size].positioning
          )}
        >
          <View
            className={cn(
              'vi-flex vi-flex-col',
              SpeechSynthesisButtonConfig[size].gap
            )}
          >
            <Button
              language="ui"
              aria-label={t('chat.bot-message.read.aria-label')}
              variant="quiet"
              size="icon"
              className={cn(
                'vi-size-6 vi-rounded-full vi-border vi-border-border-message vi-p-1',
                SpeechSynthesisButtonConfig[size].buttonSize
              )}
              onPress={() => toggleReadMessage()}
            >
              {!isReading ? (
                <VolumeHigh
                  size={SpeechSynthesisButtonConfig[size].iconSize}
                  className={cn('-vi-scale-x-100')}
                />
              ) : (
                <CircleStop
                  size={SpeechSynthesisButtonConfig[size].iconSize}
                  className={cn('-vi-scale-x-100')}
                />
              )}
            </Button>
          </View>
        </View>
      )}
    </View>
  );
}

function BotButtons({ buttons }: { buttons: BotButtons['buttons'] }) {
  const { onBotButtonPress } = useDialog();
  const t = useStore((state) => state.language.t);
  const showTextToSpeech = useStore((state) => state.ttsStt.showTextToSpeech);

  return (
    <View
      className={cn(
        'vi-flex vi-flex-row vi-flex-wrap vi-gap-1 vi-self-start vi-pr-16',
        showTextToSpeech && 'vi-mt-6'
      )}
    >
      {buttons.map((button) => {
        return (
          <Button
            id={`viind-web-chat-button-${button.id}`}
            aria-label={undefined}
            language={undefined}
            variant="outline-with-primary"
            onPress={() =>
              button.enabled && onBotButtonPress(button.message, button.payload)
            }
            key={button.id}
            allowBreakWords
            className="vi-max-w-full"
          >
            <Label htmlFor={`viind-web-chat-button-${button.id}`}>
              {/* to allow for multiple languages for the button explanation, use this text that is only visible to screen readers */}
              <Text language="ui" className="vi-hidden">
                {t('chat.bot-buttons.prefix.aria-label')}
              </Text>
              <Strong
                language={{ custom: button.language }}
                className="vi-text-base"
              >
                {button.message}
              </Strong>
            </Label>
          </Button>
        );
      })}
    </View>
  );
}

function UserMessage({
  message,
  language,
  smallCorner = 'none',
}: {
  message: string;
  language: string;
  smallCorner?: SmallCornerVariant;
}) {
  return (
    <View className="vi-max-w-lg vi-self-end vi-pl-16">
      <View
        className={cn(
          'vi-rounded-2xl vi-bg-user-message vi-px-6 vi-py-4',
          cornerVariantToTailwind(
            smallCorner,
            'vi-rounded-br-sm',
            'vi-rounded-tr-sm'
          )
        )}
      >
        <Text
          className="vi-break-words vi-text-user-message-foreground"
          language={{ custom: language }}
        >
          {message}
        </Text>
      </View>
    </View>
  );
}

function ChatDate({ date }: { date: string }) {
  // todo: the date format is fixed
  return (
    <View className="vi-flex vi-flex-row vi-justify-center vi-gap-2">
      <Text
        language={{ custom: 'en' }}
        className="vi-text-sm vi-font-bold vi-text-foreground vi-opacity-65"
      >
        {date}
      </Text>
    </View>
  );
}

function ChatFooter({
  onSendMessage,
}: {
  onSendMessage: (message: string) => void;
}) {
  const { chatReference } = useDialog();
  const [message, setMessage] = React.useState('');
  const showSpeechToText = useStore((state) => state.ttsStt.showSpeechToText);
  const t = useStore((state) => state.language.t);
  const startSpeechRecognition = useStore(
    (state) => state.ttsStt.startSpeechRecognition
  );
  const stopSpeechRecognition = useStore(
    (state) => state.ttsStt.stopSpeechRecognition
  );
  const isListening = useStore((state) => state.ttsStt.isListening);

  React.useEffect(() => {
    if (isListening) {
      focusComponent(chatReference.current);
    }
  }, [message]);

  React.useEffect(() => {
    focusComponent(chatReference.current);
  }, [chatReference.current]);

  const handleSend = () => {
    onSendMessage(message);
    setMessage('');
    focusComponent(chatReference.current);
  };

  const toggleSpeechRecognition = () => {
    if (isListening) {
      stopSpeechRecognition();
    } else {
      startSpeechRecognition((text) => {
        setMessage(`${message} ${text}`);
      });
    }
  };

  return (
    <View className="vi-flex vi-flex-row vi-items-center vi-border-t-hairline vi-border-border-message vi-p-3 vi-pl-4">
      <View className="vi-flex vi-grow vi-flex-row vi-items-center">
        <Input
          ref={chatReference}
          className={cn('vi-h-14 vi-border-2', showSpeechToText && 'vi-pr-10')}
          placeholder={t('chat.message.placeholder')}
          value={message}
          onChangeText={setMessage}
          onKeyPress={(ev) => {
            if (ev.nativeEvent.key === 'Enter') {
              handleSend();
            }
          }}
          id="viind-web-chat-send-message"
        />
        {showSpeechToText && (
          <Button
            className={cn('vi-absolute vi-right-0')}
            aria-label={t('chat.message.recognition.aria-label')}
            language="ui"
            variant="quiet-with-primary"
            size="icon"
            onPress={() => {
              toggleSpeechRecognition();
            }}
          >
            {!isListening ? (
              <Microphone size={25} />
            ) : (
              <Spinner size={25} className="web:vi-animate-spin" />
            )}
          </Button>
        )}
      </View>
      <Button
        aria-label={t('chat.message.send.aria-label')}
        language="ui"
        variant="quiet-with-primary"
        size="icon"
        disabled={message === ''}
        onPress={() => {
          handleSend();
        }}
      >
        <Send size={25} />
      </Button>
    </View>
  );
}
