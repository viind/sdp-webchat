import * as React from 'react';
import { Image } from 'react-native';
import ViindyStanding from '~/assets/images/viindy_standing.png';
import { IframeView } from './iframe';
import { useStore } from '~/src/provider/store-provider';
import { Text } from '~/components/ui';

const Viindy: React.FC = () => {
  const sidePanelUrl = useStore((state) => state.avatar.sidePanelUrl);
  const isIframeOpen = useStore((state) => state.iframe.isIframeOpen);

  if (isIframeOpen) {
    return <IframeView />;
  }

  return (
    <>
      {/* this is only so that the section has a heading for accessibility */}
      <Text role="heading" className="vi-hidden" language="ui">
        Viindy
      </Text>
      <Image
        accessibilityIgnoresInvertColors
        className="vi-aspect-auto vi-size-full vi-max-w-60"
        resizeMode="contain"
        source={sidePanelUrl ?? ViindyStanding}
      />
    </>
  );
};
Viindy.displayName = 'Viindy';
export { Viindy };
