import * as React from 'react';
import { View } from 'react-native';
import { Button, Text } from '~/components/ui';
import { ArrowLeft } from '~/lib/icons/ArrowLeft';
import { Channel } from '~/lib/icons/Channel';
import { FaceSmile } from '~/lib/icons/FaceSmile';
import { Info } from '~/lib/icons/Info';
import { Language } from '~/lib/icons/Language';
import { Message } from '~/lib/icons/Message';
import { WindowRestore } from '~/lib/icons/WindowRestore';
import { cn } from '~/lib/utils';
import Section from '../components/section';
import { usePanel } from '../provider/panel-provider';
import { useSidePanel } from '../provider/side-panel-provider';
import { useStore } from '../provider/store-provider';

type PanelItems = NonNullable<
  ReturnType<typeof usePanel>['panel']['selection']
>;
type PanelSize = ReturnType<typeof usePanel>['panel']['config']['size'];
type PanelOrientation = ReturnType<
  typeof usePanel
>['panel']['config']['orientation'];

const panelItemConfig = {
  chat: {
    label: 'panel-navigation.chat.aria-label',
    hint: 'panel-navigation.chat.aria-hint',
    icon: Message,
  },
  iframe: {
    label: 'panel-navigation.iframe.aria-label',
    hint: 'panel-navigation.iframe.aria-hint',
    icon: WindowRestore,
  },
  viindy: {
    label: 'panel-navigation.viindy.aria-label',
    hint: 'panel-navigation.viindy.aria-hint',
    icon: FaceSmile,
  },
  language: {
    label: 'panel-navigation.language.aria-label',
    hint: 'panel-navigation.language.aria-hint',
    icon: Language,
  },
  channel: {
    label: 'panel-navigation.channel.aria-label',
    hint: 'panel-navigation.channel.aria-hint',
    icon: Channel,
  },
  info: {
    label: 'panel-navigation.info.aria-label',
    hint: 'panel-navigation.info.aria-hint',
    icon: Info,
  },
} satisfies Record<
  PanelItems,
  {
    label: string;
    hint: string;
    icon: React.FC<{ size: number }>;
  }
>;

const panelSizeConfig = {
  normal: 24,
  big: 35,
} satisfies Record<PanelSize, number>;

export function PanelNavigation({ className }: { className?: string }) {
  const t = useStore((state) => state.language.t);
  const { clickOnItem, panel } = usePanel();
  const { toggleOpenState, isSidePanelOpen } = useSidePanel();
  const iconSize = panelSizeConfig[panel.config.size];

  const PanelItems = panel.config.options.map((key) => {
    const item = panelItemConfig[key];

    return (
      <PanelEntry
        key={key}
        label={item.label}
        hint={item.hint}
        onPress={() => clickOnItem(key)}
        isSelected={panel.selection === key}
        panelOrientation={panel.config.orientation}
        panelSize={panel.config.size}
      >
        <item.icon size={iconSize} />
      </PanelEntry>
    );
  });

  if (panel.config.view === 'desktop-windowed') {
    return (
      <Windowed
        className={className}
        toggleButton={
          panel.config.showToggleButton && (
            <Button
              variant="quiet-with-primary"
              size="icon"
              aria-label={
                isSidePanelOpen
                  ? t('panel-navigation.side-panel.collapse.aria-label')
                  : t('panel-navigation.side-panel.expand.aria-label')
              }
              language="ui"
              onPress={toggleOpenState}
              aria-expanded={isSidePanelOpen}
            >
              <ArrowLeft
                size={iconSize}
                className={cn(isSidePanelOpen && '-vi-scale-x-100')}
              />
            </Button>
          )
        }
      >
        {PanelItems}
      </Windowed>
    );
  }

  if (panel.config.view === 'desktop-fullscreen') {
    return <Fullscreen className={className}>{PanelItems}</Fullscreen>;
  }

  if (panel.config.view === 'mobile') {
    return <Mobile className={className}>{PanelItems}</Mobile>;
  }
  panel.config.view satisfies never;
  return null;
}

function Windowed({
  className,
  children,
  toggleButton,
}: {
  className?: string;
  children: React.ReactNode;
  toggleButton: React.ReactNode;
}) {
  const t = useStore((state) => state.language.t);
  return (
    <Section className="vi-flex vi-h-full vi-items-center vi-justify-center">
      {/* this is only so that the section has a heading for accessibility */}
      <Text role="heading" language="ui" className="vi-hidden">
        {t('panel-navigation.windowed.aria-label')}
      </Text>
      <View
        className={cn(
          'vi-pointer-events-auto vi-flex vi-flex-col vi-rounded-l-2xl vi-shadow-[rgba(17,_17,_26,_0.1)_0px_0px_16px]',
          className
        )}
      >
        <View
          className={cn(
            'vi-rounded-tl-2xl vi-bg-background vi-pt-1 vi-color-primary'
          )}
        >
          {toggleButton}
        </View>
        <View
          role="menu"
          className="vi-rounded-bl-2xl vi-bg-primary vi-pb-1 vi-color-background"
        >
          {children}
        </View>
      </View>
    </Section>
  );
}

function Fullscreen({
  className,
  children,
}: {
  className?: string;
  children: React.ReactNode;
}) {
  const t = useStore((state) => state.language.t);

  return (
    <Section
      className={cn(
        'vi-flex vi-h-full vi-flex-col vi-justify-center vi-gap-5 vi-bg-primary vi-text-background',
        className
      )}
    >
      {/* this is only so that the section has a heading for accessibility */}
      <Text role="heading" language="ui" className="vi-hidden">
        {t('panel-navigation.fullscreen.aria-label')}
      </Text>
      {children}
    </Section>
  );
}

function Mobile({
  className,
  children,
}: {
  className?: string;
  children: React.ReactNode;
}) {
  const t = useStore((state) => state.language.t);

  return (
    <Section
      className={cn(
        'vi-flex vi-flex-row vi-justify-center vi-gap-5 vi-bg-primary vi-text-background',
        className
      )}
    >
      {/* this is only so that the section has a heading for accessibility */}
      <Text role="heading" language="ui" className="vi-hidden">
        {t('panel-navigation.mobile.aria-label')}
      </Text>
      {children}
    </Section>
  );
}

const sizeConfig = {
  left: {
    normal: 'vi-h-6 vi-w-1',
    big: 'vi-h-10 vi-w-2',
  },
  bottom: {
    normal: 'vi-h-1 vi-w-6',
    big: 'vi-h-2 vi-w-10',
  },
} satisfies Record<PanelOrientation, Record<PanelSize, string>>;

function PanelEntry({
  children,
  label,
  hint,
  isSelected,
  panelOrientation,
  panelSize,
  onPress,
  buttonRef,
}: {
  children: React.ReactNode;
  label: string;
  hint: string;
  isSelected?: boolean;
  panelOrientation: PanelOrientation;
  panelSize: PanelSize;
  onPress: () => void;
  buttonRef?: React.MutableRefObject<null>;
}) {
  const size = sizeConfig[panelOrientation][panelSize];
  const t = useStore((state) => state.language.t);
  return (
    <View role="menuitem">
      <Button
        onPress={onPress}
        variant="quiet-on-primary"
        size="icon"
        aria-selected={isSelected}
        aria-label={t(label)}
        accessibilityHint={t(hint)}
        language="ui"
        focus="onPrimary"
        className={cn(
          'vi-relative',
          panelSize === 'big' && 'vi-size-16',
          panelOrientation === 'left' && 'vi-flex vi-justify-center',
          'vi-rounded-md'
        )}
        ref={buttonRef}
      >
        <View
          className={cn(
            'vi-absolute vi-rounded-r-sm vi-bg-background',
            isSelected === false && 'vi-hidden',
            size,
            panelOrientation === 'left' && 'vi-left-0',
            panelOrientation === 'bottom' && 'vi-bottom-0'
          )}
          aria-hidden={isSelected === false}
        />
        {children}
      </Button>
    </View>
  );
}
