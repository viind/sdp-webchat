import * as React from 'react';
import { Image, View } from 'react-native';
import MiniViindy from '~/assets/images/min_viindy.png';
import { Text, TextClassContext } from '~/components/ui';
import { cn } from '~/lib/utils';
import { ResizeButton } from '../components/resize-button';
import { useFeature } from '../provider/feature-provider';
import { useStore } from '~/src/provider/store-provider';
import { Strong } from '~/components/ui/strong';
import Section from '../components/section';

export function DialogHeader({
  children,
  className,
}: {
  children?: React.ReactNode;
  className?: string;
}) {
  const {
    showHeaderResizeWindow,
    showHeaderShowViindy,
    showHeaderDescription,
  } = useFeature();
  const t = useStore((state) => state.language.t);
  const title = useStore((state) => state.header.title);
  const description = useStore((state) => state.header.description);
  const headerAvatarUrl = useStore((state) => state.avatar.headerUrl);
  const headerRoundLevel = useStore((state) => state.avatar.headerRoundLevel);

  return (
    <Section
      className={cn(
        'vi-flex vi-flex-row vi-justify-between vi-bg-primary vi-pl-10 vi-pr-4',
        className
      )}
    >
      {showHeaderResizeWindow && <ResizeButton variant="quiet-on-primary" />}
      <View
        className={cn(
          'vi-flex vi-flex-row vi-items-center vi-gap-4 vi-leading-none vi-text-primary-foreground',
          showHeaderDescription && 'vi-pb-3 vi-pt-2'
        )}
      >
        {showHeaderShowViindy && (
          <Image
            accessibilityIgnoresInvertColors
            className="vi-size-12"
            resizeMode="contain"
            style={{
              borderRadius: headerRoundLevel,
            }}
            source={headerAvatarUrl ?? MiniViindy}
          />
        )}
        <View>
          <TextClassContext.Provider value={cn('vi-text-primary-foreground')}>
            <Text role="heading" className="vi-text-xl" language="ui">
              <Strong language="ui">{title ?? t('dialog-header.title')}</Strong>
            </Text>
            {showHeaderDescription && (
              <Text className="vi-font-normal" language="ui">
                {description ?? t('dialog-header.subtitle')}
              </Text>
            )}
          </TextClassContext.Provider>
        </View>
      </View>
      {children}
    </Section>
  );
}
