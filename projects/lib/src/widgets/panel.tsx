import * as React from 'react';
import { ScrollView, View } from 'react-native';
import { cn } from '~/lib/utils';
import { usePanel } from '~/src/provider/panel-provider';
import { ChannelView } from '~/src/widgets/panel-views/channel';
import { Info } from '~/src/widgets/panel-views/info';
import { Language } from '~/src/widgets/panel-views/language';
import { Viindy } from '~/src/widgets/panel-views/viindy';
import Section from '../components/section';
import { focusComponent } from '../utils';
import { Chat } from './panel-views/chat';
import { IframeView } from './panel-views/iframe';
import { NonRender } from '../layout/non-render';

type PanelItems = NonNullable<
  ReturnType<typeof usePanel>['panel']['selection']
>;

export function Panel({ className }: { className?: string }) {
  const { panel } = usePanel();
  const wrapperRefs = React.useRef<Record<PanelItems, React.RefObject<View>>>({
    chat: React.createRef<View>(),
    viindy: React.createRef<View>(),
    language: React.createRef<View>(),
    channel: React.createRef<View>(),
    info: React.createRef<View>(),
    iframe: React.createRef<View>(),
  });

  React.useEffect(() => {
    const currentElement = wrapperRefs.current[panel.selection]?.current;
    if (!currentElement) return;
    if (currentElement instanceof HTMLElement === false) return;

    // get the first focusable child element
    const focusableElement = currentElement.querySelector<HTMLElement>(
      'button:not([tabindex="-1"]), input:not([tabindex="-1"]), textarea:not([tabindex="-1"]), select:not([tabindex="-1"]), a:not([tabindex="-1"])'
    );
    focusComponent(focusableElement);
  }, [panel.selection]);

  return (
    <Section className={cn(className, 'vi-flex')}>
      {Object.entries(viewMap).map(([key, { Wrapper, Content }]) => {
        return (
          <Wrapper
            key={key}
            hidden={key !== panel.selection}
            focusRef={wrapperRefs.current[key as PanelItems]}
          >
            <Content />
          </Wrapper>
        );
      })}
    </Section>
  );
}

const CenteredView: React.FC<{
  hidden?: boolean;
  children: React.ReactNode;
  focusRef?: React.RefObject<View>;
}> = ({ hidden, children, focusRef }) => (
  <View
    ref={focusRef}
    className={cn(
      'vi-flex vi-flex-1 vi-grow vi-items-center vi-justify-center',
      hidden && 'vi-hidden'
    )}
    aria-hidden={hidden}
  >
    {children}
  </View>
);

const NonRenderCentralView: React.FC<{
  hidden?: boolean;
  children: React.ReactNode;
  focusRef?: React.RefObject<View>;
}> = ({ hidden, children, focusRef }) => {
  return (
    <NonRender hidden={hidden}>
      <CenteredView focusRef={focusRef}>{children}</CenteredView>
    </NonRender>
  );
};

const NotRenderWrapper: React.FC<{
  hidden?: boolean;
  children: React.ReactNode;
  focusRef?: React.RefObject<View>;
}> = ({ hidden, children }) => {
  return (
    <NonRender hidden={hidden}>
      <View className="vi-flex vi-flex-1 vi-grow">{children}</View>
    </NonRender>
  );
};

const viewMap: Record<
  PanelItems,
  {
    Wrapper: React.FC<{
      hidden?: boolean;
      children: React.ReactNode;
      focusRef?: React.RefObject<View>;
    }>;
    Content: React.FC;
  }
> = {
  chat: {
    Wrapper: NotRenderWrapper,
    Content: () => <Chat size="small" className="vi-h-full" />,
  },
  viindy: {
    Wrapper: NonRenderCentralView,
    Content: Viindy,
  },
  iframe: {
    Wrapper: CenteredView,
    Content: IframeView,
  },
  language: {
    Wrapper: NonRenderCentralView,
    Content: () => (
      <ScrollView alwaysBounceVertical={false} className="vi-w-full">
        <Language />
      </ScrollView>
    ),
  },
  channel: {
    Wrapper: NonRenderCentralView,
    Content: () => (
      <ScrollView alwaysBounceVertical={false} className="vi-w-full">
        <ChannelView />
      </ScrollView>
    ),
  },
  info: {
    Wrapper: NonRenderCentralView,
    Content: () => (
      <ScrollView alwaysBounceVertical={false} className="vi-w-full">
        <Info />
      </ScrollView>
    ),
  },
};
