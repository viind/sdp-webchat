import { type Socket as SocketType, io } from 'socket.io-client';
import { TypedEventEmitter } from './typed-event-emitter';
import { z } from 'zod';

const botMessageSchema = z.object({
  text: z.string(),
  quick_replies: z.array(
    z.object({
      title: z.string(),
      payload: z.string(),
      content_type: z.literal('text'),
    })
  ),
  attachment: z.string().optional(),
});

export type BotSocketMessage = z.infer<typeof botMessageSchema> & {
  id: string;
};

const messageIdSchema = z.string();
const sessionConformationSchema = z.string();

const LikeToRatingMap = {
  like: 1,
  dislike: -1,
} as const;

export class Socket extends TypedEventEmitter<{
  botMessage: BotSocketMessage;
  disconnect: undefined;
  readyChanged: boolean;
  sessionConfirmed: z.infer<typeof sessionConformationSchema>;
}> {
  private socket: SocketType;
  private sessionId?: string;
  private customerId = '';
  private botMessageQueue: z.infer<typeof botMessageSchema>[] = [];

  private _isReady = false;
  private get isReady() {
    return this._isReady;
  }
  private set isReady(value: boolean) {
    if (value === this._isReady) return;
    this._isReady = value;
    this.emit('readyChanged', value);
  }

  public get connected() {
    return this.socket.connected;
  }

  constructor(
    private socketUrl: string,
    private socketPath: string,
    private logger: WebChatStore['logger']
  ) {
    super();

    this.socket = io(socketUrl, {
      path: socketPath,
      transports: ['websocket'],
      autoConnect: false,
      reconnectionAttempts: Infinity,
      reconnectionDelay: 1_000,
    });

    this.socket.on('connect', () => {
      const sessionId = this.sessionId ?? this.socket.id;
      this.logger.debug(
        `Socket: ${this.sessionId === undefined ? 'Request new session id' : 'Use existing session id'}`,
        sessionId
      );
      this.socket.emit('session_request', { session_id: sessionId });
      this.sessionId = sessionId;
    });

    this.socket.on('disconnect', (reason) => {
      this.logger.info(`Socket: disconnected because of ${reason}`);
      if (reason === 'io server disconnect') {
        this.logger.info(
          `Socket: attempt reconnect to ${this.socketUrl}${this.socketPath}`
        );
        this.socket.connect();
        return;
      }
      this.emit('disconnect', undefined);
    });

    this.socket.on('session_confirm', (sessionObject: unknown) => {
      // TODO: get rid of client side validation after backend connection is working
      const data = sessionConformationSchema.safeParse(sessionObject);
      if (data.success === false) {
        this.logger.error(
          `Socket: Failed to parse session confirmation. Unknown message format.`,
          data.error.message
        );
        return;
      }
      if (data.data !== this.sessionId) {
        this.logger.error(
          `Socket: session confirmation does not match the session id sent by the client`
        );
        return;
      }

      this.logger.debug(`Socket: session was confirmed`);
      this.logger.trace(`Socket: session details`, sessionObject);
      this.emit('sessionConfirmed', this.sessionId);
      this.isReady = true;
    });

    this.socket.on('bot_uttered', (botMessage: unknown) => {
      if (botMessage === undefined) {
        this.logger.trace(`Socket: finish signal for bot message`);
        return;
      }
      this.logger.trace(`Socket: bot message received`, botMessage);

      // TODO: get rid of client side validation after backend connection is working
      const data = botMessageSchema.safeParse(botMessage);
      if (data.success === false) {
        this.logger.error(
          `Socket: Failed to parse bot message. Unknown message format.`,
          data.error.message
        );
        return;
      }

      // actual logic
      this.botMessageQueue.push(data.data);
    });

    this.socket.on('message_id', (messageId: unknown) => {
      this.logger.trace(`Socket: message id received`, messageId);

      // TODO: get rid of client side validation after backend connection is working
      const data = messageIdSchema.safeParse(messageId);
      if (data.success === false) {
        this.logger.error(
          `Socket: Failed to parse message id. Unknown message format.`,
          data.error.message
        );
        return;
      }

      // actual logic
      const oldestMessage = this.botMessageQueue.shift();
      if (oldestMessage === undefined) {
        this.logger.error(
          `Socket: received message id, but no bot message is queued to be tagged with the id`
        );
        return;
      }

      this.emit('botMessage', { ...oldestMessage, id: data.data });
    });
  }

  public connect({
    sessionId,
    customerId,
  }: {
    sessionId?: string;
    customerId: string;
  }) {
    if (this.socket.connected) {
      return;
    }

    this.sessionId = sessionId;
    this.customerId = customerId;

    this.logger.info(`Socket: connect to ${this.socketUrl}${this.socketPath}`);

    this.socket.connect();
  }

  public disconnect() {
    this.socket.disconnect();
    this.isReady = false;
  }

  public sendUserMessage(
    message: string,
    additionalPayload?: { iframeUrl?: string }
  ) {
    if (this.isReady === false) {
      const message = `Socket: Tried to send a user message before the socket was ready. This is not allowed.`;
      this.logger.error(message);
      throw new Error(message);
    }

    this.logger.trace(`Socket: send user message ${message}`);
    this.socket.emit('user_uttered', {
      message,
      customData: {
        customer_id: this.customerId,
        second_window_url: additionalPayload?.iframeUrl,
      },
      session_id: this.sessionId,
    });
  }

  public sendFeedback({
    reaction,
    messageId,
    reason,
  }: {
    reaction?: 'like' | 'dislike';
    messageId: string;
    reason?: string;
  }) {
    if (this.isReady === false) {
      const message = `Socket: Tried to send a feedback before the socket was ready. This is not allowed.`;
      this.logger.error(message);
      throw new Error(message);
    }

    this.logger.trace(`Socket: send user feedback`, {
      reaction,
      messageId,
      reason,
    });
    this.socket.emit('feedback', {
      rating: reaction ? LikeToRatingMap[reaction] : 0,
      rating_reason: reason ?? '',
      message_id: messageId,
    });
  }
}
