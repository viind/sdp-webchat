export class TypedEventEmitter<T extends Record<string, unknown>> {
  private listeners = {} as Record<
    keyof T,
    ((data: unknown) => void)[] | undefined
  >;

  on<K extends keyof T>(event: K, listener: (data: T[K]) => void) {
    if (!this.listeners[event]) {
      this.listeners[event] = [];
    }
    this.listeners[event].push(listener as (data: unknown) => void);
  }

  off<K extends keyof T>(event: K, listener: (data: T[K]) => void) {
    if (!this.listeners[event]) {
      return;
    }
    this.listeners[event] = this.listeners[event].filter((l) => l !== listener);
  }

  protected emit<K extends keyof T>(event: K, data: T[K]) {
    if (!this.listeners[event]) {
      return;
    }
    this.listeners[event].forEach((listener) => listener(data));
  }
}
