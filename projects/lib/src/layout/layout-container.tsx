import * as React from 'react';
import { Platform } from 'react-native';
import { cn } from '~/lib/utils';
import { useFeature } from '../provider/feature-provider';
import { useSidePanel } from '../provider/side-panel-provider';
import { useStore } from '../provider/store-provider';
import { useChatOpen } from '../provider/chat-open-provider';
import { focusComponent } from '../utils';
import { NonRender } from './non-render';

type Variances =
  | 'container'
  | 'panelNavigation'
  | 'panel'
  | 'header'
  | 'chat'
  | 'closeButton'
  | 'shadow';

type DynamicOrStaticConfig<T> =
  | T
  | ((
      isSidePanelOpen: boolean,
      config: {
        windowPaddingBottom?: string;
        windowPaddingRight?: string;
        windowCloseButtonPaddingBottom?: string;
      }
    ) => T);

const layoutConfig: Record<
  ReturnType<typeof useFeature>['view'],
  {
    grid: DynamicOrStaticConfig<{
      rows: string;
      cols: string;
      style: Partial<Record<Variances, React.CSSProperties>>;
      tw: Partial<Record<Variances, string>>;
    }>;
    hidden: DynamicOrStaticConfig<Partial<Record<Variances, boolean>>>;
    tw: DynamicOrStaticConfig<Partial<Record<Variances, string>>>;
    showPanelNavigationAtTheEnd: boolean;
    renderHeaderAtTheTop: boolean;
  }
> = {
  'desktop-fullscreen': {
    grid: {
      rows: 'min-content 1fr',
      cols: 'min-content 1fr 1fr',
      style: {
        panelNavigation: { gridArea: '1 / 1 / 3 / 2' },
        panel: { gridArea: '1 / 2 / 3 / 3' },
        header: { gridArea: '1 / 3 / 2 / 4' },
        chat: { gridArea: '2 / 3 / 3 / 4' },
      },
      tw: {
        container: cn('vi-pointer-events-auto vi-h-full'),
        header: cn('vi-bg-background'),
        chat: cn('vi-flex vi-overflow-hidden'),
      },
    },
    hidden: {
      panelNavigation: false,
      panel: false,
      header: false,
      chat: false,
      closeButton: true,
      shadow: true,
    },
    tw: {
      panel: cn(
        'vi-h-full vi-border-r vi-border-border-message vi-bg-background'
      ),
      chat: cn('vi-flex-1 vi-bg-background'),
      header: cn('vi-rounded-bl-[3.25rem]'),
    },
    showPanelNavigationAtTheEnd: false,
    renderHeaderAtTheTop: false,
  },
  'desktop-windowed': {
    grid: (isPanelOpen, config) => ({
      rows: 'min-content 1fr',
      cols: isPanelOpen
        ? '1fr min-content minmax(0, 25rem) minmax(0, 25rem) min-content'
        : '1fr min-content 0 minmax(0, 25rem) min-content',
      style: {
        container: {
          paddingBottom: config.windowPaddingBottom,
          paddingRight: config.windowPaddingRight,
        },
        panelNavigation: { gridArea: '1 / 2 / 3 / 3' },
        panel: { gridArea: '1 / 3 / 3 / 4' },
        header: { gridArea: '1 / 4 / 2 / 5' },
        chat: { gridArea: '2 / 4 / 3 / 5' },
        closeButton: {
          gridArea: '1 / 5 / 3 / 6',
          paddingBottom: config.windowCloseButtonPaddingBottom,
        },
        shadow: { gridArea: '1 / 3 / 3 / 5' },
      },
      tw: {
        container: cn('vi-h-[50rem] vi-pb-16 vi-pr-8'),
        header: cn(
          'vi-pointer-events-auto vi-rounded-tr-2xl vi-bg-background',
          isPanelOpen === false && 'vi-rounded-tl-2xl'
        ),
        chat: cn('vi-overflow-hidden vi-flex'),
      },
    }),
    hidden: (isPanelOpen) => ({
      panelNavigation: false,
      panel: isPanelOpen === false,
      header: false,
      chat: false,
      closeButton: false,
      shadow: false,
    }),
    tw: (isPanelOpen) => ({
      panel: cn(
        'vi-pointer-events-auto vi-h-full vi-rounded-l-2xl vi-bg-background',
        isPanelOpen && 'vi-border-r vi-border-border-message'
      ),
      chat: cn(
        'vi-pointer-events-auto vi-flex-1 vi-rounded-br-2xl vi-bg-background',
        isPanelOpen === false && 'vi-rounded-bl-2xl'
      ),
      shadow: cn('vi-rounded-2xl'),
      header: cn(
        'vi-rounded-bl-[3.25rem] vi-rounded-tr-2xl',
        isPanelOpen === false && 'vi-rounded-tl-2xl'
      ),
    }),
    showPanelNavigationAtTheEnd: false,
    renderHeaderAtTheTop: false,
  },
  'mobile': {
    grid: {
      rows: 'min-content 1fr min-content',
      cols: '1fr',
      style: {
        panelNavigation: { gridArea: '3 / 1 / 4 / 2' },
        panel: { gridArea: '2 / 1 / 3 / 2' },
        header: { gridArea: '1 / 1 / 2 / 2' },
      },
      tw: {
        container: cn('vi-pointer-events-auto vi-h-full'),
        header: cn('vi-bg-background'),
        panel: cn('vi-overflow-hidden vi-flex vi-bg-background'),
      },
    },
    hidden: {
      panelNavigation: false,
      panel: false,
      header: false,
      chat: true,
      closeButton: true,
      shadow: true,
    },
    tw: {
      panel: cn('vi-flex-1 vi-bg-background'),
      header: cn('vi-rounded-bl-[3.25rem]'),
    },
    showPanelNavigationAtTheEnd: true,
    renderHeaderAtTheTop: true,
  },
  'embedded': {
    grid: {
      rows: 'min-content 1fr min-content',
      cols: '1fr',
      style: {
        panelNavigation: { gridArea: '3 / 1 / 4 / 2' },
        panel: { gridArea: '2 / 1 / 3 / 2' },
        header: { gridArea: '1 / 1 / 2 / 2' },
      },
      tw: {
        container: cn('vi-pointer-events-auto vi-h-full'),
        panel: cn('vi-overflow-hidden vi-bg-background'),
      },
    },
    hidden: {
      panelNavigation: false,
      panel: false,
      header: false,
      chat: true,
      closeButton: true,
      shadow: true,
    },
    tw: {
      header: cn('vi-rounded-t-3xl'),
      panelNavigation: cn('vi-rounded-b-3xl'),
      panel: cn('vi-h-full'),
    },
    showPanelNavigationAtTheEnd: true,
    renderHeaderAtTheTop: true,
  },
};

export function LayoutContainer(
  config: Parameters<typeof LayoutContainerWeb>[0]
) {
  if (Platform.OS === 'web') {
    return <LayoutContainerWeb {...config} />;
  }
  throw new Error('LayoutContainerNative is not implemented.');
}

export function LayoutContainerWeb({
  PanelNavigation,
  Panel,
  Header,
  Chat,
  CloseButton,
  className,
}: {
  PanelNavigation: React.FC<{ className?: string }>;
  Panel: React.FC<{ className?: string }>;
  Header: React.FC<{ className?: string }>;
  Chat: React.FC<{ className?: string }>;
  CloseButton: React.FC<{ className?: string }>;
  className: string;
}) {
  const { view, isEmbedded } = useFeature();
  const { isSidePanelOpen } = useSidePanel();
  const windowPaddingBottom = useStore(
    (state) => state.positioning.windowPaddingBottom
  );
  const windowPaddingRight = useStore(
    (state) => state.positioning.windowPaddingRight
  );
  const windowCloseButtonPaddingBottom = useStore(
    (state) => state.positioning.windowCloseButtonPaddingBottom
  );
  const { isOpen } = useChatOpen();
  const containerRef = React.useRef<HTMLDivElement>(null);

  const {
    grid: gridDynamic,
    hidden: hiddenDynamic,
    tw: twDynamic,
    showPanelNavigationAtTheEnd,
    renderHeaderAtTheTop,
  } = layoutConfig[view];

  const {
    cols,
    rows,
    style: gridStyle = {},
    tw: gridTw,
  } = typeof gridDynamic === 'function'
    ? gridDynamic(isSidePanelOpen, {
        windowPaddingBottom,
        windowPaddingRight,
        windowCloseButtonPaddingBottom,
      })
    : gridDynamic;

  const { panelNavigation, panel, header, chat, closeButton, shadow } =
    typeof hiddenDynamic === 'function'
      ? hiddenDynamic(isSidePanelOpen, {
          windowPaddingBottom,
          windowPaddingRight,
          windowCloseButtonPaddingBottom,
        })
      : hiddenDynamic;

  const tw =
    typeof twDynamic === 'function'
      ? twDynamic(isSidePanelOpen, {
          windowPaddingBottom,
          windowPaddingRight,
          windowCloseButtonPaddingBottom,
        })
      : twDynamic;

  /*
   * Hint: Keep in mind that the order of the divs here
   * defines the tab order
   */
  useFocusLoop(containerRef, {
    allowEscape: view === 'desktop-windowed' || view === 'embedded',
  });

  const panelNavigationWrapper = (
    <NonRender hidden={panelNavigation}>
      <div
        className={cn(gridTw.panelNavigation)}
        style={gridStyle.panelNavigation}
      >
        <PanelNavigation className={tw.panelNavigation} />
      </div>
    </NonRender>
  );

  const headerWrapper = (
    <NonRender hidden={header}>
      <div className={cn(gridTw.header)} style={gridStyle.header}>
        <Header className={tw.header} />
      </div>
    </NonRender>
  );

  return (
    <div
      ref={containerRef}
      className={cn(gridTw.container, 'vi-grid vi-w-full', className)}
      style={{
        ...gridStyle.container,
        gridTemplateColumns: cols,
        gridTemplateRows: rows,
      }}
      role={view === 'embedded' ? undefined : 'dialog'}
      // the open state is never changed in case of embedded mode
      aria-modal={
        view !== 'desktop-fullscreen' ? undefined : isEmbedded || isOpen
      }
    >
      {renderHeaderAtTheTop && headerWrapper}
      {showPanelNavigationAtTheEnd === false && panelNavigationWrapper}
      <div
        className={cn(gridTw.panel, panel && 'vi-hidden')}
        style={gridStyle.panel}
        aria-hidden={panel}
      >
        <Panel className={tw.panel} />
      </div>
      {renderHeaderAtTheTop === false && headerWrapper}
      <NonRender hidden={chat}>
        <div className={cn(gridTw.chat)} style={gridStyle.chat}>
          <Chat className={tw.chat} />
        </div>
      </NonRender>
      <NonRender hidden={closeButton}>
        <div
          className={cn(
            gridTw.closeButton,
            'vi-flex vi-flex-col-reverse vi-pl-4'
          )}
          style={gridStyle.closeButton}
        >
          <CloseButton className={tw.closeButton} />
        </div>
      </NonRender>
      {showPanelNavigationAtTheEnd && panelNavigationWrapper}
      <NonRender hidden={shadow}>
        <div
          className={cn(
            'vi-shadow-[rgba(17,_17,_26,_0.1)_0px_0px_16px]',
            tw.shadow
          )}
          style={gridStyle.shadow}
        ></div>
      </NonRender>
    </div>
  );
}

/**
 * Hook to keep focus within a container, bringing focus back to the first
 * or last focusable element when focus moves outside the container
 */
function useFocusLoop(
  containerRef: React.RefObject<HTMLElement>,
  options?: {
    /** When true, allows focus to escape the container */
    allowEscape?: boolean;
    /** Selector for finding focusable elements */
    focusableSelector?: string;
  }
) {
  React.useEffect(() => {
    const container = containerRef.current;
    if (!container) return;

    const {
      allowEscape = false,
      focusableSelector = 'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])',
    } = options ?? {};

    let lastKeyDown: KeyboardEvent | null = null;

    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.key === 'Tab') {
        lastKeyDown = e;
      }
    };

    const handleFocus = (e: FocusEvent) => {
      const isInContainer = container.contains(e.target as Node);

      if (!isInContainer && !allowEscape) {
        const focusableElements = Array.from(
          container.querySelectorAll<HTMLElement>(focusableSelector)
        );

        // If shift+tab was pressed, focus the last element
        // Otherwise focus the first element
        const elementToFocus = lastKeyDown?.shiftKey
          ? focusableElements[focusableElements.length - 1]
          : focusableElements[0];

        focusComponent(elementToFocus);
      }
    };

    document.addEventListener('keydown', handleKeyDown);
    document.addEventListener('focusin', handleFocus);
    return () => {
      document.removeEventListener('keydown', handleKeyDown);
      document.removeEventListener('focusin', handleFocus);
    };
  }, [containerRef, options?.allowEscape, options?.focusableSelector]);
}
