import * as React from 'react';
import { Platform, View } from 'react-native';
import { useChatOpen } from '../provider/chat-open-provider';
import { useFeature } from '../provider/feature-provider';

import { cn } from '~/lib/utils';
import { OpenDialogButton } from '../components/open-dialog-buttons';
import { useDialog } from '../provider/dialog-provider';
import { useStore } from '../provider/store-provider';

export function OpenWrapper({
  App,
}: {
  App: React.FC<{ 'className': string; 'aria-hidden'?: boolean }>;
}) {
  const { view, isEmbedded } = useFeature();
  const { isOpen } = useChatOpen();
  const paddingBottom = useStore(
    (state) => state.positioning.openButtonPaddingBottom
  );
  const paddingRight = useStore(
    (state) => state.positioning.openButtonPaddingRight
  );
  hideParentScrollingHook(view, isOpen, isEmbedded);
  backendConnectionHook(isOpen, view);

  // only render these if we use overlay
  const ariaExpanded = isEmbedded ? undefined : isOpen;
  const renderOpenButton = isEmbedded === false;

  const openButtonHidden = isOpen;
  const appHidden = isEmbedded ? false : isOpen === false;

  return (
    <View
      aria-expanded={ariaExpanded}
      className={cn(
        'vi-pointer-events-none vi-flex vi-size-full vi-items-end vi-justify-end'
      )}
    >
      {renderOpenButton === false || isOpen ? null : (
        <View
          style={{
            paddingBottom: paddingBottom,
            paddingRight: paddingRight,
          }}
          className={cn('vi-pb-16 vi-pr-8', openButtonHidden && 'vi-hidden')}
          aria-hidden={openButtonHidden}
        >
          <OpenDialogButton className="vi-pointer-events-auto" />
        </View>
      )}
      <App className={cn(appHidden && 'vi-hidden')} aria-hidden={appHidden} />
    </View>
  );
}

function backendConnectionHook(
  isOpen: boolean,
  view: ReturnType<typeof useFeature>['view']
) {
  const { startChat, stopChat } = useDialog();

  React.useEffect(() => {
    if (shouldStartChat(isOpen, view)) {
      // we can't wait withing a `useEffect` call so we are not waiting for the promise to resolve
      void startChat();
    } else {
      stopChat();
    }
    return () => stopChat();
  }, [isOpen, view]);

  function shouldStartChat(
    isOpen: boolean,
    view: ReturnType<typeof useFeature>['view']
  ) {
    if (view === 'embedded' || view === 'desktop-fullscreen') {
      return true;
    }
    return isOpen;
  }
}

function hideParentScrollingHook(
  view: ReturnType<typeof useFeature>['view'],
  isOpen: boolean,
  isEmbedded: boolean
) {
  React.useEffect(() => {
    if (Platform.OS !== 'web') {
      return;
    }
    document.body.style.overflow = allowScrollingOfParentPage(
      view,
      isOpen,
      isEmbedded
    )
      ? 'auto'
      : 'hidden';
  }, [view, isOpen]);

  function allowScrollingOfParentPage(
    view: ReturnType<typeof useFeature>['view'],
    isOpen: boolean,
    isEmbedded: boolean
  ) {
    if (view === 'embedded') {
      // when it is embedded, then it should allow to scroll
      return true;
    }

    if (view === 'desktop-windowed') {
      // when it is windowed, then it should allow to scroll
      return true;
    }

    if (view === 'desktop-fullscreen' && isEmbedded) {
      // in this case the isOpen is always false,
      // as it is technically never open in case of embedded
      return false;
    }

    // only allow scrolling of the parent page when the chat is closed
    return isOpen === false;
  }
}
