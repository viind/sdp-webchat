import * as React from 'react';
import { View } from 'react-native';
import { cn } from '~/lib/utils';
import { applyScrollbarStyling } from '../hooks/scrollbar-styling';
import { useFeature } from '../provider/feature-provider';
import { applyFullHeightToParents } from '../hooks/apply-full-hight-to-parents';

export function PositionWrapper({ children }: { children: React.ReactNode }) {
  const ref = React.useRef<View>(null);
  const { view } = useFeature();

  // eslint-disable-next-line react-compiler/react-compiler
  applyScrollbarStyling(ref);

  // eslint-disable-next-line react-compiler/react-compiler
  applyFullHeightToParents({
    shouldBeApplied: true,
    ref,
  });

  // todo: consider using the popover API of newer browsers
  return (
    <View
      ref={ref}
      className={cn(
        'vi-h-full',
        view !== 'embedded' &&
          `vi-pointer-events-none vi-fixed vi-bottom-0 vi-right-0 vi-h-dvh vi-w-full`
      )}
      style={
        view !== 'embedded' && {
          backgroundColor: 'transparent',
          pointerEvents: 'box-none',
        }
      }
    >
      {children}
    </View>
  );
}
