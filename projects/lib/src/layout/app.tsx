import { View } from 'react-native';
import {
  DesktopCloseButton,
  MobileCloseButton,
  WindowedCloseButton,
} from '../components/close-dialog-button';
import {
  DesktopFullscreenToggle,
  MobileFullscreenToggle,
  WindowedFullscreenToggle,
} from '../components/fullscreen-button';
import { useFeature } from '../provider/feature-provider';
import { DialogHeader } from '../widgets/dialog-header';
import { Panel } from '../widgets/panel';
import { PanelNavigation } from '../widgets/panel-navigation';
import { Chat } from '../widgets/panel-views/chat';
import { LayoutContainer } from './layout-container';
import { OpenWrapper } from './open-wrapper';
import { PositionWrapper } from './position-wrapper';

export function App() {
  return (
    <PositionWrapper>
      <OpenWrapper App={LayoutWrapper} />
    </PositionWrapper>
  );
}

function LayoutWrapper({ className }: { className: string }) {
  return (
    <LayoutContainer
      className={className}
      PanelNavigation={PanelNavigation}
      Panel={Panel}
      Header={DialogHeaderWrapper}
      Chat={ChatWrapper}
      CloseButton={CloseButton}
    />
  );
}

function ChatWrapper({ className }: { className?: string }) {
  const { view } = useFeature();

  const chatSize = view === 'desktop-fullscreen' ? 'big' : 'small';
  return <Chat size={chatSize} className={className} />;
}

function DialogHeaderWrapper({ className }: { className?: string }) {
  return (
    <DialogHeader className={className}>
      <View className="vi-flex vi-flex-row vi-items-center">
        <HeaderFullScreenButton />
        <HeaderCloseButton />
      </View>
    </DialogHeader>
  );
}

function HeaderFullScreenButton() {
  const { view, showHeaderFullScreen } = useFeature();
  if (showHeaderFullScreen === false) {
    return <></>;
  }

  if (view === 'desktop-fullscreen') {
    return <DesktopFullscreenToggle />;
  }
  if (view === 'desktop-windowed') {
    return <WindowedFullscreenToggle />;
  }
  if (view === 'mobile' || view === 'embedded') {
    return <MobileFullscreenToggle />;
  }
  return <></>;
}

function HeaderCloseButton() {
  const { view, showHeaderClose, isEmbedded } = useFeature();
  if (showHeaderClose === false || isEmbedded) {
    return <></>;
  }
  if (view === 'desktop-fullscreen') {
    return <DesktopCloseButton />;
  }
  if (view === 'mobile') {
    return <MobileCloseButton />;
  }

  return <></>;
}

function CloseButton({ className }: { className?: string }) {
  const { view } = useFeature();
  if (view === 'desktop-windowed') {
    return <WindowedCloseButton className={className} />;
  }

  return <></>;
}
