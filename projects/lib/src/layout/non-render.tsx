import { useChatOpen } from '../provider/chat-open-provider';

export function NonRender({
  children,
  hidden,
}: {
  children: React.ReactNode;
  hidden?: boolean;
}) {
  const { isOpen } = useChatOpen();

  if (isOpen === false) return null;
  if (hidden) return null;

  return children;
}
