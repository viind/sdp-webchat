import { z } from 'zod';
import { createSliceConfig } from './type';

declare global {
  interface WebChatStore {
    avatar: AvatarStore;
  }
}

type AvatarStore = {
  setAvatar: (avatar: z.infer<typeof avatarSchema>) => void;
} & z.infer<typeof avatarSchema>;

const avatarSchema = z.object({
  headerUrl: z.string().optional(),
  // always fully rounded
  smallOpenButtonUrl: z.string().optional(),
  bigOpenButtonUrl: z.string().optional(),
  headerRoundLevel: z.number().min(0).max(100).optional(),
  bigOpenButtonRoundLevel: z.number().min(0).max(100).optional(),
  sidePanelUrl: z.string().optional(),
});

export const avatarSliceConfig = createSliceConfig(
  (set): AvatarStore => {
    return {
      setAvatar: (config) =>
        set((state) => ({
          avatar: {
            ...state.avatar,
            ...config,
          },
        })),
    };
  },
  z.object({
    avatar: avatarSchema.optional(),
  }),
  (store, config) => {
    if (!config.avatar) return;
    store.avatar.setAvatar(config.avatar);
  }
);
