import { z } from 'zod';
import { createSliceConfig } from './type';

declare global {
  interface WebChatStore {
    header: HeaderStore;
  }
}

type HeaderStore = {
  setTitle: (title: string) => void;
  setDescription: (description: string) => void;
} & z.infer<typeof headerObjectSchema>;

const headerObjectSchema = z.object({
  title: z.string().optional(),
  description: z.string().optional(),
});

export const headerSliceConfig = createSliceConfig(
  (set): HeaderStore => {
    return {
      setTitle: (title: string) =>
        set((state) => ({ header: { ...state.header, title } })),
      setDescription: (description: string) =>
        set((state) => ({ header: { ...state.header, description } })),
    };
  },
  z.object({ header: headerObjectSchema.optional() }),
  (store, config) => {
    if (config.header?.title) {
      store.header.setTitle(config.header.title);
    }
    if (config.header?.description) {
      store.header.setDescription(config.header.description);
    }
  }
);
