import { z } from 'zod';
import { createSliceConfig } from './type';

declare global {
  interface WebChatStore {
    iframe: IframeStore;
  }
}

type IframeStore = {
  openIframe: (config: z.infer<typeof iframeConfigSchema>) => void;
  closeIframe: () => void;
  iframeConfig: z.infer<typeof iframeConfigSchema> | undefined;
  isIframeOpen: boolean;
};

const iframeConfigSchema = z.object({
  pageTitle: z.string(),
  url: z.string(),
});

export const iframeSliceConfig = createSliceConfig(
  (set): IframeStore => {
    return {
      iframeConfig: undefined,
      isIframeOpen: false,
      openIframe: (config) =>
        set((state) => ({
          iframe: {
            ...state.iframe,
            iframeConfig: config,
            isIframeOpen: true,
          },
        })),
      closeIframe: () =>
        set((state) => ({
          iframe: {
            ...state.iframe,
            iframeConfig: undefined,
            isIframeOpen: false,
          },
        })),
    };
  },
  undefined,
  undefined,
  z.object({
    iframeConfig: iframeConfigSchema.optional(),
    isIframeOpen: z.boolean(),
  }),
  (partialStore) => ({
    iframeConfig: partialStore.iframeConfig,
    isIframeOpen: partialStore.isIframeOpen,
  }),
  'iframe'
);
