import { z } from 'zod';
import { createSliceConfig } from './type';
import type { CSSProperties } from 'react';

declare global {
  interface WebChatStore {
    theme: ThemeStore;
  }
}

declare module 'react' {
  interface CSSProperties {
    '--viind-background'?: string;
    '--viind-foreground'?: string;
    '--viind-foreground-primary'?: string;
    '--viind-primary'?: string;
    '--viind-primary-foreground'?: string;
    '--viind-user-message'?: string;
    '--viind-user-message-foreground'?: string;
    '--viind-bot-message'?: string;
    '--viind-bot-message-foreground'?: string;
    '--viind-border'?: string;
    '--viind-border-message'?: string;
    '--viind-success'?: string;
    '--viind-success-foreground'?: string;
    '--viind-error'?: string;
    '--viind-error-foreground'?: string;
    '--viind-scrollbar-thumb'?: string;
    '--viind-background-focus'?: string;
    '--viind-primary-focus'?: string;
    '--viind-bot-message-focus'?: string;
  }
}

type ThemeStore = {
  setTheme: (theme: ThemeColors) => void;
  setScrollbarStyling: (scrollbarStyling?: string) => void;
  colors: ThemeColors;
  variables: CSSProperties;
  scrollbarStyling?: string;
};

const themeColors = z.object({
  background: z.string().optional(),
  backgroundFocus: z.string().optional(),
  foreground: z.string().optional(),
  foregroundPrimary: z.string().optional(),
  primary: z.string().optional(),
  primaryForeground: z.string().optional(),
  primaryFocus: z.string().optional(),
  userMessage: z.string().optional(),
  userMessageForeground: z.string().optional(),
  botMessage: z.string().optional(),
  botMessageForeground: z.string().optional(),
  botMessageFocus: z.string().optional(),
  border: z.string().optional(),
  borderMessage: z.string().optional(),
  success: z.string().optional(),
  successForeground: z.string().optional(),
  error: z.string().optional(),
  errorForeground: z.string().optional(),
  scrollbarThumb: z.string().optional(),
});

type ThemeColors = z.infer<typeof themeColors>;

const jsThemeToVariableMap = {
  background: '--viind-background',
  backgroundFocus: '--viind-background-focus',
  foreground: '--viind-foreground',
  foregroundPrimary: '--viind-foreground-primary',
  primary: '--viind-primary',
  primaryForeground: '--viind-primary-foreground',
  primaryFocus: '--viind-primary-focus',
  userMessage: '--viind-user-message',
  userMessageForeground: '--viind-user-message-foreground',
  botMessage: '--viind-bot-message',
  botMessageForeground: '--viind-bot-message-foreground',
  botMessageFocus: '--viind-bot-message-focus',
  border: '--viind-border',
  borderMessage: '--viind-border-message',
  success: '--viind-success',
  successForeground: '--viind-success-foreground',
  error: '--viind-error',
  errorForeground: '--viind-error-foreground',
  scrollbarThumb: '--viind-scrollbar-thumb',
} satisfies Record<keyof NonNullable<ThemeColors>, keyof CSSProperties>;

export const themeSliceConfig = createSliceConfig(
  (set, get): ThemeStore => {
    return {
      colors: {},
      variables: {},
      setTheme: (theme) => {
        const variables: CSSProperties = {};
        (
          Object.entries(theme) as [keyof NonNullable<ThemeColors>, string][]
        ).forEach(([key, value]) => {
          const result = convertStringToValidColor(value);
          if (result.success) {
            variables[jsThemeToVariableMap[key]] = result.value;
          } else {
            get().logger.error(
              `Invalid color value for ${key}: ${result.error}`
            );
          }
        });

        set((state) => ({
          theme: { ...state.theme, colors: theme, variables },
        }));
      },
      setScrollbarStyling: (scrollbarStyling) => {
        set((state) => ({
          theme: { ...state.theme, scrollbarStyling },
        }));
      },
    };
  },
  z.object({
    theme: themeColors.optional(),
    scrollbarStyling: z.string().optional(),
  }),
  (store, config) => {
    if (config.theme) {
      store.theme.setTheme(config.theme);
    }
    if (config.scrollbarStyling) {
      store.theme.setScrollbarStyling(config.scrollbarStyling);
    }
  }
);

const hexRegex = /^#([A-Fa-f0-9]{6})$/;
const colorValue = /^\d{1,3} \d{1,3} \d{1,3}$/;

function convertStringToValidColor(
  value: string
): { success: true; value: string } | { success: false; error: string } {
  if (hexRegex.test(value)) {
    return convertHexToRgb(value);
  }
  if (colorValue.test(value)) {
    return { success: true, value };
  }
  return {
    success: false,
    error: `Invalid color value. Expected hex or rgb value, but got '${value}'`,
  };
}

function convertHexToRgb(
  value: string
): { success: true; value: string } | { success: false; error: string } {
  const hex = value.replace('#', '');
  const red = parseInt(hex.substring(0, 2), 16);
  const green = parseInt(hex.substring(2, 4), 16);
  const blue = parseInt(hex.substring(4, 6), 16);

  if (red > 255 || green > 255 || blue > 255) {
    return {
      success: false,
      error: `Invalid color value. Expected hex value, but got '${value}'`,
    };
  }
  return { success: true, value: `${red} ${green} ${blue}` };
}
