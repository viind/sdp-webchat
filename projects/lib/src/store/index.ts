import { createStore as createZustandStore } from 'zustand';
import {
  persist,
  createJSONStorage,
  subscribeWithSelector,
} from 'zustand/middleware';
import deepmerge from 'deepmerge';

import { loggerSliceConfig } from './logger';
import { headerSliceConfig } from './header';
import { channelSliceConfig } from './channel';
import { infoSliceConfig } from './info';
import { z } from 'zod';
import { positioningSliceConfig } from './positioning';
import { languageSliceConfig } from './language';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Platform } from 'react-native';
import { avatarSliceConfig } from './avatar';
import { ttsSstSliceConfig } from './tts-sst';
import { themeSliceConfig } from './theme';
import { iframeSliceConfig } from './iframe';

declare global {
  interface WebChatStore {
    isHydrated: boolean;
  }

  type WebChatStoreInstance = ReturnType<typeof createStore>;
}

const configs = [
  channelSliceConfig,
  headerSliceConfig,
  loggerSliceConfig,
  infoSliceConfig,
  positioningSliceConfig,
  languageSliceConfig,
  avatarSliceConfig,
  ttsSstSliceConfig,
  themeSliceConfig,
  iframeSliceConfig,
] as const;

const storageStrategySchema = z
  .union([z.literal('localStorage'), z.literal('sessionStorage')])
  .optional();

export const storeSchema = z
  .object({ storageStrategy: storageStrategySchema })
  .merge(headerSliceConfig.schema)
  .merge(channelSliceConfig.schema)
  .merge(loggerSliceConfig.schema)
  .merge(channelSliceConfig.schema)
  .merge(infoSliceConfig.schema)
  .merge(positioningSliceConfig.schema)
  .merge(languageSliceConfig.schema)
  .merge(avatarSliceConfig.schema)
  .merge(ttsSstSliceConfig.schema)
  .merge(themeSliceConfig.schema);

export function createStore(config: z.infer<typeof storeSchema>) {
  const useStore = createZustandStore<WebChatStore>()(
    subscribeWithSelector(
      persist(
        (...a) => ({
          logger: loggerSliceConfig.slice(...a),
          header: headerSliceConfig.slice(...a),
          channel: channelSliceConfig.slice(...a),
          info: infoSliceConfig.slice(...a),
          positioning: positioningSliceConfig.slice(...a),
          language: languageSliceConfig.slice(...a),
          avatar: avatarSliceConfig.slice(...a),
          isHydrated: false,
          ttsStt: ttsSstSliceConfig.slice(...a),
          theme: themeSliceConfig.slice(...a),
          iframe: iframeSliceConfig.slice(...a),
        }),
        {
          name: 'viindWebChat',
          storage: createJSONStorage(() =>
            Platform.OS !== 'web'
              ? AsyncStorage
              : config.storageStrategy === 'localStorage'
                ? localStorage
                : sessionStorage
          ),
          partialize: (state) =>
            Object.fromEntries(
              configs
                .filter((config) => config.persist)
                .map(
                  (config) =>
                    [
                      config.storeKey!,
                      // @ts-expect-error This is to complex to type correctly here.
                      // All the functions around it already ensure that this actually works
                      config.persist!(state[config.storeKey!]),
                    ] as const
                )
            ),
          merge: (persisted, current) => {
            // this is needed in case that the used store is synchronous
            // because `merge` is otherwise called before the store variable is even assigned
            setTimeout(() => useStore.setState({ isHydrated: true }));

            if (typeof persisted !== 'object') return current;
            const schema = z.object(
              Object.fromEntries(
                configs
                  .filter((config) => config.persist)
                  .map(
                    (config) =>
                      [config.storeKey!, config.persistSchema!] as const
                  )
              )
            );

            const parsed = schema.safeParse(persisted);
            const logLater = (
              ...args: Parameters<WebChatStore['logger']['error']>
            ) => {
              // this is needed in case that the used store is synchronous
              // because `merge` is otherwise called before the store variable is even assigned
              setTimeout(() => {
                useStore.getState().logger.error(...args);
              });
            };

            if (!parsed.success) {
              logLater(
                `StoredData: Stored data does not match the schema. Error: ${parsed.error}`,
                {
                  persisted,
                  current,
                  error: parsed.error,
                }
              );
              return current;
            }

            try {
              return deepmerge(
                current,
                parsed.data as unknown as typeof current
              );
            } catch (e) {
              logLater(
                `StoredData: Unable to merge persisted data with current data. Error: ${e}`,
                {
                  persisted,
                  current,
                }
              );
              return current;
            }
          },
        }
      )
    )
  );

  const state = useStore.getState();
  configs.forEach((sliceConfig) => {
    sliceConfig.initState?.(state, config);
  });

  return useStore;
}
