import type { z } from 'zod';
import type { StateCreator } from 'zustand';
type StoreSlice<T> = StateCreator<WebChatStore, [], [], T>;

type RemoveFunctions<T> = {
  [K in keyof T as T[K] extends (...args: any[]) => any ? never : K]: T[K];
};

type DeepPartial<T> = {
  [K in keyof T as K extends keyof RemoveFunctions<T>
    ? K
    : never]?: T[K] extends object ? DeepPartial<T[K]> : T[K];
};

type GetStoreKey<T> = keyof {
  [K in keyof WebChatStore as WebChatStore[K] extends T ? K : never]: T;
};

export function createSliceConfig<T>(slice: StoreSlice<T>): {
  slice: StoreSlice<T>;
  schema: undefined;
  initState: undefined;
  persistSchema: undefined;
  persist: undefined;
  storeKey: undefined;
};

export function createSliceConfig<T, Schema extends z.ZodType>(
  slice: StoreSlice<T>,
  schema: Schema,
  initState: (store: WebChatStore, config: z.infer<Schema>) => void
): {
  slice: StoreSlice<T>;
  schema: Schema;
  initState: (store: WebChatStore, config: z.infer<Schema>) => void;
  persistSchema: undefined;
  persist: undefined;
  storeKey: undefined;
};

export function createSliceConfig<
  T,
  Schema extends z.ZodType,
  PersistSchema extends z.ZodType<DeepPartial<T>>,
>(
  slice: StoreSlice<T>,
  schema: Schema | undefined,
  initState:
    | ((store: WebChatStore, config: z.infer<Schema>) => void)
    | undefined,
  persistSchema: PersistSchema,
  persist: (partialStore: T) => z.infer<PersistSchema>,
  storeKey: GetStoreKey<T>
): {
  slice: StoreSlice<T>;
  schema: Schema;
  initState: (store: WebChatStore, config: z.infer<Schema>) => void;
  persistSchema: PersistSchema;
  persist: (partialStore: T) => z.infer<PersistSchema>;
  storeKey: GetStoreKey<T>;
};

/**
 * This function only exists to make the setup process type safe and allow typescript to apply inference.
 * So, no need to manually type it
 */
export function createSliceConfig<
  T,
  Schema extends z.ZodType,
  PersistSchema extends z.ZodType<DeepPartial<T>>,
>(
  slice: StoreSlice<T>,
  schema?: Schema,
  initState?: (store: WebChatStore, config: z.infer<Schema>) => void,
  persistSchema?: PersistSchema,
  persist?: (partialStore: T) => z.infer<PersistSchema>,
  storeKey?: GetStoreKey<T>
) {
  return {
    slice,
    schema,
    initState,
    persistSchema,
    persist,
    storeKey,
  };
}
