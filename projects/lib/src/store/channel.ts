import { z } from 'zod';
import { createSliceConfig } from './type';

declare global {
  interface WebChatStore {
    channel: ChannelStore;
  }
}

type ChannelStore = {
  setChannels: (channels: Channel[]) => void;
  channels: Channel[];
  showChannelIcon: boolean;
};

type Channel = z.infer<typeof channelSchema>;

const channelSchema = z.object({
  title: z.string(),
  type: z.enum(['url', 'phone', 'email', 'address']).default('url'),
  value: z.string(),
  icon: z.string().optional(),
});

export const channelSliceConfig = createSliceConfig(
  (set): ChannelStore => {
    return {
      channels: [],
      showChannelIcon: false,
      setChannels: (channels: Channel[]) =>
        set((state) => ({
          channel: {
            ...state.channel,
            channels,
            showChannelIcon: channels.length > 0,
          },
        })),
    };
  },
  z.object({
    channels: z.array(channelSchema).optional(),
  }),
  (store, config) => {
    if (!config.channels) return;
    store.channel.setChannels(config.channels);
  }
);
