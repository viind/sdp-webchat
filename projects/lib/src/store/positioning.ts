import { z } from 'zod';
import { createSliceConfig } from './type';

declare global {
  interface WebChatStore {
    positioning: PositioningStore;
  }
}

const positioningSchema = z.object({
  zIndex: z.number().optional(),
  windowPaddingBottom: z.number().optional(),
  windowPaddingRight: z.number().optional(),
  openButtonPaddingBottom: z.number().optional(),
  openButtonPaddingRight: z.number().optional(),
  windowCloseButtonPaddingBottom: z.number().optional(),
});

type PositioningStore = {
  setPositioning: (positioning: z.infer<typeof positioningSchema>) => void;
  zIndex: number;
  windowPaddingBottom?: string;
  windowPaddingRight?: string;
  openButtonPaddingBottom?: string;
  openButtonPaddingRight?: string;
  windowCloseButtonPaddingBottom?: string;
  setRootElement: (rootElement?: HTMLElement) => void;
  rootElement?: HTMLElement;
};

export const positioningSliceConfig = createSliceConfig(
  (set): PositioningStore => {
    return {
      zIndex: 1000,
      setPositioning: (positioning) =>
        set((state) => ({
          positioning: {
            ...state.positioning,
            zIndex: positioning.zIndex ?? state.positioning.zIndex,
            ...applyRemToAll(positioning),
          },
        })),
      setRootElement: (rootElement) =>
        set((state) => ({
          positioning: {
            ...state.positioning,
            rootElement: rootElement,
          },
        })),
    };
  },
  z.object({
    positioning: positioningSchema.optional(),
  }),
  (store, config) => {
    if (config.positioning) {
      store.positioning.setPositioning(config.positioning);
    }
  }
);

function applyRemToAll(positioning: z.infer<typeof positioningSchema>) {
  return Object.fromEntries(
    (
      Object.entries(positioning) as [
        keyof z.infer<typeof positioningSchema>,
        number | undefined,
      ][]
    )
      .filter(([key]) => key !== 'zIndex')
      .map(([key, value]) => [key, value ? applyRemUnit(value) : undefined])
  );
}

function applyRemUnit(value: number) {
  return `${value}rem`;
}
