import { z } from 'zod';
import { createSliceConfig } from './type';
import { isChromium, isRealChrome } from '../utils';

declare global {
  interface WebChatStore {
    ttsStt: TtsSstStore;
  }
}

type TtsSstStore = {
  showTextToSpeech: boolean;
  showSpeechToText: boolean;
  isReading: boolean;
  isListening: boolean;
  setTtsSst: (ttsSst: z.infer<typeof ttsSstSchema> | boolean) => void;
  readText: (text: string, onStopCallback?: () => void) => void;
  stopReading: () => void;
  startSpeechRecognition: (onFinishCallback: (text: string) => void) => void;
  stopSpeechRecognition: () => void;

  // internal
  _speechRecognition?: SpeechRecognition;
  _stopReadingCallback?: () => void;
};

// for now the default is off
const defaultState = false;

const ttsSstSchema = z.object({
  disableTextToSpeech: z.boolean().default(false),
  disableSpeechToText: z.boolean().default(false),
});

const UnifiedSpeechRecognition =
  window.SpeechRecognition ||
  window.webkitSpeechRecognition ||
  // @ts-ignore
  window.mozSpeechRecognition ||
  // @ts-ignore
  window.msSpeechRecognition;

const isSpeechRecognitionSupported = Boolean(UnifiedSpeechRecognition);
const isSpeechSynthesisSupported = Boolean(speechSynthesis);
const isNotChromiumOrGoogleChrome = isChromium() === false || isRealChrome();

export const ttsSstSliceConfig = createSliceConfig(
  (set, get): TtsSstStore => {
    const getLanguage = () =>
      get().language.chatLanguage === 'unknown'
        ? get().language.defaultLanguage
        : get().language.chatLanguage;

    return {
      showTextToSpeech: defaultState,
      showSpeechToText: defaultState,
      isReading: false,
      isListening: false,

      setTtsSst: (config) => {
        const disableTextToSpeech =
          typeof config === 'boolean'
            ? config === false
            : config.disableSpeechToText;
        const disableSpeechToText =
          typeof config === 'boolean'
            ? config === false
            : config.disableTextToSpeech;

        set((state) => ({
          ttsStt: {
            ...state.ttsStt,
            showTextToSpeech:
              isSpeechRecognitionSupported &&
              isNotChromiumOrGoogleChrome &&
              disableTextToSpeech === false,
            showSpeechToText:
              isSpeechSynthesisSupported && disableSpeechToText === false,
          },
        }));
      },

      readText: (text, callback) => {
        if (get().ttsStt.showTextToSpeech === false) return;

        const stopReading = get().ttsStt.stopReading;
        stopReading();

        const sanitizedMessage = voiceMarkdownSanitizing(text);
        const utterance = new SpeechSynthesisUtterance(sanitizedMessage);
        utterance.lang = getLanguage();
        utterance.onend = stopReading;
        speechSynthesis?.speak(utterance);
        set((state) => ({
          ttsStt: {
            ...state.ttsStt,
            isReading: true,
            _stopReadingCallback: callback,
          },
        }));
      },

      stopReading: () => {
        speechSynthesis?.cancel();
        get().ttsStt._stopReadingCallback?.();

        set((state) => ({
          ttsStt: { ...state.ttsStt, isReading: false },
        }));
      },

      startSpeechRecognition: (callback) => {
        if (get().ttsStt.showSpeechToText === false) return;

        const stopListening = get().ttsStt.stopSpeechRecognition;
        stopListening();

        const newRecognition = new UnifiedSpeechRecognition();
        newRecognition.lang = getLanguage();
        newRecognition.interimResults = true;
        newRecognition.onresult = (event: any) => {
          const text = event.results?.[0]?.[0]?.transcript || '';
          callback(text);
        };
        newRecognition.onend = stopListening;
        newRecognition.onerror = stopListening;

        newRecognition.start();
        set((state) => ({
          ttsStt: {
            ...state.ttsStt,
            isListening: true,
            _speechRecognition: newRecognition,
          },
        }));
      },

      stopSpeechRecognition: () => {
        get().ttsStt._speechRecognition?.stop();

        set((state) => ({
          ttsStt: {
            ...state.ttsStt,
            isListening: false,
            _speechRecognition: undefined,
          },
        }));
      },
    };
  },
  z.object({ ttsStt: ttsSstSchema.or(z.boolean()).optional() }),
  (store, config) => {
    if (!config.ttsStt) return;
    store.ttsStt.setTtsSst(config.ttsStt);
  }
);

/**
 * Regex patterns translated from Python's `re.compile(...)`.
 */
const unnumberedListPattern = /\s*\n\s*[-+*]\s+/g;
const numberedListPattern = /\n\s*\d+\.\s+/g;
const boldItalicPattern = /(\*\*|__|\*|_)/g;
const imagePattern = /!\[.*?\]\(.*?\)/g;
const slashPattern = /\/-|-\/|\/ -|- \//g;
const newlinePattern = /\\\n/g;
const hyperlinkPattern = /(\[[^\]]*\])(\([^\)]*\))/g;

/**
 * Example placeholder for an emoji regex, replacing
 * Python's `rasa.utils.io.get_emoji_regex()`.
 * You might replace this with a more sophisticated approach or library.
 */
function getEmojiRegex(): RegExp {
  // Minimal example: match some common ranges of emojis
  return /[\u{1F300}-\u{1FAFF}]/gu;
}

/**
 * Removes emojis, links, and markdown symbols from text.
 * Equivalent to Python's `voice_markdown_sanitizing`.
 */
function voiceMarkdownSanitizing(text: string): string {
  if (!text) {
    return text;
  }

  // Remove emojis
  const emojiRegex = getEmojiRegex();
  text = text.replace(emojiRegex, '');

  // Remove unnumbered list markers
  text = text.replace(unnumberedListPattern, '\n\n');

  // Remove numbered list markers
  text = text.replace(numberedListPattern, '\n\n');

  // Remove bold/italic markers
  text = text.replace(boldItalicPattern, '');

  // Remove markdown images
  text = text.replace(imagePattern, '');

  // Sanitize hyperlinks
  text = text.replace(hyperlinkPattern, (match, p1) =>
    _sanitizeHyperlinks(match, p1)
  );

  // Replace certain slash patterns with ", "
  text = text.replace(slashPattern, ', ');

  // Replace escaped newlines
  text = text.replace(newlinePattern, ', ');

  return text;
}

/**
 * Sanitizes hyperlinks for voice output.
 * Equivalent to Python's `_sanitizeHyperlinks`.
 *
 * Note: In Python, `text.group(1)` is the first capture, etc.
 * In JS `String.replace(...)` callbacks, we receive them as function parameters.
 */
function _sanitizeHyperlinks(
  _fullMatch: string,
  bracketGroup: string // e.g., "[Link text]"
): string {
  const description = bracketGroup.slice(1, -1); // remove [ ]
  return description;
}
