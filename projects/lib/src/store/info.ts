import { z } from 'zod';
import { createSliceConfig } from './type';

declare global {
  interface WebChatStore {
    info: InfoStore;
  }
}

const knownInfoSchema = z.object({
  key: z.enum(['phone', 'email', 'address', 'text']),
  value: z.string(),
});

const linkInfoSchema = z.object({
  key: z.literal('link'),
  title: z.string(),
  url: z.string(),
});

type InfoStore = {
  setInfos: (
    infos: Array<z.infer<typeof knownInfoSchema | typeof linkInfoSchema>>
  ) => void;
  infos: z.infer<typeof knownInfoSchema>[];
  links: z.infer<typeof linkInfoSchema>[];
};

export const infoSliceConfig = createSliceConfig(
  (set): InfoStore => {
    return {
      infos: [],
      links: [],
      setInfos: (infos) =>
        set((state) => ({
          info: {
            ...state.info,
            links: infos.filter((info) => info.key === 'link'),
            infos: infos.filter((info) => info.key !== 'link'),
          },
        })),
    };
  },
  z.object({
    infos: z.array(knownInfoSchema.or(linkInfoSchema)).optional(),
  }),
  (store, config) => {
    if (!config.infos) return;
    store.info.setInfos(config.infos);
  }
);
