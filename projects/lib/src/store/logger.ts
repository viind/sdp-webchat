import { z } from 'zod';
import { createSliceConfig } from './type';

declare global {
  interface WebChatStore {
    logger: LoggerStore;
  }
}

type LoggerStore = {
  log: (level: LogLevel, message: string, ...optionalParams: unknown[]) => void;
  setLogLevel: (level: LogLevel) => void;
  error: (message: string, ...optionalParams: unknown[]) => void;
  warn: (message: string, ...optionalParams: unknown[]) => void;
  info: (message: string, ...optionalParams: unknown[]) => void;
  debug: (message: string, ...optionalParams: unknown[]) => void;
  trace: (message: string, ...optionalParams: unknown[]) => void;
};
type LogLevel = z.infer<typeof logLevelSchema>;
const logLevelSchema = z.enum(['error', 'warn', 'info', 'debug', 'trace']);

export const loggerSliceConfig = createSliceConfig(
  (): LoggerStore => {
    const logLevelOrder: LogLevel[] = [
      'error',
      'warn',
      'info',
      'debug',
      'trace',
    ];
    let logLevel: LogLevel = 'info';

    const log = (
      level: LogLevel,
      origMessage: string,
      ...optionalParams: unknown[]
    ) => {
      if (logLevelOrder.indexOf(level) > logLevelOrder.indexOf(logLevel)) {
        return;
      }
      const message = `Viind[${level}]: ${origMessage}`;
      console[level](message, ...optionalParams);
    };

    const setLogLevel = (level: LogLevel) => {
      logLevel = level;
    };

    return {
      log,
      setLogLevel,
      error: (message, ...params) => log('error', message, ...params),
      warn: (message, ...params) => log('warn', message, ...params),
      info: (message, ...params) => log('info', message, ...params),
      debug: (message, ...params) => log('debug', message, ...params),
      trace: (message, ...params) => log('trace', message, ...params),
    };
  },
  z.object({
    logLevel: logLevelSchema.optional(),
  }),
  (store, config) => {
    if (config.logLevel) {
      store.logger.setLogLevel(config.logLevel);
    }
  }
);
