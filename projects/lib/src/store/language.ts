import { z } from 'zod';
import { createSliceConfig } from './type';
import { NativeModules, Platform } from 'react-native';
import { defaultLanguage, i18n, uiLanguages } from '~/lang/i18n';
import { t } from 'i18next';

declare global {
  interface WebChatStore {
    language: LanguageStore;
  }
}

const languageSearchThreshold = 6;

export type KnownLanguages = keyof typeof languageCodeToChatbotLanguage;
type LanguageSubset = Record<KnownLanguages, string | string[]>;

const languageCodeToChatbotLanguage = {
  /**
   * this key will be used for all languages that are selected where no mapping between
   * the language name and the flag is found.
   */

  unknown: [] as string[],
  af: 'Afrikaans',
  am: 'አማርኛ',
  ar: 'عربي',
  as: 'অসমীয়া',
  az: 'Azərbaycan dili',
  ba: 'Башҡорт теле',
  bg: 'Български',
  bho: 'भोजपुरी',
  bn: 'বাংলা',
  bo: 'བོད་ཡིག',
  brx: 'बड़ो',
  bs: 'Bosanski',
  ca: 'Català',
  cs: 'Česky',
  cy: 'Cymraeg',
  da: 'Dansk',
  de: 'Deutsch',
  doi: 'डोगरी',
  dsb: 'dolnoserbšćina',
  dv: 'ދިވެހި',
  el: 'Ελληνική',
  en: 'English',
  es: 'Español',
  et: 'Eesti',
  eu: 'Euskara',
  fa: 'فارسی',
  fi: 'Suomalainen',
  fil: 'Filipino',
  fj: 'Na Vosa Vakaviti',
  fo: 'Føroyskt',
  fr: 'Français',
  ga: 'Gaeilge',
  gl: 'Galego',
  gom: 'कोंकणी',
  gu: 'ગુજરાતી',
  ha: 'Hausa',
  he: 'עברית',
  hi: 'हिन्दी',
  hr: 'Hrvatski',
  hsb: 'hornjoserbsce',
  ht: 'Kreyòl ayisyen',
  hu: 'Magyar',
  hy: 'Հայերեն',
  id: 'Bahasa Indonesia',
  ig: 'Igbo',
  ikt: 'Inuinnaqtun',
  is: 'Íslenska',
  it: 'Italiano',
  iu: 'ᐃᓄᒃᑎᑐᑦ',
  ja: '日本語',
  ka: 'ქართული',
  kk: 'Қазақ тілі',
  km: 'ភាសាខ្មែរ',
  kmr: 'سۆرانی',
  kn: 'ಕನ್ನಡ',
  ko: '한국어',
  ks: 'कश्मीरी',
  ku: 'Kurmancî',
  ky: 'кыргыз тили',
  ln: 'Lingála',
  lo: 'ລາວ',
  lt: 'Lietuvių kalba',
  lug: 'Luganda',
  lv: 'Latviešu',
  mai: 'मैथिली',
  mg: 'Malagasy',
  mi: 'te reo Māori',
  mk: 'македонски',
  ml: 'മലയാളം',
  mn: 'Монгол хэл',
  mr: 'मराठी',
  ms: 'Bahasa Melayu',
  mt: 'Malti',
  mww: 'Hmoob Dawb',
  my: 'မြန်မာစာ',
  nb: 'Norsk',
  ne: 'नेपाली',
  nl: 'Nederlands',
  nso: 'Sepedi',
  nya: 'Chinyanja',
  or: 'ଓଡ଼ିଆ',
  otq: 'Hñähñu',
  pa: 'ਪੰਜਾਬੀ',
  pl: 'Polski',
  prs: 'دری',
  ps: 'پښتو',
  pt: 'Português',
  ro: 'Românesc',
  ru: 'Русский',
  run: 'Ikirundi',
  rw: 'Ikinyarwanda',
  sd: 'سنڌي',
  si: 'සිංහල',
  sk: 'Slovenská',
  sl: 'Slovenski',
  sm: 'Gagana Samoa',
  sn: 'chiShona',
  so: 'Soomaalida',
  sq: 'Shqiptare',
  sr: 'Српски',
  st: 'Sesotho',
  sv: 'Svenska',
  sw: 'Kiswahili',
  ta: 'தமிழ்',
  te: 'తెలుగు',
  th: 'ไทย',
  ti: 'ቲግሪንያ',
  tk: 'Türkmen dili',
  tlh: 'tlhIngan Hol',
  tn: 'Setswana',
  to: 'lea faka-Tonga',
  tr: 'Türkçe',
  tt: 'татарча',
  ty: 'Reo Tahiti',
  ug: 'ئۇيغۇرچە',
  uk: 'Українською',
  ur: 'اردو',
  uz: 'Oʻzbek',
  vi: 'Tiếng Việt',
  xh: 'isiXhosa',
  yo: 'Èdè Yorùbá',
  yua: "Maaya t'aan",
  zh: '中文',
  zu: 'isiZulu',
};

const allDefinedLanguages = Object.fromEntries(
  Object.entries(languageCodeToChatbotLanguage).filter(
    ([, value]) => value !== ''
  )
) as LanguageSubset;

type LanguageStore = {
  t: typeof t;
  uiLanguage: KnownLanguages;
  chatLanguage: KnownLanguages;
  defaultLanguage: KnownLanguages;
  hasDefaultLanguage: boolean;
  userChangedLanguage: boolean;
  enabledLanguages: LanguageSubset;
  chatLanguageName: string;
  showLanguageMenu: boolean;
  showLanguageSearch: boolean;
  setEnabledLanguages: (languagesNames: string[]) => void;
  changeLanguage: (languageName: string, userChangedLanguage?: boolean) => void;
  setDefaultLanguage: (defaultLanguage: string) => void;
};

export const languageSliceConfig = createSliceConfig(
  (set, get): LanguageStore => {
    return {
      t,
      uiLanguage: defaultLanguage,
      chatLanguage: defaultLanguage,
      chatLanguageName: languageCodeToChatbotLanguage[defaultLanguage],
      defaultLanguage,
      hasDefaultLanguage: true,
      userChangedLanguage: false,
      showLanguageMenu: true,
      showLanguageSearch: true,
      enabledLanguages: allDefinedLanguages,
      changeLanguage: (newLanguageName, userChangedLanguage = true) => {
        const { enabledLanguages } = get().language;

        const newLanguage = Object.entries(enabledLanguages).find(
          ([, value]) => value === newLanguageName
        )?.[0] as KnownLanguages;

        set((state) => ({
          language: {
            ...state.language,
            chatLanguage: newLanguage ?? 'unknown',
            chatLanguageName: newLanguageName,
            userChangedLanguage,
          },
        }));

        const uiLanguage = (uiLanguages as string[]).includes(newLanguage)
          ? (newLanguage as (typeof uiLanguages)[number])
          : 'en';

        // check if the ui language should also be adjusted
        if (uiLanguage === get().language.uiLanguage) return;
        void i18n
          .changeLanguage(uiLanguage)
          .then(() => {
            get().logger.debug(
              `LanguageStore: change ui language to '${newLanguage}' finished`
            );
            return uiLanguage;
          })
          .catch(async (e) => {
            get().logger.warn(
              `LanguageStore: change ui language to '${newLanguage}' failed`,
              e
            );
            get().logger.info(
              `LanguageStore: change ui language to 'en' failed`
            );
            return i18n.changeLanguage('en').then(() => 'en' as const);
          })
          .then((changedLanguage) =>
            set((state) => ({
              language: {
                ...state.language,
                uiLanguage: changedLanguage,
                // @ts-ignore: this should be fine
                t: ((...args) => t(...args)) as typeof t,
              },
            }))
          );
      },
      setEnabledLanguages: (languageNames: string[]) => {
        const allKnownLanguages = new Set(
          Object.values(languageCodeToChatbotLanguage)
        );
        const unknownSelectedLanguages = languageNames.filter(
          (language) => allKnownLanguages.has(language) === false
        );

        const selectedLanguages = Object.fromEntries(
          Object.entries(languageCodeToChatbotLanguage).filter(
            ([, value]) =>
              typeof value === 'string' && languageNames.includes(value)
          )
        ) as LanguageSubset;

        if (unknownSelectedLanguages.length > 0) {
          get().logger.warn(
            `LanguageStore: Selected languages that may or may not be supported: '${unknownSelectedLanguages.join(', ')}'.
These languages will show a blank flag in the language settings, but they can still be selected.
The chat bot backend will decide if it can handle the language or not.

To remove this warning, please contact the Viind team to align the chosen languages with the supported languages.`
          );
          selectedLanguages.unknown = unknownSelectedLanguages;
        }

        set((state) => {
          return {
            language: {
              ...state.language,
              enabledLanguages: selectedLanguages,
              showLanguageMenu: Object.keys(selectedLanguages).length > 1,
              showLanguageSearch:
                Object.keys(selectedLanguages).length > languageSearchThreshold,
            },
          };
        });

        if (selectedLanguages[get().language.chatLanguage] === undefined) {
          // revert to default language if the selected language is not available in the selected languages
          get().language.changeLanguage(
            languageCodeToChatbotLanguage[defaultLanguage]
          );
        }
      },
      setDefaultLanguage: (defaultLanguage: string) => {
        const previousDefaultLanguage = get().language.defaultLanguage;
        if (previousDefaultLanguage === defaultLanguage) {
          return;
        }
        const allKnownLanguageCodes = new Set(
          Object.keys(languageCodeToChatbotLanguage)
        );
        const languageIsKnown = allKnownLanguageCodes.has(defaultLanguage);
        const defaultLanguageKey: KnownLanguages = languageIsKnown
          ? (defaultLanguage as KnownLanguages)
          : 'unknown';

        set((state) => ({
          language: {
            ...state.language,
            defaultLanguage: defaultLanguageKey,
            hasDefaultLanguage: false,
          },
        }));

        if (get().language.userChangedLanguage) {
          // the user already changed the default language once, so we should not change it again
          return;
        }

        if (defaultLanguageKey === 'unknown') {
          get().logger.warn(
            `LanguageStore: The default language '${defaultLanguage}' is not supported.
The default language will be set to 'en'.`
          );
          get().language.changeLanguage(
            languageCodeToChatbotLanguage['en'],
            false
          );
          return;
        }

        get().language.changeLanguage(
          languageCodeToChatbotLanguage[defaultLanguageKey],
          false
        );
      },
    };
  },
  z.object({
    applySystemLanguageOnStartup: z.boolean().optional(),
    defaultLanguage: z.string().optional(),
    languages: z.array(z.string()).optional(),
  }),
  (store, config) => {
    if (!config.languages) {
      store.logger.info('LanguageProvider: enable all languages');
    } else {
      store.language.setEnabledLanguages(config.languages);
    }

    if (config.defaultLanguage) {
      store.language.setDefaultLanguage(config.defaultLanguage);
    }

    if (config.applySystemLanguageOnStartup) {
      const extractLanguage = (code: string) => code.split('-')[0] ?? code;

      const getLocalLanguage = {
        ios: () =>
          extractLanguage(NativeModules.SettingsManager.settings.AppleLocale),
        android: () =>
          extractLanguage(NativeModules.I18nManager.localeIdentifier),
        web: () => extractLanguage(navigator.language),
        // todo: find the proper way to get the language of macos and windows
        macos: () => 'de',
        windows: () => 'de',
      } satisfies Record<Platform['OS'], () => string>;

      const localLanguage = getLocalLanguage[Platform.OS]();
      const languageName = (
        languageCodeToChatbotLanguage as Record<
          string,
          string | string[] | undefined
        >
      )[localLanguage];

      if (typeof languageName === 'undefined') {
        store.logger.info(
          `LanguageStore: no language mapping found for the local language '${localLanguage}'.
The language will be set to the default language '${store.language.defaultLanguage}'.`
        );
        store.language.changeLanguage(store.language.defaultLanguage);
        return;
      }

      if (typeof languageName === 'object') {
        store.logger.info(
          `LanguageStore: the local language '${localLanguage}' is not supported.
The language will be set to the default language '${store.language.defaultLanguage}'.`
        );
        store.language.changeLanguage(store.language.defaultLanguage);
        return;
      }

      store.logger.info(
        `LanguageStore: apply system language '${localLanguage}' to the chat bot.`
      );
      store.language.changeLanguage(localLanguage as KnownLanguages);
    }
  },
  z.object({
    chatLanguage: zObjectKeys(languageCodeToChatbotLanguage),
    chatLanguageName: z.string(),
    uiLanguage: zObjectKeys(languageCodeToChatbotLanguage),
    userChangedLanguage: z.boolean(),
  }),
  (partialStore) => {
    return {
      uiLanguage: partialStore.uiLanguage,
      chatLanguage: partialStore.chatLanguage,
      chatLanguageName: partialStore.chatLanguageName,
      userChangedLanguage: partialStore.userChangedLanguage,
    };
  },
  'language'
);

// source: https://github.com/colinhacks/zod/discussions/839#discussioncomment-10651593
function zObjectKeys<T extends Record<string, any>>(obj: T) {
  const keys = Object.keys(obj) as Extract<keyof T, string>[];
  return z.enum(
    keys as [Extract<keyof T, string>, ...Extract<keyof T, string>[]]
  );
}
