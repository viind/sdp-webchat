import * as React from 'react';
import { useFeature } from '../provider/feature-provider';
import { useChatOpen } from '../provider/chat-open-provider';
import { Platform } from 'react-native';

export function hideParentScrolling() {
  const { view } = useFeature();
  const { isOpen } = useChatOpen();

  React.useEffect(() => {
    if (Platform.OS !== 'web') {
      return;
    }
    const overflow =
      view === 'desktop-windowed' ? 'auto' : isOpen ? 'hidden' : 'auto';
    document.body.style.overflow = overflow;
  }, [view, isOpen]);
}
