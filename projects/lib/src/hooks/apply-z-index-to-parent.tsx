import * as React from 'react';
import { Platform } from 'react-native';
import { useFeature } from '../provider/feature-provider';
import { useStore } from '~/src/provider/store-provider';

export function applyZIndexToParent(rootTag?: HTMLElement) {
  const zIndex = useStore((state) => state.positioning.zIndex);
  const { view } = useFeature();
  const [originalPositionValue] = React.useState<string>(
    rootTag?.style.position ?? ''
  );
  const [originalZIndexValue] = React.useState<string>(
    rootTag?.style.zIndex ?? ''
  );

  React.useEffect(() => {
    if (Platform.OS !== 'web' || !rootTag) {
      return;
    }

    const isEmbedded = view === 'embedded';

    rootTag.style.position = isEmbedded ? originalPositionValue : 'relative';
    rootTag.style.zIndex = isEmbedded ? originalZIndexValue : zIndex.toString();
  }, [rootTag, zIndex, view]);
}
