import * as React from 'react';
import { Platform, type View } from 'react-native';
import { useStore } from '../provider/store-provider';
export function applyScrollbarStyling(ref: React.RefObject<View>) {
  const scrollbarStyling = useStore((state) => state.theme.scrollbarStyling);

  React.useEffect(() => {
    if (Platform.OS !== 'web') return;
    if (ref.current === null) return;
    if (scrollbarStyling === undefined) return;

    if (ref.current instanceof HTMLElement) {
      ref.current.dataset.scrollbarStyling = scrollbarStyling;
    }
  }, [scrollbarStyling, ref.current]);
}
