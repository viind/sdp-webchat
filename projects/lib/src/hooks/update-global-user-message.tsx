import * as React from 'react';

/**
 * This is not linked with typescript to the web project.
 * So, if a change occurs here: projects/web/src/index.ts
 * The this file has to be updated.
 *
 * Reason for having it separate:
 * The global types are only relevant for the web project.
 * So having the global variable present in the lib would show them for all projects that use this lib.
 * Even though, it only exists in the web project.
 */
type GlobalTypeWithoutActuallyAssigningGlobalType = {
  setupViindWebChat: SetupViindWebChat;
};

interface SetupViindWebChat {
  __internal: { addUserMessage?: (message: string) => void };
}

export function updateGlobalUserMessage(func: (message: string) => void) {
  const global =
    globalThis as unknown as GlobalTypeWithoutActuallyAssigningGlobalType;
  React.useEffect(() => {
    if (global.setupViindWebChat === undefined) {
      // would happen in non-browser environments
      return;
    }
    global.setupViindWebChat.__internal.addUserMessage = func;
  }, [func]);
}
