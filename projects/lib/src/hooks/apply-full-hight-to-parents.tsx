import * as React from 'react';
import { Platform, type View } from 'react-native';
import { cn } from '~/lib/utils';
import { useStore } from '../provider/store-provider';

type Config = {
  shouldBeApplied: boolean;
  ref: React.RefObject<View>;
};

export function applyFullHeightToParents(config: Config) {
  const [wasApplied, setWasApplied] = React.useState(false);
  const rootElement = useStore((state) => state.positioning.rootElement);

  React.useEffect(() => {
    if (Platform.OS !== 'web') {
      return;
    }
    if (config.shouldBeApplied === false || wasApplied || !rootElement) {
      return;
    }

    const classToApply = cn(`vi-h-full`);
    const current = config.ref.current;
    if (!(current instanceof HTMLElement)) {
      return;
    }

    let currentElement: HTMLElement | null = current;
    while (currentElement && currentElement !== rootElement?.parentNode) {
      currentElement.classList.add(classToApply);
      const next: ParentNode | null = currentElement.parentNode;
      if (!(next instanceof HTMLElement)) {
        return;
      }
      currentElement = next;
    }
    setWasApplied(true);
  }, [config, config.ref.current, rootElement]);
}
