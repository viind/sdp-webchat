const { getLanguageCountries } = require('@ladjs/country-language');
import * as Flags from 'country-flag-icons/react/3x2';

// Common language codes that we want to map
// TODO: update whenever the languages change
const languageCodes = Object.keys({
  af: '',
  am: '',
  ar: '',
  as: '',
  az: '',
  ba: '',
  bg: 'Български',
  bho: '',
  bn: '',
  bo: '',
  brx: '',
  bs: '',
  ca: '',
  cs: '',
  cy: 'Česky',
  da: 'Dansk',
  de: 'Deutsch',
  doi: '',
  dsb: '',
  dv: '',
  el: 'Ελληνική',
  en: 'English',
  es: 'Español',
  et: 'Eesti',
  eu: '',
  fa: '',
  fi: 'Suomalainen',
  fil: '',
  fj: '',
  fo: '',
  fr: 'Français',
  ga: '',
  gl: '',
  gom: '',
  gu: '',
  ha: '',
  he: '',
  hi: '',
  hr: 'Hrvatski',
  hsb: '',
  ht: '',
  hu: 'Magyar',
  hy: '',
  id: 'Bahasa Indonesia',
  ig: '',
  ikt: '',
  is: '',
  it: 'Italiano',
  iu: '',
  ja: '日本語',
  ka: '',
  kk: '',
  km: '',
  kmr: '',
  kn: '',
  ko: '한국어',
  ks: '',
  ku: '',
  ky: '',
  ln: '',
  lo: '',
  lt: 'Lietuvių kalba',
  lug: '',
  lv: 'Latviešu',
  mai: '',
  mg: '',
  mi: '',
  mk: '',
  ml: '',
  mn: '',
  mr: '',
  ms: '',
  mt: '',
  mww: '',
  my: '',
  nb: 'Norsk',
  ne: '',
  nl: 'Nederlands',
  nso: '',
  nya: '',
  or: '',
  otq: '',
  pa: '',
  pl: 'Polski',
  prs: '',
  ps: '',
  pt: 'Português',
  ro: 'Românesc',
  ru: 'Русский',
  run: '',
  rw: '',
  sd: '',
  si: '',
  sk: 'Slovenská',
  sl: 'Slovenski',
  sm: '',
  sn: '',
  so: '',
  sq: '',
  sr: 'Српски',
  st: '',
  sv: 'Svenska',
  sw: '',
  ta: '',
  te: '',
  th: '',
  ti: '',
  tk: '',
  tlh: '',
  tn: '',
  to: '',
  tr: 'Türkçe',
  tt: '',
  ty: '',
  ug: '',
  uk: '',
  ur: '',
  uz: '',
  vi: 'Tiếng Việt',
  xh: '',
  yo: '',
  yua: 'Українською',
  zh: '中文',
  zu: '',
});

const hardOverrideTable = {
  // there exists a flag called "sv", but the language code for sweden based on ISO 639-1 is "se"
  // therefore this hard override is needed
  sv: 'se',
};

const backupMap = {
  // https://en.wikipedia.org/wiki/Assamese_language
  as: 'in',
  // https://en.wikipedia.org/wiki/Bashkir_language
  ba: 'ru',
  // https://en.wikipedia.org/wiki/Bhojpuri_language
  bho: 'fj',
  // https://en.wikipedia.org/wiki/Tibetic_languages
  bo: 'cn',
  // https://en.wikipedia.org/wiki/Boro_language_(India)
  brx: 'in',
  // https://en.wikipedia.org/wiki/Dogri_language
  doi: 'in',
  // https://en.wikipedia.org/wiki/Lower_Sorbian_language
  dsb: 'de',
  // https://en.wikipedia.org/wiki/Basque_language
  eu: 'es',
  // https://en.wikipedia.org/wiki/Faroese_language
  fo: 'dk',
  // http://en.wikipedia.org/wiki/Galician_language
  gl: 'es',
  // https://en.wikipedia.org/wiki/Konkani_language
  gom: 'in',
  // https://en.wikipedia.org/wiki/Upper_Sorbian_language
  hbs: 'de',
  // https://en.wikipedia.org/wiki/Inuinnaqtun
  // no flag available for Nunavut
  ikt: undefined,
  // https://sv.wikipedia.org/wiki/Inuktitut
  // no flag available for Nunavut
  iu: undefined,
  // https://en.wikipedia.org/wiki/Kurdish_language
  kmr: 'iq',
  // https://en.wikipedia.org/wiki/Kannada
  kn: 'in',
  // https://en.wikipedia.org/wiki/Kashmiri_language
  ks: 'in',
  // https://en.wikipedia.org/wiki/Luganda
  lug: 'ug',
  // https://en.wikipedia.org/wiki/Maithili_language
  mai: 'in',
  // https://en.wikipedia.org/wiki/Malayalam
  ml: 'in',
  // https://en.wikipedia.org/wiki/Marathi_language
  mr: 'in',
  // https://en.wikipedia.org/wiki/Hmong_language
  mww: 'cn',
  // https://en.wikipedia.org/wiki/Odia_language
  or: 'in',
  // https://en.wikipedia.org/wiki/Otomi_language
  otq: 'mx',
  // https://en.wikipedia.org/wiki/Sindhi_language
  sd: 'pk',
  // https://en.wikipedia.org/wiki/Samoan_language
  // library does not return the associated country code
  sm: 'sm',
  // https://en.wikipedia.org/wiki/Klingon_language
  // There is no flag for Klingon
  tlh: undefined,
  // https://en.wikipedia.org/wiki/Tongan_language
  // library does not return the associated country code
  to: 'to',
  // https://en.wikipedia.org/wiki/Tatar_language
  tt: 'ru',
  // https://en.wikipedia.org/wiki/Tahitian_language
  ty: 'pf',
  // https://en.wikipedia.org/wiki/Uyghur_language
  ug: 'cn',
  //https://en.wikipedia.org/wiki/Yucatec_Maya_language
  yua: 'mx',
};

// Get available flag codes (converting to lowercase for comparison)
const availableFlagCodes = Object.keys(Flags).map((code) => code.toLowerCase());

// Generate the mapping
const mapping = {};
languageCodes.forEach((code) => {
  // Check if there is a hard override for this language code
  if (hardOverrideTable[code] !== undefined) {
    mapping[code] = hardOverrideTable[code];
    return;
  }

  // Check if language code directly matches available flags
  if (availableFlagCodes.includes(code)) {
    mapping[code] = code;
    return;
  }

  // Then try getting country code from language data
  try {
    const countries = getLanguageCountries(code);
    const countryCode = countries[0]?.code_2?.toLowerCase();
    if (countryCode && availableFlagCodes.includes(countryCode)) {
      mapping[code] = countryCode;
      return;
    }
  } catch (error) {
    // Ignore errors from getLanguageCountries
  }

  // Finally fall back to backup map
  if (backupMap[code] && availableFlagCodes.includes(backupMap[code])) {
    mapping[code] = backupMap[code];
  }
});

// Special case for English - use GB instead of US
mapping.en = 'gb';

// Generate the output file content
const output = `// This file is generated - do not edit manually
export const languageToFlagCodeMap: Record<string, string|undefined> = ${JSON.stringify(mapping, null, 2)};
`;

const fs = require('fs');
const path = require('path');

// Ensure the directory exists
const dir = path.dirname('src/generated/language-to-flag-code.ts');
if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir, { recursive: true });
}

// Write the file
fs.writeFileSync('src/generated/language-to-flag-code.ts', output, 'utf8');

console.log('Generated language-to-flag-code mapping');
