import * as React from 'react';
import { useStore } from '~/src/provider/store-provider';
export const TextLanguageContext = React.createContext<string | undefined>(
  undefined
);

export type LanguageOption = 'ui' | 'chat' | 'context' | { custom: string };
export type LanguageProp = { language: LanguageOption };
export function useLanguage(language?: LanguageOption) {
  const uiLanguage = useStore((state) => state.language.uiLanguage);
  const chatLanguage = useStore((state) => state.language.chatLanguage);
  const textLanguage = React.useContext(TextLanguageContext);

  if (language === undefined) {
    return undefined;
  }

  if (typeof language === 'object') {
    return language.custom;
  }

  if (language === 'context') {
    return textLanguage;
  }

  if (language === 'ui') {
    return uiLanguage;
  }

  if (language === 'chat') {
    return chatLanguage;
  }

  return undefined;
}
