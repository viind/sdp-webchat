import { Platform, View } from 'react-native';

export function Label({
  children,
  htmlFor,
  className,
}: {
  children: React.ReactNode;
  className?: string;
  htmlFor: string;
}) {
  if (Platform.OS === 'web') {
    return (
      <label htmlFor={htmlFor} className={className}>
        {children}
      </label>
    );
  }

  return <View className={className}>{children}</View>;
}
