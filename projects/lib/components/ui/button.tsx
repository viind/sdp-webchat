import { cva, type VariantProps } from 'class-variance-authority';
import * as React from 'react';
import { Pressable } from 'react-native';
import { TextClassContext } from '~/components/ui/text';
import { cn } from '~/lib/utils';
import { useStore } from '~/src/provider/store-provider';
import {
  useLanguage,
  type LanguageOption,
  type LanguageProp,
} from '../primitives/hooks/useLanguage';

const buttonVariants = cva(
  'vi-group vi-flex vi-items-center vi-justify-center vi-rounded-md  web:vi-transition-colors focus-within:vi-outline focus-within:vi-outline-2',
  {
    variants: {
      variant: {
        'default': 'vi-bg-primary active:vi-opacity-90 web:hover:vi-opacity-90',
        'outline':
          'vi-group/button vi-border-2 vi-border-border vi-bg-background active:vi-opacity-50 web:hover:vi-opacity-50',
        'outline-with-primary':
          'vi-group/button active:vi-opacity-50 web:hover:vi-opacity-50 vi-border-2 vi-border-primary vi-bg-background',
        'quiet': 'vi-group/button',
        'quiet-with-primary': 'vi-group/button',
        'quiet-on-primary': 'vi-group/button',
      },
      size: {
        default:
          'native:vi-h-12 native:vi-px-5 native:vi-py-3 vi-h-10 vi-px-4 vi-py-2',
        sm: 'vi-h-9 vi-rounded-md vi-px-3',
        lg: 'native:vi-h-14 vi-h-11 vi-rounded-md vi-px-8',
        icon: 'vi-size-12',
      },
      focus: {
        onBackground: 'vi-outline-background-focus',
        onBotMessage: 'vi-outline-bot-message-focus',
        onPrimary: 'vi-outline-primary-focus',
        primaryWithRing:
          'vi-outline-primary-focus focus-within:vi-ring-2 vi-ring-offset-2 vi-ring-background-focus',
      },
    },
    defaultVariants: {
      variant: 'default',
      size: 'default',
      focus: 'onBackground',
    },
  }
);

const buttonTextVariants = cva(
  'native:text-base vi-text-sm vi-font-medium vi-text-foreground web:vi-whitespace-nowrap web:vi-transition-colors',
  {
    variants: {
      variant: {
        'default': 'vi-text-primary-foreground',
        'outline': 'vi-text-primary',
        'outline-with-primary': 'vi-text-primary',
        'link': 'vi-text-primary group-active:vi-underline',
        'quiet':
          'vi-text-foreground group-hover/button:vi-text-opacity-50 group-active/button:vi-text-opacity-50',
        'quiet-with-primary':
          'vi-text-foreground-primary group-hover/button:vi-text-opacity-50 group-active/button:vi-text-opacity-50',
        'quiet-on-primary':
          'vi-text-primary-foreground group-hover/button:vi-text-opacity-50 group-active/button:vi-text-opacity-50',
      },
      size: {
        default: '',
        sm: '',
        lg: 'native:vi-text-lg',
        icon: '',
      },
    },
    defaultVariants: {
      variant: 'default',
      size: 'default',
    },
  }
);

type ButtonProps = Omit<
  React.ComponentPropsWithoutRef<typeof Pressable> &
    VariantProps<typeof buttonVariants> & {
      allowBreakWords?: boolean;
      realDisabled?: boolean;
    },
  'aria-label'
> &
  (
    | { 'aria-label': undefined; 'language': undefined }
    | { 'aria-label': string; 'language': LanguageOption }
  );

const Button = React.forwardRef<
  React.ElementRef<typeof Pressable>,
  ButtonProps & Partial<LanguageProp>
>(
  (
    {
      'aria-label': ariaLabel,
      accessibilityHint,
      className,
      variant,
      size,
      focus,
      language,
      realDisabled,
      disabled,
      ...props
    },
    ref
  ) => {
    const { error } = useStore((state) => state.logger);
    const lang = useLanguage(language);

    if (size === 'icon') {
      if ((!ariaLabel || ariaLabel === '') && disabled === false) {
        error('Button: aria-label is required for icon buttons');
      }
    }

    if (ariaLabel && language === undefined) {
      error('Button: language is required for button with aria-label');
    }

    return (
      <TextClassContext.Provider
        value={cn(
          disabled && 'web:vi-pointer-events-none',
          buttonTextVariants({ variant, size }),
          props.allowBreakWords && 'web:vi-whitespace-normal'
        )}
      >
        <Pressable
          className={cn(
            disabled && 'vi-opacity-60 web:vi-pointer-events-none',
            buttonVariants({ variant, size, focus, className }),
            props.allowBreakWords && 'vi-h-auto'
          )}
          ref={ref}
          role="button"
          aria-label={ariaLabel}
          lang={lang}
          accessibilityHint={accessibilityHint}
          disabled={realDisabled}
          {...props}
        />
      </TextClassContext.Provider>
    );
  }
);
Button.displayName = 'Button';

export { Button, buttonTextVariants, buttonVariants };
export type { ButtonProps };
