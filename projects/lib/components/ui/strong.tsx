import * as React from 'react';
import { Platform, type StyleProp, type TextStyle } from 'react-native';
import { Text, TextClassContext } from './text';
import {
  useLanguage,
  type LanguageOption,
} from '../primitives/hooks/useLanguage';
import { cn } from '~/lib/utils';

const Strong: React.FC<{
  children: React.ReactNode;
  className?: string;
  style?: StyleProp<TextStyle>;
  language: LanguageOption;
}> = ({ children, className, style, language }) => {
  if (Platform.OS === 'web') {
    return (
      <WebStrong language={language} className={className}>
        {children}
      </WebStrong>
    );
  }
  return (
    <Text style={style} language={language} className={className}>
      {children}
    </Text>
  );
};
Strong.displayName = 'Strong';
export { Strong };

const WebStrong: React.FC<{
  children: React.ReactNode;
  language: LanguageOption;
  className?: string;
}> = ({ children, language, className }) => {
  const lang = useLanguage(language);
  const textClass = React.useContext(TextClassContext);
  return (
    <strong lang={lang} className={cn(textClass, 'vi-font-bold', className)}>
      {children}
    </strong>
  );
};
WebStrong.displayName = 'WebStrong';
