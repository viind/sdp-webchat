import type * as React from 'react';
import {
  Pressable as NativePressable,
  type PressableProps as NativePressableProps,
} from 'react-native';
import {
  useLanguage,
  type LanguageOption,
} from '../primitives/hooks/useLanguage';

export type PressableProps = Omit<NativePressableProps, 'aria-label'> &
  (
    | { 'aria-label': undefined; 'language': undefined }
    | { 'aria-label': string; 'language': LanguageOption }
  );

const Pressable: React.FC<PressableProps> = ({ language, ...props }) => {
  const lang = useLanguage(language);
  return <NativePressable {...props} lang={lang} />;
};
Pressable.displayName = 'Pressable';
export { Pressable };
