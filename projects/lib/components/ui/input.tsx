import * as React from 'react';
import { TextInput, View } from 'react-native';
import { cn } from '~/lib/utils';
import { Text, TextClassContext } from './text';
import { Label } from './label';

const Input = React.forwardRef<
  React.ElementRef<typeof TextInput>,
  Omit<React.ComponentPropsWithoutRef<typeof TextInput>, 'id'> & { id: string }
>(
  (
    {
      className,
      placeholderClassName,
      placeholder,
      onChangeText,
      id,
      ...props
    },
    ref
  ) => {
    const textClass = React.useContext(TextClassContext);
    const [hasValue, setHasValue] = React.useState(false);

    return (
      <View className="vi-relative vi-grow">
        <TextInput
          ref={ref}
          id={id}
          className={cn(
            'native:vi-h-10 native:vi-text-lg native:vi-leading-tight vi-h-12 vi-rounded-[1.75rem] vi-border vi-border-border-message vi-bg-background vi-p-5 vi-text-foreground placeholder:vi-opacity-65',
            props.editable === false &&
              'vi-opacity-50 web:vi-cursor-not-allowed',
            textClass,
            className
          )}
          placeholder={placeholder}
          onChangeText={(text) => {
            setHasValue(text.length > 0);
            onChangeText?.(text);
          }}
          placeholderClassName={cn('vi-text-foreground', placeholderClassName)}
          {...props}
        />
        {placeholder && hasValue && (
          <View
            className="vi-absolute -vi-top-2.5 vi-pl-4"
            pointerEvents="none"
          >
            <Label htmlFor={id} className="vi-bg-background vi-px-1">
              <Text language="ui" className="vi-text-sm vi-text-primary">
                {placeholder}
              </Text>
            </Label>
          </View>
        )}
      </View>
    );
  }
);

Input.displayName = 'Input';

export { Input };
