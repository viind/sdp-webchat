import * as AccordionPrimitive from '~/components/primitives/accordion';
import * as React from 'react';
import { Platform, Pressable, View } from 'react-native';
import Animated, {
  Extrapolation,
  FadeIn,
  FadeOutUp,
  LayoutAnimationConfig,
  LinearTransition,
  interpolate,
  useAnimatedStyle,
  useDerivedValue,
  withTiming,
} from 'react-native-reanimated';
import { cn } from '~/lib/utils';
import { TextClassContext } from '~/components/ui/text';
import { ChevronDown } from '~/lib/icons/ChevronDown';

const Accordion = React.forwardRef<
  AccordionPrimitive.RootRef,
  AccordionPrimitive.RootProps
>(({ children, ...props }, ref) => {
  return (
    <LayoutAnimationConfig skipEntering>
      <AccordionPrimitive.Root
        ref={ref}
        {...props}
        asChild={Platform.OS !== 'web'}
      >
        <Animated.View layout={LinearTransition.duration(200)}>
          {children}
        </Animated.View>
      </AccordionPrimitive.Root>
    </LayoutAnimationConfig>
  );
});

Accordion.displayName = AccordionPrimitive.Root.displayName;

const AccordionItem = React.forwardRef<
  AccordionPrimitive.ItemRef,
  AccordionPrimitive.ItemProps
>(({ className, value, ...props }, ref) => {
  return (
    <Animated.View
      className={'vi-overflow-hidden'}
      layout={LinearTransition.duration(200)}
    >
      <AccordionPrimitive.Item
        ref={ref}
        className={cn('vi-border-b vi-border-border', className)}
        value={value}
        {...props}
      />
    </Animated.View>
  );
});
AccordionItem.displayName = AccordionPrimitive.Item.displayName;

const Trigger = Platform.OS === 'web' ? View : Pressable;

const AccordionTrigger = React.forwardRef<
  AccordionPrimitive.TriggerRef,
  AccordionPrimitive.TriggerProps
>(({ className, children, ...props }, ref) => {
  const { isExpanded } = AccordionPrimitive.useItemContext();

  const progress = useDerivedValue(() =>
    isExpanded
      ? withTiming(1, { duration: 250 })
      : withTiming(0, { duration: 200 })
  );
  const chevronStyle = useAnimatedStyle(() => ({
    transform: [{ rotate: `${progress.value * 180}deg` }],
    opacity: interpolate(progress.value, [0, 1], [1, 0.8], Extrapolation.CLAMP),
  }));

  return (
    <TextClassContext.Provider
      value={cn(
        'native:vi-text-lg vi-font-medium web:group-hover:vi-underline'
      )}
    >
      <AccordionPrimitive.Header className="vi-flex vi-rounded-md vi-outline-background-focus focus-within:vi-outline focus-within:vi-outline-2">
        <AccordionPrimitive.Trigger
          ref={ref}
          {...props}
          className="vi-outline-none"
          asChild
        >
          <Trigger
            className={cn(
              'vi-group vi-my-4 vi-flex vi-flex-row vi-items-center vi-justify-between web:vi-flex-1 web:vi-transition-all',
              className
            )}
          >
            <>{children}</>
            <Animated.View style={chevronStyle}>
              <ChevronDown
                size={18}
                className={'vi-shrink-0 vi-text-foreground'}
              />
            </Animated.View>
          </Trigger>
        </AccordionPrimitive.Trigger>
      </AccordionPrimitive.Header>
    </TextClassContext.Provider>
  );
});
AccordionTrigger.displayName = AccordionPrimitive.Trigger.displayName;

const AccordionContent = React.forwardRef<
  AccordionPrimitive.ContentRef,
  AccordionPrimitive.ContentProps
>(({ className, children, ...props }, ref) => {
  const { isExpanded } = AccordionPrimitive.useItemContext();
  return (
    <TextClassContext.Provider value={cn('native:vi-text-lg')}>
      <AccordionPrimitive.Content
        className={cn(
          'vi-overflow-hidden vi-text-sm web:vi-transition-all',
          isExpanded
            ? 'web:vi-animate-accordion-down'
            : 'web:vi-animate-accordion-up'
        )}
        ref={ref}
        {...props}
      >
        <InnerContent className={cn('vi-pb-4', className)}>
          {children}
        </InnerContent>
      </AccordionPrimitive.Content>
    </TextClassContext.Provider>
  );
});

function InnerContent({
  children,
  className,
}: {
  children: React.ReactNode;
  className?: string;
}) {
  if (Platform.OS === 'web') {
    return <View className={cn('vi-pb-4', className)}>{children}</View>;
  }
  return (
    <Animated.View
      entering={FadeIn}
      exiting={FadeOutUp.duration(200)}
      className={cn('vi-pb-4', className)}
    >
      {children}
    </Animated.View>
  );
}

AccordionContent.displayName = AccordionPrimitive.Content.displayName;

export { Accordion, AccordionContent, AccordionItem, AccordionTrigger };
