import { Text as SlotText } from '~/components/primitives/slot';
import { type SlottableTextProps, type TextRef } from '../primitives/types';
import * as React from 'react';
import { Text as RNText } from 'react-native';
import { cn } from '~/lib/utils';
import {
  useLanguage,
  type LanguageProp,
} from '../primitives/hooks/useLanguage';

const TextClassContext = React.createContext<string | undefined>(undefined);

export type TextProps = SlottableTextProps & LanguageProp;

const Text = React.forwardRef<TextRef, TextProps>(
  ({ className, asChild = false, language, ...props }, ref) => {
    const textClass = React.useContext(TextClassContext);
    const Component = asChild ? SlotText : RNText;
    const lang = useLanguage(language);

    return (
      <Component
        className={cn(
          'vi-font-roboto vi-text-base vi-text-foreground web:vi-select-text',
          textClass,
          className
        )}
        lang={lang}
        ref={ref}
        {...props}
      />
    );
  }
);
Text.displayName = 'Text';

export { Text, TextClassContext };
