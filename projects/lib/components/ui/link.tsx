import * as React from 'react';
import { Text, type TextProps } from './text';
import { cn } from '~/lib/utils';
import { Linking, Platform } from 'react-native';
import { cva, type VariantProps } from 'class-variance-authority';

interface LinkProps extends TextProps {
  href: string;
}

const linkVariants = cva(
  'focus-within:vi-outline focus-within:vi-outline-2 vi-rounded-md vi-outline-offset-2',
  {
    variants: {
      focus: {
        onBackground: 'vi-outline-background-focus',
        onBotMessage: 'vi-outline-bot-message-focus',
        onPrimary: 'vi-outline-primary-focus',
        primaryWithRing:
          'vi-outline-primary-focus focus-within:vi-ring-2 vi-ring-offset-2 vi-ring-background-focus',
      },
    },
    defaultVariants: {
      focus: 'onBackground',
    },
  }
);

const Link: React.FC<LinkProps & VariantProps<typeof linkVariants>> = ({
  className,
  href,
  focus,
  ...props
}) => {
  if (Platform.OS === 'web') {
    return (
      <a
        target="_blank"
        href={href}
        className={cn(className, 'vi-underline', linkVariants({ focus }))}
      >
        {props.children}
      </a>
    );
  }
  return (
    <Text
      {...props}
      role="link"
      onPress={(e) => {
        e.preventDefault();
        void Linking.openURL(href);
      }}
      className={cn(className, 'vi-underline')}
    />
  );
};
Link.displayName = 'Link';
export { Link };
