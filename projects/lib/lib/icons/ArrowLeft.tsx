import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const ArrowLeft = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 25 20.455" {...props}>
      <Path
        d="M24.477 75.356a1.652 1.652 0 000-2.412l-8.929-8.519a1.85 1.85 0 00-2.528 0 1.652 1.652 0 000 2.412l5.888 5.612H1.786a1.706 1.706 0 100 3.408H18.9l-5.876 5.612a1.652 1.652 0 000 2.412 1.85 1.85 0 002.528 0l8.929-8.519z"
        transform="rotate(180 12.5 42.19)"
      />
    </Svg>
  );
};
ArrowLeft.displayName = 'ArrowLeft';

export { ArrowLeft };
