import * as React from 'react';

import { cssInterop } from 'nativewind';
import NativeSvg from 'react-native-svg';
import { TextClassContext } from '~/components/ui';
import { cn } from '../utils';

cssInterop(NativeSvg, {
  className: {
    target: 'style',
    nativeStyleToProp: { width: true, height: true, color: true },
  },
});

export type SvgProps = React.ComponentPropsWithoutRef<typeof NativeSvg> & {
  size?: number;
};

const Svg = ({ size, width, height, className, ...props }: SvgProps) => {
  const textClass = React.useContext(TextClassContext);
  if (size) {
    return (
      <NativeSvg
        className={cn(textClass, className)}
        width={size}
        height={size}
        fill="currentColor"
        {...props}
      />
    );
  }

  return (
    <NativeSvg
      className={cn(textClass, className)}
      width={width}
      height={height}
      fill="currentColor"
      {...props}
    />
  );
};
Svg.displayName = 'Svg';
export { Svg };
