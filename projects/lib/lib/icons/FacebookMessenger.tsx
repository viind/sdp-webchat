import * as React from 'react';

import { Svg } from './tailwindSvg';
import { LinearGradient, Path, Stop } from 'react-native-svg';

const FacebookMessenger = (
  props: React.ComponentPropsWithoutRef<typeof Svg>
) => {
  return (
    <Svg id="Layer_1" viewBox="0 0 1000 1000" {...props}>
      <LinearGradient
        gradientTransform="rotate(-90 1903 1903)"
        gradientUnits="userSpaceOnUse"
        id="tweaked_values_1_"
        x1={2806}
        x2={3806}
        y1={500}
        y2={500}
      >
        <Stop offset={0.1137} stopColor="#006dff" />
        <Stop offset={0.9493} stopColor="#00c6ff" />
      </LinearGradient>
      <Path
        d="M1000 500c0 276.1-223.9 500-500 500S0 776.1 0 500 223.9 0 500 0s500 223.9 500 500z"
        id="tweaked_values"
        fill="url(#tweaked_values_1_)"
      />
      <Path
        d="M499.5 162.1c-185 0-335 140.3-335 313.3 0 98.4 48.5 186.2 124.5 243.7V839l114.3-63.4c30.5 8.5 62.8 13.1 96.2 13.1 185 0 335-140.3 335-313.3s-150-313.3-335-313.3zm35.3 420.1l-86.6-90-166.9 92.3 182.9-194.1 86.6 90L717.7 388 534.8 582.2z"
        id="Bubble_Shape"
        fill="#fff"
      />
    </Svg>
  );
};
FacebookMessenger.displayName = 'FacebookMessenger';

export { FacebookMessenger };
