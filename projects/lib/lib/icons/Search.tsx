import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const Search = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 23 22.994" {...props}>
      <Path d="M16.53 9.343a7.187 7.187 0 10-7.187 7.187 7.187 7.187 0 007.187-7.187zm-1.388 7.326a9.342 9.342 0 111.527-1.527l6.014 6.014a1.077 1.077 0 01-1.523 1.523z" />
    </Svg>
  );
};

Search.displayName = 'Search';

export { Search };
