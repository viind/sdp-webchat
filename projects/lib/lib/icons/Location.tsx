import * as React from 'react';

import { Svg } from './tailwindSvg';
import { Circle, G, Path } from 'react-native-svg';

const Location = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 32 32" {...props}>
      <G
        fill="none"
        stroke="currentColor"
        strokeLinejoin="round"
        strokeMiterlimit={10}
        strokeWidth={2}
      >
        <Path d="M27 12c0-6.075-4.925-11-11-11S5 5.925 5 12c0 8 11 19 11 19s11-11 11-19z" />
        <Circle cx={16} cy={12} r={4} />
      </G>
    </Svg>
  );
};
Location.displayName = 'Location';

export { Location };
