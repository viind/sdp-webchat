import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const GripLinesVertical = (
  props: React.ComponentPropsWithoutRef<typeof Svg>
) => {
  return (
    <Svg viewBox="0 0 15 35" {...props}>
      <Path
        d="M10 34.5a2.5 2.5 0 015 0v30a2.5 2.5 0 01-5 0zm-10 0a2.5 2.5 0 115 0v30a2.5 2.5 0 11-5 0z"
        transform="translate(0 -32)"
      />
    </Svg>
  );
};

GripLinesVertical.displayName = 'GripLinesVertical';

export { GripLinesVertical };
