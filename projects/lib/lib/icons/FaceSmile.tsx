import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const FaceSmile = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 24 24" {...props}>
      <Path d="M21.75 12A9.75 9.75 0 1012 21.75 9.75 9.75 0 0021.75 12zM0 12a12 12 0 1112 12A12 12 0 010 12zm8.325 2.911a5.044 5.044 0 007.35 0 1.125 1.125 0 111.655 1.523 7.28 7.28 0 01-10.65 0 1.125 1.125 0 111.655-1.523zM6.769 9.75a1.5 1.5 0 111.5 1.5 1.5 1.5 0 01-1.5-1.5zm9-1.5a1.5 1.5 0 11-1.5 1.5 1.5 1.5 0 011.5-1.5z" />
    </Svg>
  );
};

FaceSmile.displayName = 'FaceSmile';

export { FaceSmile };
