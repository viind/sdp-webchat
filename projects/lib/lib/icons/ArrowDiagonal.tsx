import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const ArrowDiagonal = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 30 30" {...props}>
      <Path data-name="Path 1603" d="M0 0h30v30H0z" fill="none" />
      <Path
        data-name="Path 1604"
        d="M20.267 5.067h4.667v4.667"
        fill="none"
        stroke="#122536"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
      <Path
        data-name="Line 79"
        transform="translate(17.524 5.476)"
        fill="none"
        stroke="#122536"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M0 7L7 0"
      />
      <Path
        data-name="Path 1605"
        d="M9.734 24.934H5.067v-4.667"
        fill="none"
        stroke="#122536"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
      <Path
        data-name="Line 80"
        transform="translate(5.476 17.524)"
        fill="none"
        stroke="#122536"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M0 7L7 0"
      />
    </Svg>
  );
};

ArrowDiagonal.displayName = 'ArrowDiagonal';

export { ArrowDiagonal };
