import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const Send = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 20.907 21.523" {...props}>
      <Path
        d="M22.368 11.813l-18.7-9.7A1.121 1.121 0 002.1 3.51l2.611 7.225 11.908 2.049-11.904 2.048L2.1 22.057a1.069 1.069 0 001.567 1.294l18.7-9.7a1.039 1.039 0 00.001-1.838z"
        transform="translate(-2.009 -1.994)"
      />
    </Svg>
  );
};
Send.displayName = 'Send';

export { Send };
