import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const X = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 25 25" {...props}>
      <Path
        d="M56.185 100.194a2.5 2.5 0 00-3.537-3.537l-8.223 8.231-8.231-8.223a2.5 2.5 0 00-3.537 3.537l8.231 8.223-8.223 8.231a2.5 2.5 0 103.537 3.537l8.223-8.231 8.231 8.223a2.5 2.5 0 103.537-3.537l-8.231-8.223z"
        transform="translate(-31.925 -95.925)"
      />
    </Svg>
  );
};

X.displayName = 'X';

export { X };
