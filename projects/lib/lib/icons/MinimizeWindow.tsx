import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const MinimizeWindow = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 26 26" {...props}>
      <Path
        data-name="compress-solid (1)"
        d="M9.286 33.857a1.857 1.857 0 10-3.714 0v3.714H1.857a1.857 1.857 0 000 3.714h5.572a1.855 1.855 0 001.857-1.857zM1.857 48.714a1.857 1.857 0 000 3.714h3.714v3.714a1.857 1.857 0 003.714 0v-5.571a1.855 1.855 0 00-1.857-1.857zm18.572-14.857a1.857 1.857 0 10-3.714 0v5.571a1.855 1.855 0 001.857 1.857h5.571a1.857 1.857 0 100-3.714h-3.714zm-1.858 14.857a1.855 1.855 0 00-1.857 1.857v5.571a1.857 1.857 0 003.714 0v-3.713h3.714a1.857 1.857 0 100-3.714z"
        transform="translate(0 -32)"
      />
    </Svg>
  );
};

MinimizeWindow.displayName = 'MinimizeWindow';

export { MinimizeWindow };
