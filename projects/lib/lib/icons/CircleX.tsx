import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const CircleX = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 37 37" {...props}>
      <Path d="M18.5 3.469A15.031 15.031 0 113.469 18.5 15.031 15.031 0 0118.5 3.469zM18.5 37A18.5 18.5 0 100 18.5 18.5 18.5 0 0018.5 37zm-5.854-24.354a1.727 1.727 0 000 2.45l3.4 3.4-3.4 3.4a1.732 1.732 0 002.45 2.45l3.4-3.4 3.4 3.4a1.732 1.732 0 002.45-2.45l-3.4-3.4 3.4-3.4a1.732 1.732 0 00-2.45-2.45l-3.4 3.4-3.4-3.4a1.727 1.727 0 00-2.45 0z" />
    </Svg>
  );
};

CircleX.displayName = 'CircleX';

export { CircleX };
