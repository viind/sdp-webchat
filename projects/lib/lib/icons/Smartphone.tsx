import * as React from 'react';

import { Svg } from './tailwindSvg';
import { G, Path } from 'react-native-svg';

const Smartphone = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 48 48" {...props}>
      <G data-name="26-Smartphone">
        <Path d="M35 0H13a5 5 0 00-5 5v38a5 5 0 005 5h22a5 5 0 005-5V5a5 5 0 00-5-5zm3 43a3 3 0 01-3 3H13a3 3 0 01-3-3v-3h28zm0-5H10V9h28zm0-31H10V5a3 3 0 013-3h22a3 3 0 013 3z" />
        <Path d="M23 42H25V44H23z" />
      </G>
    </Svg>
  );
};
Smartphone.displayName = 'Smartphone';

export { Smartphone };
