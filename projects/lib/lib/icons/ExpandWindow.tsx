import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const ExpandWindow = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 26 26" {...props}>
      <Path
        d="M1.857 32A1.855 1.855 0 000 33.857v5.571a1.857 1.857 0 103.714 0v-3.714h3.715a1.857 1.857 0 000-3.714zm1.857 18.571a1.857 1.857 0 00-3.714 0v5.571A1.855 1.855 0 001.857 58h5.572a1.857 1.857 0 100-3.714H3.714zM18.571 32a1.857 1.857 0 100 3.714h3.714v3.714a1.857 1.857 0 003.714 0v-5.571A1.855 1.855 0 0024.143 32zM26 50.571a1.857 1.857 0 00-3.714 0v3.714h-3.715a1.857 1.857 0 100 3.714h5.571A1.855 1.855 0 0026 56.143z"
        transform="translate(0 -32)"
      />
    </Svg>
  );
};

ExpandWindow.displayName = 'ExpandWindow';

export { ExpandWindow };
