import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const ArrowDiagonalMinimize = (
  props: React.ComponentPropsWithoutRef<typeof Svg>
) => {
  return (
    <Svg viewBox="0 0 30 30" {...props}>
      <Path data-name="Path 1606" d="M0 0h30v30H0z" fill="none" />
      <Path
        data-name="Path 1607"
        d="M22.5 12.5h-5v-5"
        fill="none"
        stroke="#122536"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
      <Path
        data-name="Path 1608"
        d="M25 5l-7.5 7.5"
        fill="none"
        stroke="#122536"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
      <Path
        data-name="Path 1609"
        d="M7.5 17.5h5v5"
        fill="none"
        stroke="#122536"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
      <Path
        data-name="Path 1610"
        d="M12.5 17.5L5 25"
        fill="none"
        stroke="#122536"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
      />
    </Svg>
  );
};

ArrowDiagonalMinimize.displayName = 'ArrowDiagonalMinimize';

export { ArrowDiagonalMinimize };
