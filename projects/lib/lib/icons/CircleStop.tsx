import * as React from 'react';

import { Svg } from './tailwindSvg';
import { Path } from 'react-native-svg';

const CircleStop = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 512 512" {...props}>
      <Path d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM192 160l128 0c17.7 0 32 14.3 32 32l0 128c0 17.7-14.3 32-32 32l-128 0c-17.7 0-32-14.3-32-32l0-128c0-17.7 14.3-32 32-32z" />
    </Svg>
  );
};
CircleStop.displayName = 'CircleStop';

export { CircleStop };
