import * as React from 'react';
import { G, Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const Language = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 24 24.05" {...props}>
      <G data-name="Group 1955">
        <Path
          d="M11.59 71.75h1.61l.307.741 5.389 13.172.187.457h-2.6l-.98-2.4a.644.644 0 01-.12.007H9.4a.644.644 0 01-.12-.007l-.98 2.4H5.708l.187-.457 5.389-13.172.307-.741zm.8 4.363l-2.132 5.217h4.274z"
          transform="translate(-972 -647.773) translate(976.918 585.703)"
        />
        <Path
          data-name="language-sharp-light"
          d="M34.845 71.75v2.327h6.246V76.4h-2.33l-3.346 6.609c.124.08 1.979 2.4 1.979 2.4l-1.437 1.6-1.766-2.109-5.651 4.543-1.43-1.718 5.408-4.289-3.294-4.521 1.643-1.643 2.807 4.045 2.483-4.917h-9.981v-2.323h6.342V71.75z"
          transform="translate(-972 -647.773) translate(945.824 576.023)"
        />
      </G>
    </Svg>
  );
};

Language.displayName = 'Language';

export { Language };
