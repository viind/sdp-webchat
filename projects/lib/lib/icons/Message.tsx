import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const Message = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 24.241 24.24" {...props}>
      <Path d="M7.575 17.423A2.273 2.273 0 019.848 19.7v.758l3.433-2.576a2.28 2.28 0 011.364-.455h6.567a.76.76 0 00.758-.758V3.03a.76.76 0 00-.758-.758H3.03a.76.76 0 00-.758.758v13.636a.76.76 0 00.758.758zm2.273 5.871l-.009.009-.241.18-.81.606a.765.765 0 01-.8.071.746.746 0 01-.417-.677V19.7H3.03A3.033 3.033 0 010 16.666V3.03A3.033 3.033 0 013.03 0h18.181a3.033 3.033 0 013.03 3.03v13.636a3.033 3.033 0 01-3.03 3.03h-6.567z" />
    </Svg>
  );
};

Message.displayName = 'Message';

export { Message };
