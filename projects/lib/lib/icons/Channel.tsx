import * as React from 'react';
import { Path } from 'react-native-svg';
import { Svg } from './tailwindSvg';

const Channel = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 24 21.816" {...props}>
      <Path
        data-name="8725576_channel_icon"
        d="M21.716 17.265a3.271 3.271 0 00-1.886.611L17.159 16.3a4.078 4.078 0 00.2-1.21 4.361 4.361 0 00-3.271-4.209V8.347a3.271 3.271 0 10-2.181 0v2.53a4.361 4.361 0 00-3.271 4.209 4.078 4.078 0 00.2 1.21l-2.679 1.58a3.36 3.36 0 101.254 1.788l2.508-1.494a4.361 4.361 0 006.149 0l2.508 1.494a3.271 3.271 0 103.14-2.4zM4.271 21.627a1.09 1.09 0 111.09-1.09 1.09 1.09 0 01-1.09 1.09zm8.723-17.446a1.09 1.09 0 11-1.09 1.09 1.09 1.09 0 011.09-1.09zm0 13.084a2.181 2.181 0 112.181-2.181 2.181 2.181 0 01-2.181 2.181zm8.723 4.361a1.09 1.09 0 111.09-1.09 1.09 1.09 0 01-1.091 1.091z"
        transform="translate(-1 -1.992)"
      />
    </Svg>
  );
};

Channel.displayName = 'Channel';

export { Channel };
