import * as React from 'react';

import { Svg } from './tailwindSvg';
import { Path } from 'react-native-svg';

const AngleUp = (props: React.ComponentPropsWithoutRef<typeof Svg>) => {
  return (
    <Svg viewBox="0 0 30.712 30.712" {...props}>
      <Path
        d="M44.028 128.694a2.289 2.289 0 013.235 0l11.426 11.426a2.287 2.287 0 01-3.235 3.235l-9.812-9.812-9.812 9.8a2.287 2.287 0 01-3.235-3.235l11.426-11.426z"
        transform="rotate(-45 -115.162 112.249)"
      />
    </Svg>
  );
};
AngleUp.displayName = 'AngleUp';

export { AngleUp };
