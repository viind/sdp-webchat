import { type ClassNameValue, extendTailwindMerge } from 'tailwind-merge';

const customTwMerge = extendTailwindMerge({
  prefix: 'vi-',
});

export function cn(...inputs: ClassNameValue[]) {
  return customTwMerge(inputs);
}

/**
 * The type `any` is sometimes needed in generic contexts for it to function properly.
 */
export type AcceptedAny = any;
