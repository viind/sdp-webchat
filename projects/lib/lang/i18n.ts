import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import en from './en.json';
import de from './de.json';

const resources = {
  en,
  de,
};

export const defaultLanguage = 'de';
export const uiLanguages = ['en', 'de'] satisfies (keyof typeof resources)[];

void i18n.use(initReactI18next).init({
  resources,
  lng: defaultLanguage,
});

export { i18n };
