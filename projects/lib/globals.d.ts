declare module '*.png' {
  import { type ImageSourcePropType } from 'react-native-svg';
  const content: ImageSourcePropType;
  export default content;
}
