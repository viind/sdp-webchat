import react from '@vitejs/plugin-react';
import { execSync } from 'node:child_process';
import { join } from 'node:path';
import { defineConfig, type Plugin } from 'vite';
import packageJson from '../../package.json';
import ReactNativeWeb from 'vite-plugin-react-native-web';

// https://vitejs.dev/config/
export default defineConfig(() => {
  // provide debug information
  // prefer the tag and commit short sha from gitlab ci
  process.env.VITE_VERSION = process.env.CI_COMMIT_TAG || packageJson.version;
  process.env.VITE_COMMIT =
    process.env.CI_COMMIT_SHORT_SHA ||
    execSync('git rev-parse --short HEAD').toString().trim();

  console.log(`Used Version: ${process.env.VITE_VERSION}`);
  console.log(`Used Commit: ${process.env.VITE_COMMIT}`);

  return {
    // changes the `index.html` file to load the files relative to the index file instead of from root (`/`)
    base: './',
    root: __dirname,
    plugins: [ReactNativeWeb(), NativeWindPlugin(), OneFileBundlePlugin()],
    build: {
      outDir: join(__dirname, '../../dist/web'),
      emptyOutDir: true,
      // transpilation config from commonjs to es6 modules
      commonjsOptions: {
        transformMixedEsModules: true,
      },
      rollupOptions: {
        output: {
          entryFileNames: `viind-sdp-webchat.min.js`,
        },
      },
    },
    resolve: {
      alias: {
        '@viind/sdp-webchat': join(__dirname, '../lib/src/index.tsx'),
        '~': join(__dirname, '../lib'),
      },
    },
  };
});

const OneFileBundlePlugin = () => {
  return [
    {
      name: 'one-file-bundle',
      config() {
        return {
          build: {
            rollupOptions: {
              output: {
                /**
                 * This forces vite to bundle all JS code into a single file.
                 * The default behavior is to split the code into multiple files at dynamic imports.
                 */
                inlineDynamicImports: true,
              },
            },
          },
        };
      },
    } satisfies Plugin,
  ];
};

const NativeWindPlugin = () => {
  return [
    react({
      babel: {
        // use the same babel config for bob builder and vite
        configFile: join(__dirname, '../../babel.config.cjs'),
      },
      // https://www.nativewind.dev/v4/getting-started/other-bundlers
      jsxImportSource: 'nativewind',
      jsxRuntime: 'automatic',
    }),
  ];
};
