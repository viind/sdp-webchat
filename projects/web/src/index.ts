/// <reference types="vite/client" />
import { AppRegistry } from 'react-native';
import { viindSdpWebChatSchema, type ViindProps } from '../../lib/src';
import { z } from 'zod';

declare global {
  interface ImportMetaEnv {
    VITE_VERSION: string;
    VITE_COMMIT: string;
    VITE_CUSTOMER_ID: string;
    VITE_SOCKET_URL: string;
  }

  // the globalThis can only be overloaded by providing a var declared variable
  var setupViindWebChat: ViindSetup;
}

interface ViindSetup {
  /**
   * Create the Viind WebChat as an overlay on top of the current page.
   */
  (config: ViindWebChatConfig): Promise<{ sendMessage: SendMessage }>;

  /**
   * Create the Viind WebChat as an embedded chat inside the provided parent element.
   */
  (
    config: ViindWebChatConfig,
    parentElement: HTMLElement
  ): Promise<{ sendMessage: SendMessage }>;

  /**
   * Internal API.
   * Don't use this.
   * You will break the implementation.
   */
  __internal: { addUserMessage?: (message: string) => void };
}

/**
 * Send a message as if the user typed it.
 *
 * @param message Only string is allowed. If the message is empty, nothing happens
 *
 * @throws {Error} If the message is not of type string.
 */
type SendMessage = (message: string) => void;

globalThis.setupViindWebChat = setupViindWebChat;
setupViindWebChat.__internal = {};

const viindConfigSchema = viindSdpWebChatSchema;

export type ViindWebChatConfig = Partial<
  Omit<ViindProps, 'embedded' | 'initPayload'>
>;
type MakePropertyOptional<T, OptionalKey extends keyof T> = Omit<
  T,
  OptionalKey
> & {
  [Key in OptionalKey]?: T[Key];
};

// When the type is inferred from zod, then default values are included as non optional.
// this default config can however not contain values that would be marked as required by zod.
const defaultConfig: MakePropertyOptional<ViindProps, 'initPayload'> = {
  logLevel: import.meta.env.DEV ? 'debug' : 'warn',
  customerId: import.meta.env.VITE_CUSTOMER_ID,
  socketUrl: import.meta.env.VITE_SOCKET_URL,
  socketPath: '/socket.io.multitenant/',
};

export async function setupViindWebChat(
  config: ViindWebChatConfig = {},
  parentElement?: HTMLElement
) {
  console.log('Viind SDP WebChat', {
    version: import.meta.env.VITE_VERSION,
    commit: import.meta.env.VITE_COMMIT,
  });

  const rootTag = parentElement ?? createRootElement();
  rootTag.classList.add('viind-web-chat');
  const initialProps = getValidConfig(config);
  initialProps.embedded = parentElement !== undefined;
  initialProps.rootElement = parentElement;
  initialProps.rootTag = rootTag;

  await applyCssDynamically();
  const { ViindSdpWebChat } = await import('../../lib/src');

  AppRegistry.registerComponent('ViindSdpWebChat', () => ViindSdpWebChat);
  AppRegistry.runApplication('ViindSdpWebChat', {
    initialProps,
    rootTag,
  });

  await waitForAddUserMessage();
  return {
    sendMessage: (message: unknown) => {
      const actualMessage = z.string().parse(message);
      const addUserMessage =
        globalThis.setupViindWebChat.__internal.addUserMessage;
      if (addUserMessage === undefined) {
        throw new Error(
          '[Viind WebChat] internal error: addUserMessage was overridden with undefined'
        );
      }
      if (actualMessage.trim().length === 0) {
        console.warn(
          '[Viind WebChat]: Tried to send empty message. Message was ignored.'
        );
        return;
      }
      addUserMessage(actualMessage);
    },
  };
}

function getValidConfig(config: Partial<ViindProps>): ViindProps {
  const validatedConfig = viindConfigSchema.safeParse({
    ...defaultConfig,
    ...config,
  });

  if (validatedConfig.success === false) {
    console.error(
      'Invalid web chat config. The only default values will be applied instead!',
      {
        providedConfig: config,
        defaultValues: defaultConfig,
      },
      validatedConfig.error.message
    );
  }

  const knownKeys = new Set(Object.keys(defaultConfig));

  const unknownKeys = new Set(
    Object.keys(config).filter((key) => !knownKeys.has(key))
  );

  if (unknownKeys.size > 0) {
    console.warn(
      'Unknown web chat config keys. These keys will be ignored, but you may still want to remove them from your config.',
      {
        providedConfig: config,
        defaultValues: defaultConfig,
        unknownKeys: [...unknownKeys],
      }
    );
  }

  return validatedConfig.data ?? defaultConfig;
}

function createRootElement() {
  const rootElement = document.createElement('div');
  rootElement.setAttribute('id', 'sdpWebchat');
  document.body.appendChild(rootElement);
  return rootElement;
}

async function applyCssDynamically() {
  /**
   * because of the custom font, the vite plugin `vite-plugin-css-injected-by-js` does not resolve the font file locations correctly.
   * As the Css is integrated into the JS file, but the change in file location is not considered by the plugin.
   *
   * Therefore the css file is now separate.
   * But, the customer should not need to keep track of multiple files when integrating the web chat.
   * Therefore, the css file is loaded dynamically and we can ensure that the correct file is loaded.
   */
  const { default: styleUrl } = await import('../../lib/index.css?url');
  const link = document.createElement('link');

  link.setAttribute('rel', 'stylesheet');
  link.setAttribute('href', styleUrl);
  document.head.appendChild(link);
  return new Promise<void>((resolve) => {
    link.onload = () => resolve();
  });
}

async function waitForAddUserMessage() {
  return new Promise<void>((resolve) => {
    const internal = globalThis.setupViindWebChat.__internal;
    const interval = () => {
      if (internal.addUserMessage !== undefined) {
        resolve();
        return;
      }
      requestAnimationFrame(interval);
    };
    requestAnimationFrame(interval);
  });
}
